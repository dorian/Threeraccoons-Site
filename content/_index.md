+++
title = "Home"
draft = "false"
+++

Welcome to my website!
Here is where I post things that I've created and share my ideas.
There's no specific theme or topic for the content, just whatever happens to interest me at the time or falls under the umbrella of some project.
I know the site tends to look a little weird, but I've always loved weird websites.
I could go grab a Hugo theme that someone else made and it would look much more professional, but my favorite thing about having my own website is just cobbling together my own stuff.



1. [Blog](/blog): My personal blog, which is more or less a public diary.
1. [Music](/healing-music): My Grandparents and I have been recording some folk music at home. Give it a listen!
1. [Links](/links): When people share useful links on social media I save and organize them for later reference.
1. [Tags](/tags): All the various tags I've used on different posts.
1. [Codeberg](https://codeberg.org/dorian/): My git repo with my code n stuff.

Follow/Contact:

---
title: "Session Five"
date: 2021-03-16T16:03:12-04:00
draft: false
categories:
- healing-music
---

{{< audioplayer file="../../audio/audio-210316" >}}

## Brother John
This past weekend she could almost do a C chord on the ukulele by herself! That wasn't the case today, she need a lot more guidance. We'll get there though! Her strumming is doing really good. She is doing a better and better job of hitting more strings at one time. She's also started tapping her feet as she plays. The fact that she is keeping time is really exciting! Perhaps at some point we'll get a drum or tambourine to see how well she likes playing with those. She's playing with the ukulele more today than she was last time we recorded as well.

She's got those lyrics down today! "Morning bells are ringing" really tripped her up in the past, so I'm really excited to hear her saying that line clearly.

## Rocky Mountain
I have to admit, from here on for this recording session I didn't pay terribly close attention to my grandparents. I was very excited that my grandpa joined us, and then I focused a great deal on my own playing. I feel like my observations are lack luster form here on.

Grandpa has decided to join us! In the past he has really resisted joining us in this. He likes to joke about how he just isn't capable of making music. I never pushed it beyond asking once each time Grandma and I were working on stuff. Last week I prompted her to beckon him to join us, and he did! Today he didn't even resist at all! I put the lyrics up on my laptop screen for them to read as I played for these songs.

This song is new to both of them, so they are really unsure of it. I'm really excited to see how they improve with this song as we practice. The cat also joins us for this song!

## Au clair de la lune
This song is a bit simpler, so it was easier for them to sing. You can hear them singing pretty consistently for the whole thing. Pretty soon we'll put out a single ;).

## Donkey Riding
Grandma is more confident on this one than Grandpa, but that's expected since she's been practicing it with me for a bit. He definitely gains some confidence with the chorus as we do several verses. In this you get to hear a new verse for Donkey Riding that I put together, and our attempts to write one about my grandparents home town. Maybe in the future we can put together a whole set all about places that are important to them!

## Lavender's Blue
This is one of the first songs I learned on the ukulele, so they've heard this a lot. They did pretty well!

## Fin
And that's all we did today! I'm not sure what we'll do for our next session honestly. This is pretty much the full swath of songs I have that I can currently play for them on the ukulele. I have a few more that I need to work on, so maybe I'll have a new on for us all next time. Or maybe I can get Grandpa to try his hand at the uke!

---
title: "Session Four"
date: 2021-03-16T16:02:58-04:00
draft: false
categories:
- healing-music
---

{{< audioplayer file="../../audio/audio-210311" >}}

## Brother John & Donkey Riding

What beautiful renditions of Brother John! Grandma was more willing to try out playing this time than she has been recently. And she remembers the fingering for the C chord! She still needs some help finding the right spot to put her fingers, but she knows she need to put a finger on the first string to get it. And she recognized when her fingers were off and the chord sounded wrong! She fixed that herself with minimal help from me. I was amazed at Grandma's solo! She's got the first couple of words down, and even when she fumbled with the second stanza she still kept going with rhythm and tune! Incredible!

She's remembering more words from Donkey Riding as well. She sang almost the entire first verse with me. Her grasp on the chorus is improving as well. I really love this song, and I'm so excited that she's getting it down. And then she prompted Grandpa to join! And he agreed! It was all the more fun because all of us were working on it. He even asked if I had a book with the words on it so he could sing along. That was a great idea, and really helped a lot I think. That's a great addition of tools to help them be more engaged and feel confident on our project.

They really love my joke about recording an album and making the big bucks. It always gets great laughs. Joke's on them I'm planning on editing these and putting them on bandcamp for pay what you want.

---
title: "Healing Music"
date: 2021-03-04T18:21:34-05:00
draft: false
---
Disclaimer: I am not a therapist. This is simply a personal project and journey of healing I am taking with my loved ones.

Towards the end of Summer 2020 my therapist suggested that I learn to play an instrument. The various skills and focus you have to develop are excellent therapeutic tools. Plus music is just plain fun. So after some prompting I got a tenor ukulele. I'm not particularly good at it, but I play so that my grandparents and I might find enjoyment and healing together.

For those of you who don't know me, I live with my grandparents. My grandmother has Alzheimer's, and my grandfather and I care for her. Below are some recordings I've made of us playing and singing together. I hope to at some point put some of our better recordings into proper songs and put an album out on bandcamp, but that's down the road a bit. I'm sharing them here in the hopes that you might find joy and healing in our collaborative therapeutic music.

If you find these meaningful or they bring you joy in any way please get in contact and let me know (My contact details are available on the homepage of this site). My grandparents and I would love to hear from you.

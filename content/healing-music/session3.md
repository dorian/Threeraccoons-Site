---
title: "Session Three"
date: 2021-03-16T16:02:54-04:00
draft: false
categories:
- healing-music
---

{{< audioplayer file="../../audio/audio-210302" >}}

# Session Three
Unfortunately I left the tv on in the background so the audio quality is a bit mucky in this one :(.

## Brother John
Her strumming is really improving! She's managing to hit more of the strings, and in a smoother fashion. This is all sans any kind of coaching from me about it. I help her get her finger set up to play the C chord, but after that I just let her play. She's also getting better at the words. Perhaps we'll make it to a point where she can sing and play at the same time soon!

## Donkey Riding
She is really starting to belt out that chorus! I am continually impressed by how much she engages with and pays attention and engages with the song beyond what I prompt her specifically to do. She is having fun, and using a great number of cognitive skills to play with me.

## Au claire de la lune
She's slowly picking up the words to this one. I'm not sure if it's more difficult than Donkey Riding, or if the words are just less familiar. Playing the notes of this song are fairly simple on the ukulele, it's four notes, and you press on string down the whole time.

This also evoked a memory! She didn't have any specific names or times that she could share, but it spurred a memory of a happy time where she had played with an instrument before. And this play is opening up desires that she had not shared before. I had no idea prior to our work together on this project that she had any wish to play an instrument at all.

## Spoons!
I broke out the spoons to see how that would change the dynamics of our play. I'm curious to see if an added rhythm instrument affects her playing at all. She already keeps very good time while we play, she tends to tap her feet with the rhythm of whatever song we happend to be playing. Adding another instrument also gives us some space to just play and jam, rather than having the play oriented towards the goal of completing a song.

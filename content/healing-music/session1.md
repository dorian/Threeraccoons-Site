---
title: "Session One"
date: 2021-03-07T13:07:37-05:00
draft: false
categories:
- healing-music
---

{{< audioplayer file="../../audio/audio-210218" >}}

## Our First Song
This isn't quite the first time we've played together. We have done some short renditions of Brother John in the past, but this is the first time Grandma has worked at remembering lyrics and singing them while I play. I find the second recording really interesting, because it reveals some of her self conception. She is hesitant to engage with music because she views herself as not very good at it. However, when I leave the ukulele near hear and walk into the other room, she pretty consistently will reach out and pluck the strings. She is interested in music and the instrument, but self consciousness about being "bad" at it. holds her back from exploring it.

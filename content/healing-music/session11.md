---
title: "Session Eleven"
date: 2021-04-07T16:29:20-04:00
draft: false
categories:
- healing-music
---

Over the weekend we decorated the maraca. I picked up some metalic sharpies to draw on the green duct tape. I drew some designs, a cat face and a dog face, and added some squiggles before handing it to Grandma to let her add something. She wrote "Apricot" by the cat and that's all she wanted to do. She's been much more interested in the maraca since we did that, but only to read what we wrote on it, not to play around with it. Maybe in the future!

## Brother John

{{< audioplayer file="../../audio/audio-210406-s1" >}}

Again today she wasn't real eager to play, but she did. Having our maraca around seems to help some. I can play that while she plays the ukulele. She's rather fatigued today, but once she gets playing she enjoys it enough to keep going. She didn't want to sing at all today. I think just playing was enough work for her to focus on. When I took over playing and her and Grandpa sang she did really well.

## Au Clair De La Lune

{{< audioplayer file="../../audio/audio-210406-s2" >}}

Grandpa joined us again! Grandma is focused on the maraca so she didn't join in singing. I'm really glad she still finds the joke about recording her album funny. It's really fun to hear her laugh about it, and it bring a lot of joy to this project of ours.

## Saturday Night

{{< audioplayer file="../../audio/audio-210406-s3" >}}

Grandma sang along with this one! This one works really well, Grandpa and Grandma both sing along really well. It works out better when I can manage to play the right chords! Grandma really cracks up over the joke about singing it in French, I need to practice that more, and maybe I can get her and Grandpa to do it with me. I think that would really give them a kick, to sing in a foreign language. So much joy with this song!

## Donkey Riding

{{< audioplayer file="../../audio/audio-210406-s4" >}}

This song sounds a lot different to my ear when Grandpa sings it with us. He fills in a lower voice register and fills it out very nicely I think. Grandma is really going with the maraca too! She is so full of laughs today it's wonderful. Grandpa gets a big smile on his face whenever she laughs like that.

## Lavender's Blue

{{< audioplayer file="../../audio/audio-210406-s5" >}}

She knows the words to this song! All I had to say was "Lavender's Blue" and she started singing it. After we finish singing it she was looking at the decoration and writing we put on the maraca. Grandpa's contribution was writing "I like anything" on the top. Her joke about writing on the other house is new. That's the first time she's made it! I'm curious to see if it's something she'll say again.

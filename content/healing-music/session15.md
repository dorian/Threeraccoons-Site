---
title: "Session Fifteen"
date: 2021-05-14T16:12:01-04:00
draft: false
categories:
- healing-music
---

{{< audioplayer file="../../audio/audio-210501" >}}

We have a special treat this time! My dear friend John and his wife Becky visited and played music with us! John has a much nicer recording set up than I do so this recording session sounds very different from our normal ones. I hope you enjoy!

We even got a new version of Brother John today! John and Becky showed us what it sounds like in Chinese! I thought that was so cool, next time I'm going to ask them to go slower and help us to learn it. Exploring lanugages and culture through music! Donkey Riding was out power house today. Grandma is the most familiar with is, so that's the one we tended to fall back on when in doubt. I hope you enjoy all of our filler conversation in between songs on this one. It's definitely my favorite part.

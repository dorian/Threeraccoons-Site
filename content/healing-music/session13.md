---
title: "Session Thirteen"
date: 2021-04-17T11:46:42-04:00
draft: false
categories:
- healing-music
---

{{< audioplayer file="../../audio/audio-210413" >}}

## Brother John
This time and the past few playing has taken enough mental work from her that she hasn't sung along. When I take the uke and play she sings along. That's a really interesting metric of her cognitive abilities. Can/does she feel confident enough to coordinate the instrument and her voice at the same time.

When I played the second round and Grandma sang Grandpa played the maraca. I had hoped he would also sing along, but he doesn't sing unless I pull up words on the computer screen so that he can read them. I certainly don't mind doing this for him, but I don't have the words for Brother John somewhere where I can easily pull them up. I'm really curious about why he only wants to read lyrics instead of reciting them from memory.

## Saturday Night
I love her laugh in this. After we finish the song we have an exchange about going outside. I'm ashamed of the tone of my voice in this. This is a common difficulty we encounter every few days this time of year. It was really sunny out and looked very nice. But this was a cool day and later in the evening so it was in the low 40's outside. Too cold to go sit outside.

## Donkey Riding
She jumps right in on the chorus like usual. We play around some with ad libbing but we didn't get too far today. I worry our repetoir is getting boring to listen to. I need to learn new songs I feel.

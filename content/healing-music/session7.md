---
title: "Session Seven"
date: 2021-03-25T15:12:57-04:00
draft: false
categories:
- healing-music
---

## Brother John

{{< audioplayer file="../../audio/audio-210323-s1" >}}

More ad libbing! It brings me such joy to listen to her fill in. It really shows what she does remember! She's doing an excellent job with the notes. It's interesting how the music of it sticks more in her mind than the words do. I've always been the opposite, I'm a very textual person.

## Interlude

{{< audioplayer file="../../audio/audio-210323-s2" >}}

I left the ukulele by her while I went and got her a glass of milk, and she really went to town playing with it! It makes me smile thinking about how much she's drawn to the instrument and has fun with it, even if she's self conscious about her playing ability. It shows me how important it is to leave her alone with it, to give her space where she doesn't feel the pressure of observation and so feels free to play. She posesses a lot of joy and creativity, and I can help her express it.

## Donkey Riding

{{< audioplayer file="../../audio/audio-210323-s3" >}}

She's getting really good at that chorus! Including when I mess it up! She was right on time and ready to jump in. And she ad libbed a verse about her home town! How cool is that?! I didn't even have to prompt her, she just added stuff in. And then we spent some time talking about where they used to live.

## Extra Thoughts
Something that I really love about folk music is how the structure is ameneable to adding new bits and adjustments that are unique to one's own experience. We can take songs of tradition and use them in the creative process of generativity. From our past and into our present and future. It's part of why I focus on using folk music and public domain music for this project here. We get to create and play without being limited by copyright.

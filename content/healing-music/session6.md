---
title: "Session Six"
date: 2021-03-21T08:22:08-04:00
draft: false
categories:
- healing-music
---

## Brother John

{{< audioplayer file="../../audio/audio-210319-s1" >}}

After we sang it together once I left the space open for her to try it herself. I didn't push, merely continued playing and gave her room to try. She didn't know many of the words on her first try by herself, but she got the notes and cadence right. We practiced it together a second time, after which I gave her more space to try. And she did it! She got the words down, and when she forgot the words to the third line she improvised and hit all the notes properly. What a wonderful rendition of the song. She's getting better at placing her fingers for the C chord as well, she was only off by one fret the first time, and the second time she was spot on. Then we tried the F chord, which requires two fingers and to arch them so that is a lot more difficult for her.

## Donkey Riding

{{< audioplayer file="../../audio/audio-210319-s2" >}}

She has the chorus of Donkey Riding down pretty well. She only needs minimal prompting when we get to that part of the song.

## Au clair de la lune

{{< audioplayer file="../../audio/audio-210319-s3" >}}

This time I put the lyrics up for her. She does really well with it! I think it's really helpful for our dynamic when I mess things up. It's good practice (for me) in going with the flow instead of trying to back up and fix things. She already does pretty good with this. She might be self conscious about it, but in the moment she does a good job of just going with whatever is happening in the song.

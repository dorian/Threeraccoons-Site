---
title: "Session Eight"
date: 2021-03-29T09:13:50-04:00
draft: false
categories:
- healing-music
---

## Intro & Brother John

{{< audioplayer file="../../audio/audio-210326-s1" >}}

I left Grandma with the ukulele while I got a cup of coffee so that she could just play. The past week or so she's been much more willing to just pick it up and play with it. She seems less self conscious about it.

The other day I left the ukulele in front of Grandma as I went to do something in the other room. We were just playing around, so I didn't have the mic set up. She picked up the uke and started playing and singing Brother John with zero prompting or guidance from anyone! She can only remember the first line so far, but lyrics are such a small part of that task. She picked up the uke, had the desire and confidence to play, started strumming, and tossed out some lyrics. How amazing is that! Grandma has never played an instrument as far as I know, so this all strikes me even more as a wonderful example of the kind of growth, learning, and generativity that is capable even while dealing with dementia.

As we learn more and she gains skills and confidence we have more and more fun just playing together.

## Donkey Riding

{{< audioplayer file="../../audio/audio-210326-s2" >}}

She comes in strong with that chorus now! And in the second verse she knows more of the words! Something I really love about our playing music together is that it's not me teaching her. She loves music, and I'm playing and holding space for her to join me and have fun. This is a project we are doing together.

## Au Clair De La Lune && Saturday Night

{{< audioplayer file="../../audio/audio-210326-s3" >}}

We just gave Au Clair De La Lune a quick run through. I'm hoping at some point to help her play the notes for it, since most of it only requires one string to be pressed the entire time. Then we did Saturday Night once before doing Au Clair De La Lune again. She really loves music, and her wish to be a good singer is really delightful. I love that we have this simple songs that are easy for us both to play and sing.

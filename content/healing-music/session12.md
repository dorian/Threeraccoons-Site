---
title: "Session Twelve"
date: 2021-04-13T16:42:37-04:00
draft: false
categories:
- healing-music
---

{{< audioplayer file="../../audio/audio-210410" >}}

## Donkey Riding
She's not too thrilled about this at the beginning today. But once we start singing she finds her flow and joins in well. I have a lot of fun playing with making up verses on the spot, and Grandma really joins in today! I think her confidence with the chorus helps. She gets to join in and just go with a well practiced routine and forget her anxiety about sounding bad or not being a good singer.

I had so much fun with the lyrics she came up with this time! My smile was so broad. It fills my heart with so much joy to hear her invent and laugh. She's extra goofy today and I love it.

She really loves to joke about how her weight on a donkey would be too extreme for it. She's always been a big woman, Grandpa likes to remark about how even when they first met she was "rolly polly". It's pretty adorable coming from him, it's full of love. It stresses me out a little when it comes from her. She's not being all that self deprecating, but I get flustered that she conceives of her self as "too much". I never confront her about it as that wouldn't get us anywhere, but I try to say otherwise and encourage her that she is just fine. It really makes me think about my own self compassion. The habitual ways of thinking about oneself that I need to establish now in order that in a possible future where I develop dementia I fall into patterns of self love rather than self critique.

## Brother John
A very nice interlude by Grandma here as I get her a glass of milk.

She didn't really argue about being able to play the uke today! She jumped right in and played. Her strumming is also really good today. I got the spoons out and played with those while she strummed. I even tried my hand at adlibbing some lyrics! This is a task that does not come easily to me. And then Grandma started with Frere Jaques all on her own with no prompting! How exciting. And then she tried the spoons! I fully expeted her to refuse to try! She's really good with those spoons.

## Saturday Night
I went right into Everybody Love Saturday Night while she was hammering away at the spoons. She kept time beautifully and jumped in at the end! So much fun.

## Long Long Ago
I'm very rusty on this song, which makes me not keep time very well and that makes it hard for Grandma to play the spoons because she follows me for timing. But it's a really fun time to play together! I also think it's a really great recording. I think it's important to record our work at learning and not just the final product. So many people are afraid to make beautiful things because they can't make them perfectly. Bad art is the most beautiful art of all if you ask me.

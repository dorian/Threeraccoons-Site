---
title: "Session Ten"
date: 2021-04-02T07:35:11-04:00
draft: false
categories:
- healing-music
---

## Brother John

{{< audioplayer file="../../audio/audio-210401-s1" >}}

She really doesn't want to play today. I'm really curious as to why. She expresses the same reasons for this, i.e. she doesn't think that she can play. But today rather than expressing that and then accepting the ukulele to play with it she says she doesn't want to play.

## Donkey Riding

{{< audioplayer file="../../audio/audio-210401-s2" >}}

She sings the whole verse this time! We've never explicitly practiced the verse before, just the chorus. She sings along even with her lack of confidence about the words. An interesting combination with her usual lack of confidence in playing the ukulele and her new refusal to play it today. I wonder what the happy balance is between offering her various options to participate in music so that she can play in whatever way feels comfortable and offering too many options and becoming overwhelming.

We have been trying out the maraca the past couple of sessions. She does pretty good at keeping time with it! She doesn't seem particularly thrilled about it, perhaps if we decorate it it'll become more enticing.

She constructs another verse by herself again! I find this so exciting and fun! I would like to figure out a good way to do more of this kind of exercise where she gets to iterate on familiar things.

## Saturday Night

{{< audioplayer file="../../audio/audio-210401-s3" >}}

She starts off strong for the first line and then falls off. I'm curious why. The second line is the same as the first one, which she jumped in on readily. I should probably take this one slower, walk her through it a bit more.

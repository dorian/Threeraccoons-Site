---
title: "Session Two"
date: 2021-03-16T09:24:36-04:00
draft: false
categories:
- healing-music
---

{{< audioplayer file="../../audio/audio-210225" >}}

## Brother John
This time we start off the first recording with Grandma playing. Brother John is a very simple song to play on the ukulele. All you need is a C chord and some practice strumming. Even across this single session of playing together you can hear her improvemtents of learning the words to the song. Of course with her Alzheimer's this isn't permament, but it lasts longer than I would expect.

Not only are her skills at singing these songs improving, the act of singing them and our negotiation of whether or not she can sing is bringing her great joy.

## Donkey Riding
In the second recording you can hear pretty clearly that she knows more words to the chorus of Donkey Riding, and sings them with greater confidence. Not only that, but she sings more of it with me without my prompting at all. I have only specifically practiced the words "Hey ho, away we go!" with her. The rest of the lines that she is learning and singing with me she has paid attention to and joined in on of her own volition.

## Au clair de la lune
This is the first time we've worked on this song, so this is a good marker for her initial levels of skill and frustration with a brand new song. I'm excited to have recordings of this as her familiarity increases, so we can see how her engagement with the song changes over time.

---
title: "Session Fourteen"
date: 2021-05-05T12:56:34-04:00
draft: false
categories:
- healing-music
---

{{< audioplayer file="../../audio/audio-210419" >}}

It's been a little while since we played! We open this session spending a little time just playing around!

## Brother John
We do a couple quick tries at Brother John with Grandma on the uke, and I'm playing the spoons. Grandma isn't singing a long today, but that's because she's really focused on the ukulele.

I've put some stickers inbetween the frets on the ukulele to see if they can help us with some chords. Grandma does really well with a C chord, and I'm interested if we can work on getting the hang of other ones as well. Maybe some day she'll even manage to switch chords mid song!!

She does a great job at keeping time with the spoons, once I show her how to get the sound out of them. I wonder if we could explore rhythm with those. That would be a really interesting challenge for her I think.

## Donkey Riding & Everybody Loves Saturday Night
Hey ho! Away we go with this song again! The spoons really held Grandma's attention here, because otherwise she always sings along with this.

## Jam session
Grandpa agreed to try out the ukulele this time!!! He has always refused in the past and I'm so excited that he agreed to try this time. He's really self conscious about not playing well and is hesitant to just try it out. Grandma continued to play the spoons for this and I grabbed the maraca. It was great fun!

## Lavender's Blue & Everybody Loves Saturday Night
When Grandpa was done jamming on the uke we switched and I played Lavender's Blue. Our rhythm is kind of all 

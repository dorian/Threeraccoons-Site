---
title: "Session Nine"
date: 2021-03-30T07:33:06-04:00
draft: false
categories:
- healing-music
---

## Brother John

{{< audioplayer file="../../audio/audio-210329-s1" >}}

We made a new instrument over the weekend! I've been wanting some kind of simple rhythm instrument to play with since Grandma does really well with keeping track of the beat of a song. Any time we play, and often when we listen to music she taps her feet to the song. After strumming for a bit today she showed some interest in picking. So I helped her figure out some coordination for that. Maybe we'll manage to get some more complex melodies going! When I prompted her to work on singing she did really well! She still needs help with the lyrics, but as long as someone is singing them with her she confidently gets it out.

## Donkey Riding & Saturday Night

{{< audioplayer file="../../audio/audio-210329-s2" >}}

She's joining in on the words of the verse again! She's getting so good at this song it's amazing. I think it helps that this is probably my favorite song that we've done together. And then she did the shaker and sang at the same time! What great coordination! Think of all various muscles you have to work in tandem for that, and in time, and while thinking about the words of the song.

We spent some time talking about sharing our recordings with others. I've been hesitant to bring this up with her. My Grandparents are both rather shy, and they both have this assumption about our recordings that because they aren't "good" at music that this project is kind of silly. But she surprised me, she does know how our playing might be enjoyable and helpful for others. I underestimated her understanding.

Our second try at Saturday Night, it's shaping up nicely I think. She tossed some words in there too, I think we can build up a really fun ad hoc play with that one! I'm excited to try it out more!

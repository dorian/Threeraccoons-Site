+++
title = "About"
date = "2020-10-09"
+++

I am Dorian, welcome to my blog. This is a place for me to play around with
website design and share my passion for free software. Free software (Libre not
Gratis) has the power to enable us to build a future where all memebers of our
society can be on equal footing, not dependent on big tech companies or vast
sums of startup capital to give ourselves the tools we need.

You can contact me via email at dorian at this domain here.
On Mastodon @dorian@queer.party
And on Matrix @dorian:threeraccoons.xyz

+++
title = "Today's Accomplishments"
description = "Got a couple good things done today."
date = "2020-09-28"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Set up nginx to properly load the searx instance at searx.threeraccoons.com. Feel free to use it if you are so inclined.
2. Set up nginx to serve the public_html directory in user's home folders
3. Started setting up a /etc/skel set up. Need to do more.
4. Filled out some topics for lessons I would like to make for tilde.school

I would have liked to do better:
1. Honestly I don't know. It was an okay day until it wasn't, but I handeled the bad stuff well.

Tomorrow I would like to:
Tomorrow is a very busy day. If I get even just a little headway on either making lesson plans

+++
title = "Today's Accomplishments"
description = "Tidied up some digital stuff today."
date = "2020-10-07"
draft = "False"
categories = ["blog"]
+++

Today I Accomplished:
1. Journaled
2. Meditated
3. Fixed Element/Riot instance
4. Made Facebook post about alum group meeting
5. configured weechat-matrix to be functional
6. backed up some media files
7. backed up diary and writing
8. Spent time with Grandma and Grandpa

I would like to do better:
Grandpa and I struggled to communicate over some stuff today. I want to do better there.

Tomorrow I would like to:
1. Play with ikiwiki and maybe convert this blog over there. Once I get an ikiwiki setup I like it should be easy. Both use plain text and markdown.
2. Pick and host a form to send out to alum.
3. Look into Matrix-Signal bridge

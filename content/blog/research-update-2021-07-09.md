---
title: "Research Update 2021 07 09"
date: 2021-07-09T08:54:44-04:00
draft: true
---

I haven't written an update in a few weeks. My reading has slowed down drastically. I've been struggling with organization lately, and a poor sleep schedule the past week is doing the opposite of helping. I'm not sure where this project is going honestly. I think I'm hitting upon something that's very interdisciplinary and very important, but I can't seem to collect my thoughts on it well enough to talk about it in a way that means anything to anyone else.

## Articles
### Decentralized vs. Distributed Organization by JP Vergne
Vergne offers an analysis of organization structures on two axis, information processing and decision making.

---
date: 2021-04-28T20:26:19-04:00
title: "Wednesday's Accomplishments"
draft: false
categories:
- blog
---

Againg I haven't posted much with this. I have really wanted to the past week or two, but I keep putting it off or forgetting to. I really need to track my daily accomplishments more closely so that I can feel better about the work that I am doing.

Today I accomplished:
1. Started a painting art project with Grandpa and Grandma
2. Set up a PO Box, shared it online, and received various cards for Grandpa and Grandma's 70th anniversary.
3. Read an article about measuring social emotional skills.
4. Made progress and notes on an article about empathy
5. Started some indoor plants for the garden.
6. Watched some videos on caring for people with dementia with Grandpa and Grandma.
7. Finished and published a how to guide on running a matrix server.
8. Recorded and published more music sessions with Grandma and Grandpa.
9. Started a Yoga habit. I'm 18 days in and doing great!

Next time I would like to do better on:
1. Being vulnerable.

Tomorrow I would like to:
1. Read some more of the article on Empathy.
2. Read (hopefully finish) Dare to Lead by Brene Brown.
3. Dare to be vulnerable in some small way.

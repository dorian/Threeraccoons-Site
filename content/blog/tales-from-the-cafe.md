+++
title = "Tales from the Cafe"
author = ["Dorian Wood"]
date = 2021-10-24T06:48:00-04:00
lastmod = 2021-12-19T20:47:22-05:00
tags = ["book-review"]
draft = false
weight = 3006
+++

Author: Toshikazu Kawaguchi

Narrator: Kevin Shen

Published: 2021

Read on: 2021-10-20

This is the sequel to "Before the Coffee Gets Cold" which I read earlier this year. More great stories of the same kind. The premise is that in a certain cafe in Japan you can travel to the past; you cannot change the present no matter how hard you try, you must remain in the same seat the whole time, and you can only meet someone who has been to the cafe before. I love that first one. You cannot change things. Things are as they are, the only thing you can achieve is changing yourself by a short conversation with someone in a coffee shop. The theme of all these stories was your duty to be happy. If loved ones have suffered tragedies, then it is important that we work our hardest to find happiness in order to make sure all their effort to make us happy while they were with us was not in vain.

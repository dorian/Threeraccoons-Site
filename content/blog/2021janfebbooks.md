---
title: "January - February Book Roundup"
date: 2021-02-18T22:11:46-05:00
draft: false
categories:
- article
tags:
- book-review
---
I read some books! My total so far this year is twelve. And I've realized two important things about my relationship to books in the past couple of months. The first is that I enjoy reading more than any other medium of culture. For various reasons I've watched much less TV and movies the past two months than I had been, and I'm realizing that the books that took their place tended to bring me more joy. I've also realized that a good part of my enjoyment of TV shows and movies is the social aspect. They are much easier to experience with another person than books in my experience. I look forward to enjoying more movies as I am able to participate in more social activities post pandemic. But also this has made me conscious of ways in which I might make my consumption of books more social, and how that might bring me more joy and help me feel more connected to my social circle. Enough contemplation of the virtue of various media formats, on to the books!

## Books

### Wishful Drinking
#### Audiobook; Author: Carrie Fisher; Published: 2009; Read on: 2021-01-07

What a delightful memoir! I was never super into Star Wars as a kid so most of my familiarity with Carrie Fisher is from social media screenshots of her great tweets. And this memoir very much fits in with the tone she cultivated on there. She was traumatized a few years prior to the book by the death of a friend, and that is very much the current that flows underneath everything she shares. She shares quite a bit about the traumatic event, and the fact that it affected her deeply, but she never dwells on it, and she always cracks a joke about it. I found myself wondering a little, are the jokes a way to avoid the weight of it? Or are they a way of dealing with it? We all suffer trauma in life, and we all deal with it in our own way. I really got the sense that Carrie was dealing with it the best she could (you bring a certain je ne sais quois of vitality when you do I think), and I walked away from this book with the lesson that I should trust myself to make it through, and to be there for my friends.

### Cassio Vol. 4: Final Blood
#### Graphic Novel; Author: Stephen Desberb; Published: 2018; Read on: 2021-01-07

Next I read the next volume in the Cassio series. I have some...rather critical feelings about this series, but I think I'd like to finish the series first before I pass that kind of judgement.

### Before The Coffee Gets Cold
#### Audiobook; Author: Toshikazu Kawaguchi; Narrator: Arina Li; Published: 2020; Read on: 2021-01-15

Oof. I loved this book! It's such an interesting take on time travel and I was gripped! The premise is that in a certain coffee shop in Japan it's possible to travel back in time, but there are rules. Among them; you can only travel back in time to meet someone who has been to the shop before, nothing you do to change the past will change the present, and you must return before the coffee gets cold. I would say it's a deep exploration of how we deal with regrets, and the ways in which small shifts of our own internal perspectives can have profound impacts on how we judge our situation, and the actions we are willing to take to achieve our own happiness. The narrative follows several different characters, each with something in the past that they long for, something that is a huge hindrance to their happiness in the present. And each one, even though they change nothing, return from the past and are able to use their experience to pursue and achieve a happiness that had not even conceived of. I feel there's a profound life lesson here, and I cannot recommend it enough. If you only choose one book to read from my list here, choose this one.

### The Perennial Philosophy
#### Audiobook; Author: Aldous Huxley; Narrator: Matthew Lloyd Davies; Published: 1945; Read on: 2021-01-23

I have a lot of feelings about this one. And I do feel I need to toss in a disclaimer here, I listened to this in audiobook format. Which for me, while I love them, I often don't give them my full attention. And this book is dense, if you really want to pull out all it has in it, it needs one's full attention. Okay, disclaimer over. So large portions of this book is Huxley being far more mystical about a single omnipotent deity than I'm comfortable with. Which, really wasn't bad, and he's certainly never preachy about it, it's just that after growing up in a toxic religious environment I'm not keen on the whole g-d thing. I do see myself as having faith and participating in religious practices, but those that I chose to pursue are all very light on g-d. That said, this is very good. I think this is a useful religious piece for people of any faith. Huxley (in a very typical Anglo-American manner) attempts to dig into the mystical/spiritual practices of all the major world religions and find their common threads. I think he does quite a good job, I agree with much of his analysis. That said, not much in this book really struck me as useful for my personal spiritual practices. That's not a lack in the book, but rather I have kind of just already chosen the religious traditions that I find valuable and wish to dive deeply into. However this book works for any reader that's interested in spiritual exploration. This is a great overview, but one should choose a small number of spiritual practices that partake in these common threads and pursue them with persistence.

### The Great Gatsby
#### Audiobook; Author: F. Scott Fitzgerald; Narrator: NPR's Planet Money Team; Published: 1925; Read on: 2021-02-06

The Great Gatsby is in the public domain this year! And as a celebration, NPR's Planet Money team did a full, unabridged reading of the book and put it out for [all to listen to](https://www.npr.org/2021/01/14/956800308/the-great-gatsby)! This was a fun book to return to. I read it in high school, but as is often the case with teenagers, I did not have the attention or experience to appreciate it's skillful writing. One of the things I like about this novel is how short it is. I love books I can read in an afternoon. It makes it possible to digest the whole thing in a day, and then just mull over the narrative for awhile. I recommend listening to [this BBC In Our Times Episode](https://www.bbc.co.uk/programmes/m000r4tq) on The Great Gatsby before listening to the book. I found that it primed me really nicely to take better note of the magnificent imagery that makes this novel a great one.

### Wayward Children Series
#### Author: Seanan McGuire
* Every Heart a Doorway; Audiobook; Narrator: Cynthia Hopkins; Published: 2016; Read on: 2021-02-02
* Down Among the Sticks and Bone; Narrator: Seanan McGuire; Published: 2017; Read on: 2021-02-03
* Beneath the Sugar Sky; Narrator: Michelle Dockrey; Published: 2018; Read on: 2021-02-11
* In An Absent Dream; Narrator: Cynthia Hopkins; Published: 2019; Read on: 2021-02-13
* Come Falling Down; Narrator: Seanan McGuire; Published: 2020; Read on: 2021-02-15

I devoured this series by Seanan McGuire. It's so much fun! I really love all the bizzare, weird, impossible worlds she constructs in these. They make for great adventures, but also their impossiblities offer some perpectives to think about our own world, our own ethics and morals. The one that has stuck with me most is the world McGuire creates in In An Absent Dream. The Goblin Market enforces the concept of fair value on all transactions its citizens make. You must pay fair value for the goods or services that you receive. If you do not, the market will gradually turn you into a bird. Interestingly, as the story points out, enforcing fairness seems to also preclude generosity. If all transactions must be fair, then it's much more difficult to give someone a gift. The Goblin Market is generally a place full of happiness and love, but it raises questions for me of how I would ideally like to structure my communities. How do we want to perform transactions and social cohesion? Is fairness something that makes for a thriving and vibrant community?

### Sexual Intelligence
#### Audiobook; Author: Marty Klein; Narrator: Alan Winter; Published: 2013; Read on: 2021-02-22

This is a fascinating book. I picked it up at a whim because of course that title is interesting. Klein has a really warm writing style and I found myself liking him more and more throughout the book. Klein really loves sex, and all the quirks, oddities, and variety of human sexual expression. His enthusiasm made the book a really fun read, and his exploration of the differences between men and women are some of my favorite. To paraphrase, there aren't any. Klein lays bare (pun intended) all the ways in which sex is work, and how the emotional vulnerability and connection that is quite often what people really want from sex requires concerted effort and change on our part. The lessons this book holds have very little to do with sex. We all want (and need) genuine human connection. Sometimes that connection involves sex, but most of the time it doesn't. It's an immensely valuable skill to look at ourselves and discover the ways in which our habits and inclinations caues us to act in ways that cut us off from other human beings. Genuine connection requires vulnerability, and it requires us to give up our daydreams of perfect encounters, and to run the risk of failure. Engage with those three things and it will transform your relationships.

### Using and Administering Linux Vol. 1
#### Ebook; Author: David Both; Published: 2018; Read on: 2021-02-25

I picked up this book as a part of a bundle of books on Linux from Humble Bundle, So not only do I have this three volume set, but there's also about a dozen other books. I'm going to be busy with technical reading this year. And boy did I really love this book! I love free software, and GNU/Linux holds a special place in my heart. This book is a delightful read, as Both takes the time to cover the history and structure of why behind various system structures and plans. I need to go back over this volume and take better notes before I move on to the next one but I'm very eager. Computers are such magical little machines! I love them so much, and it's so much fun to reach into the internals of these incredibly complex boxes of electricity and mess with them. Bend the thinking machines to your will, as it were.

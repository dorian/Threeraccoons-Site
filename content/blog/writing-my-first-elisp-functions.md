---
title: "Writing My First Elisp Functions"
date: 2021-05-21T10:26:19-04:00
draft: false
categories:
- article
---

These functions are by no means clever or good, but I've been struggling to grasp elisp so writing about it seems like a good way to cement my understanding. I'm not really a coder. I'm a fairly competent sysadmin, and I know bash well, but figuring out how to play with other languages seems to elude me.

## The Goal: Navigate My Diary
I keep my diary in markdown files, a new file for each day. I'd like to write some functions to easily navigate my diary files, instead of having to manually find the file each time I want to open it.

### Step 1: Figure out how to play with elisp functions
Part of my struggle to play with elisp is I've been usure of how to get output from commands. With bash this is straight forward to me, just run the command in the terminal! The word REPL is tossed around in emacs and lisp circles that initially that's what I tried looking for. I've played with SLIME a bit before when I was looking into learning common lisp. So I was looking for something like that, or something like the python REPL that I used years ago. Turns out there's nothing like that in emacs. I knew what I wanted had to be something that came by default, because emacs is just elisp, there has to be an easy way to play with elisp that ships with it.

I found what I was looking for in the 'inferior emacs lisp mode' or ielm! It's a mode that makes it easy to navigate and write elisp code in the buffer and evaluate it with C-x C-e. The output will be displayed to the right of the cursor.

### Step 2: What is it that I want to do?
Okay so, the first big annoyance I want to solve is the tedium of navigating to my diary file all the time. I'd like a function that will open it. So I need a function that does the find-file operation that I've been doing, and I need to give it the file name for "today".

I need:
- To give (find-file) a string for today's diarly file
- To build a string dynaimcally with today's date by concatenating:
  - "~/path/to/dir/"
  - today's date
  - ".md"

### Step 3: Building This Function
I've written shell scripts using this kind of functionality before, so I know pretty much what I want to do with the catenation. A quick internet search for concat in elisp reveal the function (concat), which takes any number of arguments and returns a string of all them concatenated together.

I plunked `(concat "~/path/to/dir/" "date" ".md")` into the scratch buffer, activated ielm (M-x ielm), and moved the cursor to the closing paren and hit C-x C-e to evaluate it. Text popped up to the right of the cursor showing that it evaluated to `~/path/to/dir/date.md`. Perfect.

Awhile back I copy-pasted some code from Stack Overflow for inputing date stamps into my documents. Peeking at that I found I get the date string with `(format-time-string "%Y-%m-%d")`, so let's plunk that in for the date section. `(concat "~/path/to/dir/" (format-time-string "%Y-%m-%d") ".md")` evals to `~/path/to/dir/2021-05-16.md`. Almost there! Just need to feed that expression to find-file!

Wrapping the find-file function around it is easy:
```
(find-file (concat "~/path/to/dir/" (format-time-string "%Y-%m-&d") ".md"))
```
And evaluating it opens a buffer with today's diary file! Yes! All that's left is to make this a function.

Standard lispy stuff for defining a function:
```
(defun open-diary ()
    (find-file(concat "~/path/to/dir/" (format-time-string "%y-%m-%d" ".md"))))
```

Aaaaand it doesn't work. Peaking around at the other functions defined in my config file I see many of them have `(interactive)` set.

```
(defun open-diary ()
    (interactive)
    (find-file(concat "~/path/to/dir/" (format-time-string "%y-%m-%d" ".md"))))
```
And that works! Hooray! So it seems that if the user is going to call a function it has to have the `(interactive)` in it's function definition.

### Step 4: Make this work with EVIL mode
I use Doom Emacs, so I want to set this function to a leader key so I can easily activate it. I've done this with other stuff before, so it's really easy to add:
```
(map! :leader
    (:prefix-map ("d" . "diary")
     :desc "Open Today's diary"       "o" #'open-diary))
```

## The End!
And all done! I've cobbled together my first elisp function that isn't just copy paste from Stack Overflow! Hopefully many more will follow.

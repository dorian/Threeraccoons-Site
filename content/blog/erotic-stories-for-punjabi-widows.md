+++
title = "Erotic Stories For Punjabi Widows"
author = ["Dorian Wood"]
date = 2022-06-02T12:22:00-04:00
lastmod = 2022-06-02T12:22:30-04:00
tags = ["book-review"]
draft = false
weight = 3002
+++

## Info {#info}

Author: Balli Kaur Jaswal

Narrator: Meera Syal

Published: 2017

Started on: 2022-05-17

Finished on: 2022-05-23

Link: [Erotic Stories For Punjabi Widows](https://www.hoopladigital.com/title/11870611)


## Review {#review}

I really loved this book! I might even return to it sometime this year. The interplay between generations and cultures is wonderfully delightful. I immensely enjoyed the ways Jaswal makes those interplays bi-directional. Nikki begins our journey having grown up in Britain, so she is very familiar with western feminism and initially patronizes the widows who sign up for her language classes. She initially sees them as victims of patriarchy, but she comes to learn of how strong they are. Not only that but they hold wisdom and strength for her as she has to endure sexism and navigate her modern world of London, which is not really all that less sexist than the Punjabi community as Nikki first believes.

This is a great book, a wonderful debut by this author. So much so that I immediately went on to read [The Unlikely Adventures Of The Shergill Sisters]({{< relref "unlikely-adventures-of-the-shergill-sisters.md" >}}), Jaswal's second novel. So far it is equally as good.

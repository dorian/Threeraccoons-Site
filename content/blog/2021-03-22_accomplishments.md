---
date: 2021-03-22T20:07:33-04:00
title: "Monday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Watched two videos of Stats Fundamentals course
2. Worked, was with preschoolers today!
3. Planned some stuff for the garden
4. Scheduled the next alum group meeting
5. I will probably go finish a book after this!

Next time I would like to do better on:
1. Being more proactive with children, being interested in their world rather than waiting as much.

Tomorrow I would like to:
1. Build one or two huglekulture's for our garden!
2. Record more music with Grandpa and Grandma!
3. Play around with some magic/code ideas.

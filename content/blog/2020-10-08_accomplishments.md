+++
title = "Today's Accomplishments"
description = "Didn't complete many tasks, but made progress on several."
date = "2020-10-08"
draft = "False"
categories = ["blog"]
+++

Today's Accomplishments:
1. Choose a forms framework to use for the alum group. Worked a bit to get it working, still need to tinker more.
2. Tested out ikiwiki for blog purposes. Don't have the hang of it yet, still needs more tinkering.
3. Caught up on a lot of rss feeds.

I would like to do better:
Today was a pretty good day. I don't have anything.

Tomorrow I would like to:
1. Play with ikiwiki more
2. Look into signal-matrix bridge
3. Write a script to further automate this.

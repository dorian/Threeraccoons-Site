+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-11-27"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Finished a batch of Christrmas cards with Grandma and Grandpa.
1. Made good progress on a sock.

Next time I would like to do better on:
1. Planning my effort for the day.

Tomorrow I would like to:
1. Website redesign
1. Make checklist of items/steps for Mastodon server

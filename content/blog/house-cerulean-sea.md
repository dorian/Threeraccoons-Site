+++
title = "The House in the Cerulean Sea"
author = ["Dorian Wood"]
date = 2021-08-22T09:28:00-04:00
lastmod = 2021-12-19T20:47:25-05:00
tags = ["book-review"]
draft = false
weight = 3013
+++

Author: T.J. Klune

Narrator: Daniel Henning

Published: 2020

Read on: 2021-08-22

What an absolutely delightful book! I love Linus Baker so gosh darn much. His love for children is just so terribly heartwarming. I also identify heavily with how rules heavy Linus is and how much he opens in the beginning of the novel as passionate about order and bureaucracy. It was really something special to see how Arthur and Zoe and the kids encourage Linus to open up his heart and eyes to the injustices in the world.

The children in the novel are just so gosh darn lovable! Especially in the audiobook, which has wonderful voice acting. How can you not fall in love with Luci as you listen to his tiny six year old voice pontificates about how he is death, plague, pestilence, and the end of days?

And the end, when they finally fall in love?! So. Good. Such great queer love. I love this novel so so much. Everything about it is so tender.

+++
title = "Bury My Heart at Wounded Knee"
author = ["Dorian Wood"]
date = 2021-11-24T09:37:00-05:00
lastmod = 2021-12-19T20:47:22-05:00
tags = ["book-review"]
draft = false
weight = 3004
+++

Author: Dee Brown

Narrator: Grover Gardner

Published: 2010

Read on: 2021-11-19

This book was a hard read.
I have known that my country did terrible things to Indigenous peoples who first lived on this land, but I was not aware of just how atrocious, vile, and heartless the seizure of this land was.
It's not easy, but I think every American should read these stories, to understand our history of how and why we are here.
It is only through incredible violence.
We committed sickening acts of violence on human beings for the sake of profit, so that we might take their lands and exploit it for profit and wealth.
We killed human beings for no reason other than to continue killing the planet.

+++
title = "2020 in Books"
date = 2020-12-29T15:57:55-05:00
draft = false
categories = ["article"]
tags = ["book-review"]
+++

## 20 in 20
At the start of this year I made a plan to read 20 books. Last year I managed that goal and I was excited to pursue it again. It's fun to turn a leisure time activity like reading into a project that I get to feel proud of at the end of the year. Plus if I don't have a list I just won't remember what I've read ha. The written word is an excellent tool for aiding memory. Take that Plato. And writing them all down gives me a wonderful opportunity to contemplate a theme of the year for myself. Something from me that ties all these books together quite and speaks to my journey that I have gone on as I've lived this challenging year and brought these books along with me.

My theme was perspective. The importance of seeing life through another's eyes, of understanding my motivations and drive and desires do not exist in a vacuum, but they coexist with all the beings that surround me. All of our threads are woven into the tapestry of life, and my thread can only be what it is and find peace in the wholeness of the tapestry by interacting with all the other threads. My towering friends in my yard and around my house all participate in this same life, of taking in resources and making the careful decisions of what work to do so that they can thrive the best they possibly can and help out their neighbors along the way in order to foster a vibrant and healthy community (The Hidden Life of Trees). Often times what I really need to do is to set my self aside, and all the business that goes on in my mind in order to really see what this self and all the other selves in the world are really striving after (Becoming Nobody). Stepping outside of the self is sometimes a very necessary part of peering through the eyes of another, as it can aid us in traveling through time and space and allow us to experience the magic of falling deeply in love with someone we have never met (This Is How You Lose The Time War). It lets us experience the joy of another as if it were our own (Queers Destroy Science Fiction), and kindles in our hearts the burning desire to stand in solidarity with those others, whether like us or not, that we might all gain true liberation (Trans Liberation). My journey in perspective has shown me that in my loneliest days the one thing that can save me is my committment to help someone else (The One And Only Ivan). And I learned that no one, no matter how different their life may have been from mine, if I take the time to see things from their perspective will ever really be that different from me (The Wings Of Fire).

So, my goal was 20 books. Then 2020 happened. Turns out being stuck at home most of the year gives ample time for reading. I've managed to traverse the worlds of 47 book so far, and maybe I can churn out one or two more in the two days I have left ;). I'm very excited and proud of this. Big numbers are fun. Some of these books are shorter than others, and I shamelessly count graphic novels as books despite their relative shortness.

Here's the full list, and some of my thoughts about each one. They are listed roughly in the order that I read them this year, with the books of series grouped together.

## Books

### Cat Pictures Please and Other Stories
#### Book; Author: Naomi Kritzer; Published: 2017; Read on: 2020-01-10

I *finished* this book in 2020, although I started it in 2019. But one must draw lines somewhere, and it feels an appropriate way to cross thresholds in the month belonging to the god of such things. But the book.

This is a delightful collection of short stories! The first story, for which the collection is named is possibly my favorite. An AI program gains sentience and decides to help people, the only thing she asks for in payment is that you continue uploading pictures of your cat to social media. The last story in the collection, and thus one of the first things I read this year, is titled "So Much Cooking." The main character is Natalie, and the story is entries from her food blog as she lives through a flu pandemic so bad that it halts life as we know it for a time. So uh. Starting my year with an omen I guess. It's a very good story, if perhaps a little too real for our current circumstances.

### The Hidden Life of Trees
#### Book; Author: Peter Wohllenben; Published: 2016; Read on: 2020-01-17

This book is phenomenal. It quite literally changed the way I look at trees and other plant life. Wohllenben cares for old growth forests in Germany, and he has an incredibly wealth of knowledge about trees and their life cycles in their natural habitats as well as in environments manipulated by humans. Trees are not so different than you and I. Each tree is unique, and according to it's particular temperament it makes decisions about how to best use it's resources to not just live, but to thrive.

### The Song of Achilles
#### Audiobook; Author: Madeline Miller; Narrator: Frazer Douglas; Published: 2012; Read on: 2020-01-30

I'm a bit late to the party on this one. Several of my friends in college raved about this book when it came out. I now see why they did. It's a gripping telling of The Illiad from the perspective of Patroclus, Achilles closest companion and brother in arms (also lover).

### The Tensorate Trilogy
#### Audiobook; Author: JY Yang; Series:
* The Black Tides of Heaven; Published: 2019; Read on: 2020-02-259
* The Red Threads of Fortune; Published: 2019; Read on: 2020-02-26
* Descent of Monsters; Published: 2019; Read on: 2020-04-22

I picked up this series at the [recommendation of C.-E. Boyles](https://www.brokenhandsmedia.com/blog/2020/4/21/edgars-book-round-up-march-april-2020). The first two books and be read in either order, the narrative happens simultaneously from the view of two different characters. Which is a really interesting way to do a trilogy if you ask me. Narrative structure aside, I really enjoyed this series and it's unique world of East Asian Steampunk and following the characters through their journeys as they navigate a world full of magic and imperial bureaucracy (I think there's a fourth book out now? I need to look it up).

### Becoming Nobody
#### Audiobook; Author & Narrator: Ram Dass; Published: 2019; Read on: 2020-03-08

This popped up as a recommendation in the Hoopla app. And it intrigued me enough to check out. I'm a student of meditation and some Buddhist philosophy, so this was interesting. At first I felt like this was going to be another terrible story of a westerner co-opting eastern philosophy/religion for their own personal newage trip, but it turned out to really not be that. Ram Dass really seems to have gone on an interesting and deep journies of personal growth. If you dislike Buddhist philosophies you won't like it, but otherwise it might be interesting.

### This Is How You Lose The Time War
#### Book; Authors: Amal el-Mohtar, Max Gladstone; Published: 2019; Read on: 2020-03-10

Another [recommendation of Edgar's](https://www.brokenhandsmedia.com/blog/2020/8/5/camerons-book-roundup-2020-part-1). Holy Moly I loved this book. This is a journey of time travel that I loved immensely. The places Red travels are almost entirely unimportant. What is important is the relationship, and the ways of interacting available to two time travelers, who happen to be two assassins who never meet. They fall in love in an incredibly tender way.

### Queers Destroy Science Fiction!
#### Audiobook; Editors: Seanan McGuire, Sigrid Ellis, Steve Berman, Gabrielle De Cuir; Narrators: A Skyboat Full Cast; 2018; 2020-03-18

I've read this collection of stories a couple years ago in ebook form, and I was delighted to find it on the Hoopla app in audiobook. I love this collection so much. It's chuck full of delightful queer characters, including numerous trans characters. With each story I found myself delighted at another queer perspective about life present and future. I should note that these stories are often explicit, you may want to listen with headphones for this one.

### Real Queer America
#### Book; Samantha Allen; Published: 2019; Read on: 2020-03-29

One of things I wanted to do this year was to explore Queer literature and stories more. So I went on my libarary's website and tracked down copies of Real Queer America and Trans Liberation. My library didn't have them but I was able to order them through interlibrary loan. They arrived at the library just as the schools were beginning to close down and before any official lockdowns were in effect, so I was able to nab them before getting stuck at home for several months. Real Queer America is a wonderful journey through various queer communities in the United States in the places you might least expect them. And yet, as Samantha Allen shows us, queer folk are thriving. We have an incredible power to transform the places we live. We are not simply doomed to leave home and flee to the big city hoping to find acceptance.

### Trans Liberation
#### Book; Author: Leslie Feinberg; Published: 1999; Read on: 2020-05-02

I'd put this book as essential reading for trans folks honestly. It's a collection of speeches and guest essays from Feinberg's travels speaking to various groups about gender and solidarity. It's not theory, it's some real practical perspectives on what it means to have solidarity not just among queer folk, but also with straight folk who don't fit the mold of heteronormative society either. We all have the same interest in being able to express who we are and to be accepted rather than shunned.

### Traders Tales Series
#### Author & Narrator: Nathan Lowell; Series:
* Quarter Share; Audiobook; Published: 2012; Read on: 2020-04-28
* Half Share; Audiobook; Published: 2012; Read on: 2020-05-01
* Full Share; Audiobook; Published: 2012; Read on: 2020-05-05
* Double Share; Audiobook; Published: 2012; Read on: 2020-05-08
* Captains Share; Audiobook; Published: 2013; Read on: 2020-05-12
* Owners Share; Audiobook; Published: 2013; Read on: 2020-05-16

I love this series so much. To the point that I listen through it about once a year. You get to follow Ishmael Horatio Wong as he meets and overcomes the many obstacles of life. It's fun to watch a character you love win time after time. In the first book Ishmael is forced off a company owned planet after his mother dies and he doesn't have access to work to pay the company rent. He lucks out to find someone at the union hall willing to show him how to get a berth on a merchant vessel. Once aboard Ish finds a new family and spends the next several decades of his life flying from system to system on merchant ships as he works his way up the ladder to eventually be captain and even owner.

### Sissy
#### Book; Author: Jacob Tobia; Published: 2019; Read on: 2020-06

Another selection from my mission to read more queer stories, Tobia's memoir was gripping. Tobia shares how they have traveled through life, attempting to navigate the restrictive and senseless world of gender and how they were and weren't allowed to be in the world. If you want a window into what some nonbinary people go through growing up, read this book.

### The Mountains Sing
#### Audiobook; Author: Nguyen Phan Que Mai; Narrator: Quyen Ngo; Published: 2020; Read on: 2020-06

Another random Hoopla pick, this one I really enjoyed. The narrative was a little hard for me to follow at times, but I suspect that is largely due to book containing various flashbacks. This is only troublesome for me because I tend to not parse dates or numbers when I'm listening. That said, the flashbacks and multiple perspectives over the course of generations in this novel are absolutely phenomenal. It's a journey of one family and three generations of women as they struggle to survive the trauma of a war torn Vietnam.

### Tears of the Trufflepig
#### Audiobook; Author: Fernando A. Flores; Narrator: Raul Castillo; Published: 2019; Read on: 2020-06

Yet again, random Hoopla pick. Amazing. I highly recommend this novel. It's so good. I think I would give this novel the classification of cyberpunk honestly. It takes place in an unspecified near future where animals and livestock are scarce (or in many cases extinct), and the rich live on "filtered" animals and plants, which are created artificially. Over ethical concerns filtering animals has been outlawed. Of course when things are illegal that simply means there will be complex networks of organizations dealing in such things outside of the law for profit. On top of all this the in vogue market of illicit materials popular among the rich is shrunken heads of indigenous peoples of the Americas. These things tie together in some delightful mystical twists by the end.

### Magic for Liars
#### Audiobook; Author: Sarah Gailey; Narrator: Xe Sands; Published: 2019; Read on: 2020-08-15

This one as well was a [reccomendation from Edgar](https://www.brokenhandsmedia.com/blog/2020/6/30/edgars-book-round-up-may-june-2020). Private Eyes and magic. Except our PI protagonist doesn't have magic, something that she's spent a lot of time being bitter about and avoiding since her sister went off to magical high school and she lost her closest friend. But she can't avoid it any longer when there's a murder at the school her sister teaches at, and all the magic in the world can't seem to solve it.

### The One And Only Ivan
#### Audiobook; Author: Kathrine Applegate, Narrator: Adam Grupper; Published: 2013; Read on: 2020-09-05

I love Applegate. I devoured Animorphs as a child and I'm always eager to read more of her work. And this one. This one is phenomenal. Exit 8 Big Top Mall is a roadside zoo that is slowly dying. The owner is mostly washed up, and in a desparate attempt to save his business and income he purchases a baby elephant to hopefully attract more customers again. Ivan was once the main attraction of the Big Top Mall, but fame is never forever. These days Ivan spends most of his time watching tv, and sometimes drawing pictures to be sold in the gift shop. Ivan lives in a concrete cage, with no companionship except for that offered by the other animals at the attraction. This story is based on a true story of a gorilla who spent thirty lonely years at roadside zoo before public outcry brought a spotlight on him and the operator of the attraction, and he was removed to a zoo where he could have a proper life, not just an existence in a cage. Written in the first person perspective of Ivan, this novel takes you on a journey of what it's like to be alone for thirty years in a cage. It's sad and deeply moving, but Ivan gets a happy ending.

### So You Want To Talk About Race
#### Audiobook; Author: Ijeoma Oluo; Narrator: Bahni Turpin; Published: 2018; Read on: 2020-09-04

With the events of May, and the fact that I'm a person of European American descent with considerable privilege because of that, I wanted to include some reading that would help expand my perspective of the world. This was very good, although since I'm now three months removed from it I'm not going to attempt a summary of it. I think everyone should read it. You can most likely get it for free through your local library.

### How To Destroy Surveillance Capitalism
#### Ebook; Author: Cory Doctorow; Published: 2020; Read on: 2020-09-10

I'm not entirely sure this counts as a book but Doctorow calls it such, and I'll willing to go with that since it bolsters the number of books I've read this year ;). Regardless of how we might categorize it, this is very good. It has informed how I think of social media and the problems caused by it a great deal. It's not a pancea to fix all the ills of social media forever, but the fact that a few large corporations control the entirety of our online experience these days is the direct source of a great many harms. If you are worried about various abuses of the state upon citizens, you should read this. The monopolistic practicies of large tech companies today has contributed a great deal to the power of state agencies to target and harass innocent people, and breaking up those monopolies would an incredible step towards creating a healthy internet. Since this piece has been published The US Department of Justice and 11 states have filed a suit against Google, and all fifty states have filed one against Facebook. If you want more conceptual tools to analyze what those lawsuits could mean, [read this book](https://onezero.medium.com/how-to-destroy-surveillance-capitalism-8135e6744d59).

### His Only Wife
#### Audiobook; Author: Peace Adzo Medie; Narrator: Soneela Nankani; Published: 2020; Read on: 2020-10-10 & 2020-10-29

I loved this book so much I read it twice while I had it checked out from the library in the Hoopla app! I'm a sucker for stories of the regular working class girl who ends up falling in love with a rich man and the task of navigating a new world. Eli married Afi in absentia, an arranged marriage on the part of his family in an attempt to coerce him to end a relationship with another woman they don't like. Afi, with her iron will, intense convictions, and refusal to give up her own self worth turns out to be a poor pawn for their plan. It's not that Afi is unwilling to follow their plan, but she is her own woman and refuses to bow to the whims of her selfish benefactor mother in law. It's a bittersweet ending in some ways, but also immensely enjoyable since Afi accomplishes her greatest dreams.

### The Keeper of Lost Things
#### Book; Author: Ruth Hogan; Narrators: Jane Collingwood, Sandra Duncan; Published: 2017; Read on: 2020-10-19

We are connected to so many different lives through very mundane and every day threads. Hogan ties together two narratives in this novel; a writer and a publisher, and the loved ones who care for them in their old age and carry our each of their last wishes. Interspersed between these two stories are the lost items the famous writer collects and the unknown stories of their previous owners. This book was another random grab from Hoopla. It's good and I enjoyed it, but I don't have much to recommend it in particular above other novels in my list this year.

### The Wings Of Fire
#### Author: Tui T Sutherland; Narrator: Shannon McManus; Series:

* The Dragonet Prophecy; Audiobook; Published: 2012; Read on: 2020-10-03
* The Lost Heir; Audiobook; Published: 2016; Read on: 2020-10-09
* The Hidden Kingdom; Audiobook; Published: 2016; Read on: 2020-10-31
* The Dark Secret; Audiobook; Published: 2013; Read on: 2020-11-04
* The Brightest Night; Audiobook; Published: 2016; Read on: 2020-11-07
* Moon Rising; Audiobook; Published: 2016; Read on: 2020-11-15
* Winter Turning; Audiobook; Published: 2016; Read on: 2020-11-20
* Escaping Peril; Audiobook; Published: 2016; Read on: 2020-12-06
* Talons of Power; Audiobook; Published: 2016; Read on: 2020-12-14
* Darkness of Dragon; Audiobook; Published: 2017; Read on: 2020-12-18
* The Lost Continent; Audiobook; Published: 2018; Read on: 2020-12-23
* The Hive Queen; Audiobook; Published: 2018; Read on: 2020-12-28

Wow. I didn't realize that I've gone through all these books in just three months. This series is immensely good. The target audience is very much children in primary school, but at this point this series is my top epic fantasy. Sutherland writes each book from the perspective of another character, and the running theme of compassion and empathy for those different than you routinely touches my heart and makes me consider my own practices of compassion. Another of the themes that runs through this series as various children save the world time and time again is family. What it looks like, how do you handle when your parents don't live up to your expectations, and finding love and support in unexpected places. On top of adventure, magic, prophecy, and several murderous queens. Oh and all the characters are talking dragons. What's not to love?!

### Scary Stories Audio Collection
#### Audiobook; Author: Alvin Schwartz; Narrator: George S. Irving; Published: 2019; Read on: 2020-11-24

I adored these stories as a child. I had all three books, read them all cover to cover multiple times, and even memorized several of the poems. If you've never read these stories you need to check this out. Schwartz collected various folk scary stories from across the United States and compiled them. Some are very clearly campfire stories, others are games to played in a classroom around Halloween, all of them are delightfully spooky and sometimes creepy.


### The New Jim Crow
#### Audiobook; Author: Michille Alexander; Narrator: Karen Chilton; Published: 2012; Read on: 2020-11-25

An excellent examination of the power dynamics behind our justice system. What really stuck with me the most however was the authors preface to a new edition published this year, pointing out how a lot of these power dynamics have shifted to putting migrants in dentention centers as the publicity of jailing Black Americans has gotten worse and worse. A must read for any European American folk wishing to do their part to cease the injustice in their communities.

### The Tea Dragon Society
#### Graphic Novel; Author & Illustrator: Katie O'Neill; Published: 2019; Read on: 2020-12-01

This short graphic novel is absolutely delightful! It's a great afternoon breather from other work, going on a journey through a fantastic world where dragons have grown and developed into something like housecats. Tempermental and picky, care of tea dragons requires a dedicated owner. Once the dragon has bonded to an owner, the leaves that grow on their horns can be used to brew a tea that allows any drinker to experience the memories that dragons shared with their owners.

### Animorphs
#### Author: K.A. Applegate; Series:
* Book One: The Invasion; Audiobook; Narrator: MacLeod Andrews; Published: 2020/1996; Read on: 2020-12-02
* Book Two: The Visitor; Audiobook; Narrator: Emily Elliot; Published: 2020/1996; Read on: 2020-12-10

I LOVED this series as a kid. I devoured them in middle school, so I'm beyond thrilled that Scholastic is producing an audiobook run of them. Much like the Wings Of Fire series each book takes the perspective of a different character (although if I remember correctly the Animorph series has multiple books in the perspective of each character). I forgot how short the Animorphs books are however. These two audiobooks ran about three hours each, so I finished them each in an afternoon. It's fun to return to your childhood media and examine it fresh from your adult perspective. It's fascinating to read them as an adult and see in what ways they've been influenced by the culture of the 90's, as well as having the opportunity to compared them to Applegate's more recent work The One And Only Ivan. I really love the Animorphs series, but if you are going to read only one of Applegate's books, read The One And Only Ivan.

### Cassio
#### Author & Illustrator: Stephen Desberg; Series:
* Vol. 1: The First Assassin; Graphic Novel; Published: 2018; Read on: 2020-12-10
* Vol. 2: Second to Strike; Graphic Novel; Published: 2018; Read on: 2020-12-25

Another book that I picked at a random recommendation of Hoopla. Hoopla has a really nice comic viewer that zooms in on individual panels and lets you progress through the page. I think it's my favorite way to read comic art honestly. The series follows both the archaeologist Ornella Grazzi as she discovers a tomb that holds clues to the mysterious murder of a prominent Roman lawyer from the second century C.E. and the lawyer Cassio who somehow has mysterious powers to heal. As Ornella continues her research and attempts to uncover the idenitity of the four assassins who murdered Cassio she finds herself slipping deeper into the midst of powerful and violent people who are desperate to get their hands on something Cassio left behind.

### The Horn, The Pencil & The Ace of Diamons
#### Book; Author: Edgar Mason; Published: 2020; Read on: 2020-12-27

I really loved this novella. It has the best quality of all good novellas; which is that something happens, its a little bizarre, and you have to make sense of it. Five people tell you a bit about what happened, two perish, and maybe Carrie saved the world. It's a delightful afternoon journey that I highly recommend. It's the first published novella of a dear friend of mine, and you should definitely go [pick up a copy](https://broken-hands-media.itch.io/the-horn-the-pencil-and-the-ace-of-diamonds).

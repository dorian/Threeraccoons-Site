---
date: 2021-03-23T20:57:32-04:00
title: "Tuesday's Accomplishments"
draft: true
categories:
- blog
---

Today I accomplished:
1. We built a huglekultur in our yard! Gonna attempt to grow some food stuff this year!
2. Watched more stats videos
3. Previewed the next volume of the sysadmin books. I need to get some notes ready about the last one and then get cracking.

Next time I would like to do better on:
1. Late morning I really struggled with waiting for our afternoon activities. I had empty time and was stressed about figuring out what to do in it.

Tomorrow I would like to:
1. Do my taxes! I've been putting those off.
2. Set up more things on my T500 properly.
3. Therapy!

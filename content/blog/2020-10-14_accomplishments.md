+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-10-14"
draft = "False"
categories = ["blog"]
+++
Today I accomplished:
1. Drove Grandpa to his appointment
1. Meditated! Day 4 in a row!
1. Reworked basically the entire script for this! Made my first functions in BASH!
1. Signed up for Stoic Week
1. Confirmed a date for the classics alum meeting

Next time I would like to do better on:
1. I did a really good job at taking a break when I needed to today. I would like to express my own feelings more in the future.

Tomorrow I would like to:
1. Send an email out to the alum mailing list informing people of the meeting date next week.
1. Attend Ben's workshop through the Classics dept.
1. See if I can work out Lute's oneliner for streams
1. Start looking into setting up a mail server

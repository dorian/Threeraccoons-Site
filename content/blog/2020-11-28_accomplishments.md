+++
title = "2020-11-28's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-11-28"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Made a checklist for all the components for a mastodon server, as well as the steps needed to accomplish it.
1. Made progress on a sock.
1. Took Grandma for a ride.
1. Organized files on my harddrive, backed up some stuff, I now have more space!
1. Sent pictures of my grandpa's new finish on a cabinet to my family, lots of supportive communication ensued. Good times.

Next time I would like to do better on:
1. Not take Grandma's jokes about how it's a burden to care for her so seriously.

Tomorrow I would like to:
1. Finish writing down steps for a SMTP server.
1. Finish sock (I'm almost done), start a new one
2. research vim actions to replace responsegatherer.sh

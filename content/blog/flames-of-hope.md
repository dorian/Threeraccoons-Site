+++
title = "The Flames of Hope"
author = ["Dorian Wood"]
date = 2022-05-21T19:05:00-04:00
lastmod = 2022-05-21T19:05:15-04:00
tags = ["book-review"]
draft = false
weight = 3001
+++

## Info {#info}

Author: Tui T. Sutherland

Narrator: Shannon McManus

Published: 2022

Started on: 2022-05-11

Finished on: 2022-05-16

Link: [The Wings of Fire: The Flames of Hope](https://www.hoopladigital.com/title/14805759)


## Review {#review}

I love this series so much. I honestly cannot recommend it enough. If you enjoy fantasy at all, and are willing to read books aimed at primary school aged children, this series is a must read.

This is the conclusion of The Lost Continent arc. I really enjoyed the way that Sutherland wraps in humans in this arc. The previous struggles had all been about conflicts between dragon tribes, but this one touches upon a time long ago when humans shared the land with dragons, and how poorly they treated dragons.

I don't want to spoil the plot of the book, and I really don't have much to say beyond that. I liked this book as much as I liked all the rest, check it out. <3

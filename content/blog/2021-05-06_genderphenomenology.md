---
title: "Gender Phenomenology Thoughts"
date: 2021-05-06T21:44:02-04:00
draft: false
categories:
- blog
---
This is not polished at all. Just some rambly thoughts stemming from this video.
[Jordan Peterson's Ideology](https://www.youtube.com/watch?v=m81q-ZkfBm0)

Just watched "Jordan Peterson's Ideology" by Abigail Thorn. Really good. Made me excited about phenomenology and structuralism again. But early on she talked about phenomenology of gender and I found that really interesting. In the early days of her transition it was obvious that people saw her as a man attempting to be a woman. With all of the social struggles that come with that. But there was a point, where people stopped hesitating to call her miss, where it was obvious they no longer had to second guess and it just came naturally. She became a woman to them. So where does gender lie? Is it something that adheres inside of Abigail? Is her gender something that is determined by other people? Do they validate and confirm her gender? Is gender something that emerges from social interaction, of broadcast and reception of social signals? I don't think I have a gender that adheres in the physical material (I include the consciousness in that) of this being at all. It's an emergent property that comes about when this being engages with other humans. Our existence uniquely plays with and against that mass of ideas, tropes, and stereotypes about gender and behavior that we all carry because of our mutual existence inside of this society. And our unique expression and play with that cultural ether will broadcast particular signals that are entirely dependant upon that ether. They would not exist without it. Where culture, and thereby that mass of collective ideas and stories, is different, so too will those messages will be different. Our gender is a social thing, not a part of us but rather a socially emergent representation of our unique personhood.

---
title: "Setting Emacs Variables"
date: 2021-06-01T10:03:02-04:00
draft: false
categories:
- article
---

So I've been struggling with learning how to configure Emacs. Emacs has that really nice customize function, but that doesn't really help me understand how variables are being set under the hood. On top of that I'm using Doom Emacs, which doesn't support customize anyways. Today I have achieved a small victory.

I found it! I finally found it and even that was just happening to stumble upon what I wanted! I'm pretty excited over this haha.

## The describe functions
Emacs itself tells you about the describe functions, but it has taken me awhile to get used to exploring them. You can see a list of them by pressing M-x, typing in "describe", and pressing tab twice to bring up completion. Today I finally realized that the one I needed was `describe-variable`.

I wanted to change the directories that elfeed and elfeed-org used for my RSS feeds, so running describe-variable and searching for elfeed showed me the list of variables with elfeed in the name. I want to move the elfeed-org list of feed, and searching brought up the rmh-elfeed-org-files variable. Hit enter on it and it pulls up documentation on the individual variable.

I've looked at documentation through this method before but I could never quite find what I wanted from it. This time I noticed that there's a "Set" link in the documentation, and so I click on it. That activates the minibuffer with eval and the symbol used to set the variable! Success! The code that gets pulled up is the code you can put in whatever file you save your configs in.

## Conclusion
I'm so excited that I've finally groked a little bit more about how to investigate Emacs variables and manipulate them more easily!

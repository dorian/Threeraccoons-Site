---
date: 2021-05-03T16:27:16-04:00
title: "Monday's Accomplishments"
draft: false
categories:
- blog
---

For the past three days.

Today I accomplished:
1. Recorded music with friends! John and his wife came and recorded music with us! It was great fun!
1. Had a family get together for my sister's birthday! Several hours spent outside visiting with family. It's been awhile since we've been able to be together for that long.
1. Planted stuff in the garden! We'll see if anything grows.

Next time I would like to do better on:
1. Planning and Organization. I'm realizing I need to come up with some core projects, similar to core values, that I'm committed to making consistent progress on.

Tomorrow I would like to:
1. Work on fixing the Mastodon server.
1. Therapy!

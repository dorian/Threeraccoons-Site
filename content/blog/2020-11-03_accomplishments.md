+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-11-03"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Raked leaves in the yard. Got nearly the whole yard done.
1. Practiced Ukulele
1. Took Grandma for a ride
1. Wrote 900 words for nanowrimo

Next time I would like to do better on:
1. Focus better when I have time to work on a project. Minimize distractions.

Tomorrow I would like to:
1. The FCC product project. Still haven't done that.
1. Explore a conceptual map for privacy
1. Nanowrimo writting

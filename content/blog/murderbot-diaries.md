+++
title = "The Murderbot Diaries"
author = ["Dorian Wood"]
date = 2021-07-17T09:31:00-04:00
lastmod = 2021-12-19T20:47:27-05:00
tags = ["book-review"]
draft = false
weight = 3017
+++

Author: Martha Wells

Narrator:Kevin R. Free

Published: 2017 - 2020

Titles:

All Systems Red; Read on 2021-07-11

Artificial Condition; Read on 2021-07-12

Rogue Protocol; Read on 2021-07-13

Exit Strategy; Read on 2021-07-14

Network Effect; Read on 2021-07-17

I DEVOURED this series. It's so much fun I really can't recommend it enough. Murderbot is such a likable character. Wells' does a great job of making Murderbot seem like an inhuman machine, but one that is nevertheless still a person. And Murderbot isn't really all that inhuman, it has organic parts after all, and shares the same bodily configuration as humans. My favorite recurring line is how much Murderbot hates it when it leaks. The most interesting part of the entire series so far is watching Murderbot develop emotional skills. First as it develops the social skills to interact with humans who see it as an autonomous and sentient being rather than a tool, and then with another sentient bot. The series is absolutely delightful, but it is also a very interesting exploration of what it would be like to have access to a great deal of information and technical skill, and yet very few socio-emotional skills.

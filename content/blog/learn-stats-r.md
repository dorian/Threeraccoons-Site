+++
title = "Learning Statistics With R"
author = ["Dorian Wood"]
date = 2021-09-03T09:28:00-04:00
lastmod = 2021-12-19T20:47:24-05:00
tags = ["book-review"]
draft = false
weight = 3011
+++

Author: Danielle Navarro

Published: 2019

Read on: 2021-09-03

I really loved this textbook. It's really easy to read (as easy as that math can be anyways), and playing with R and doing stats is just plain fun. I'm already working on projects to put these skills to use and hopefully expand them further. If you want a good intro to statistics and some software to do them, this is a great (and free) place to start.

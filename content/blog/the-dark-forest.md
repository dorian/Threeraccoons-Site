+++
title = "The Dark Forest"
author = ["Dorian Wood"]
date = 2021-12-08T17:06:00-05:00
lastmod = 2021-12-19T20:47:21-05:00
tags = ["book-review"]
draft = false
weight = 3002
+++

## Info {#info}

Author: Cixin Liu

Translator: Joel Martinsen

Narrator: PJ Ochian

Published: 2015

Read on: 2021-12-04


## Review {#review}

Again I am surprised with how much I really enjoy this hard science fiction! Liu sprinkles it in at just the right moments in the midst of the drama of two species at war for their survival.

Liu structures the differences between humans and the Tri-Solarans so fascinatingly. The Tri-Solarans are unfamiliar with any kind of deception and subterfuge, but possess technology far beyond what humanity is capable of. The second novel in the series then centers on how four humans selected can engage trickery and deception against an enemy from whom they can hide only their thoughts. How is it possible to fight against such a foe?

With this plot set up Liu has a great avenue for embedding fascinating drama and suspense in the midst of hard scifi details that only minimally depend on those elements. The final twist of the last human opponent to the Tri-Solarans totally caught me off guard. The foreshadowing of sociological elements as the key to the solution was really fascinating.

I read Cameron's [review of The Three Body Problem](<https://www.brokenhandsmedia.com/blog/2021/11/2/camerons-book-reviews-late-2021>) shortly after I finished that book and was drawn in by his mentioning how this series has a reputation for being very grim. And they certainly live up to that. The second novel does end on something of a positive note, with a truce between humanity and the invading Tri-Solarans for the time being.

Oh! I have to say, the second and third books in this series are LONG! This audiobook clocked in at 22 hours and the third is 28. I think they are the longest audio fiction I've listened to this year. I'm already part way into the next novel in the series and I have to say, it takes a dark turn again fairly quickly. I think I'll need to find something very light hearted after this.

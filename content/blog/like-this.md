+++
title = "Like This"
author = ["Dorian Wood"]
lastmod = 2022-07-11T16:49:53-04:00
tags = ["book-review"]
draft = true
weight = 3002
+++

## Info {#info}

Author: Rumi

Narrator: Coleman Barks

Published: 2016

Started on: 2022-07-07

Finished on:

Link: [Like This: More Poems of Rumi](https://www.hoopladigital.com/title/15101193)


## Review {#review}

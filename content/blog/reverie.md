+++
title = "Reverie"
author = ["Dorian Wood"]
date = 2021-08-11T09:29:00-04:00
lastmod = 2021-12-19T20:47:25-05:00
tags = ["book-review"]
draft = false
weight = 3014
+++

Author: Ryan La Sala

Narrator: Michael Crouch

Published: 2020

Read on: 2021-08-11

Spoilers Heavy

This novel begin in media res, but neither you nor the main character are aware of that in the beginning. Which I found a really fascinating opening to the story. There are machinations and power struggles going on that you and the main character are in the dark about. I also really enjoyed the presence of gay perspectives and opinions that ran through the novel, going beyond just the main character's sexuality. Also! Gay kissing! Very wow, much good.

Traveling through the mystical worlds of people dreams, and the super powers that our main cast exercise in those worlds was really delightful. I really enjoyed the magical world building. The power of hopes and dreams manifest into reality. The good and danger that those things bring.

---
title: "The Linksys WRT54G: Free Software Builds Better Tools"
date: 2021-01-26T14:21:36-05:00
draft: false
categories:
- article
---

## Summary

The Linksys WRT54G is a legendary router of sorts in the free software community, as a powerful tool at a reasonable price point.
This is largely due to the fact that it runs free software, meaning end users have the freedom to modify it.
This gives them the ability to make it better suit their needs, and even add new functionality.

## A Fortunate Trojan Horse

I'm not going to cover the history of the device as it has been done quite well [here](https://thenewstack.io/the-open-source-lesson-of-the-linksys-wrt54g-router/), but rather I'd like to talk about how free software can result in better tools.
Various levels of outsourcing resulted in Linksys, and thus later Cisco, selling hardware that has GPL liscensed code in its firmware.
This meant that Cisco was legally required to release the code used in the firmware to customers who bought the WRT54G.
Anyone with the prequisite skills could examine the firmware, and easily make modifications or extend it any way they wanted.
Users were now limited by nothing more than the raw technical limitations of the hardware.

## From Trade Secrets to Hobbyist Dream

Typically this kind of thing is avoided by manufacturers, hence why the story of the Linksys WRT54G is much of a story at all.
Some insist that such firmware software is part of trade secrets, that companies must keep it secret as part of their strategy to make a profit.
Other stances make the case that software is safer from a security standpoint when software is kept secret, preventing malicious actors from finding flaws and exploiting them.

The success of the WRT54G points to both those not being the case.
This model is still in production and is still for sale today, more than 15 years after it first debuted.
It is popular among computer and electronic hobbyists precisely because of the availability of its source code.
This availability allows intricate modifications, from tweaking its intended function to elaborate project such as turning it into a [wifi enabled robot](https://hackaday.com/2008/10/07/inexpensive-powerful-router-based-robot/).

It even inspired the [DD-WRT](https://dd-wrt.com/) project for expanding the capability and uses of ordinary consumer wifi routers.
That project began on the Linksys WRT54G but now supports a [wide array of hardware](https://wiki.dd-wrt.com/wiki/index.php/Supported_Devices), enabling people to use a wide variety of hardware configured specifically for their unique desires and needs.

## The Lesson

So why is any of this important to talk about?
Because it holds many implications about how we as a society are going to move forward.
How do we want the tools we buy to function?
Are we content that manufacturers can withhold the source code for their firmware, and thus insist that we can only use the tools we bought in the ways they say we should be able to?
I believe the brighter future is the one where we have the option and the power to extend the usefulness of our tools with our own ingenuity, to expand the capabilities of tools and use them in ways the manufacturer never envisioned.

Free software is the mechanism by which we can develop a world where everyone gets to experiment. Where everyone gets to try their ideas out and build something useful (or even just cool). In turn we can all share our experiments, we all will be able to benefit from everyone else's ideas, and we can build computer systems and networks that best serve our communities.

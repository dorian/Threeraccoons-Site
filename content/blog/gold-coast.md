+++
title = "The Gold Coast: Part 2 of the Three Californias Triptych"
author = ["Dorian Wood"]
date = 2022-03-01T18:10:00-05:00
lastmod = 2022-03-09T17:12:27-05:00
tags = ["book-review", "three-californias-triptych"]
draft = false
weight = 3004
+++

## Info {#info}

Author: Kim Stanley Robinson

Series: Part Two of The Three Californias Triptych

Narrator: Stefan Rudnicki

Published: 1988 & 2015

Started on: 2022-01-22

Finished on: 2022-02-14

Link: [The Gold Coast](https://www.hoopladigital.com/title/11337221)


## Review {#review}

This one took me a long time to get into. For pretty much the first half of the book I was not very interested in the characters or their lives. Jim in particular I just found incredibly...boring. Which as I progressed through the book I realized was kind of the point. A major point of drama at the end is what Jim ends up deciding to do given all his listless meandering through life. More in the review of the third and final book of the trilogy, [Pacific Edge: Part 3]({{< relref "pacific-edge.md" >}}).

---
title: "Elisp Diary Functions"
date: 2021-07-07T13:17:33-04:00
draft: false
---

I finally got around to moving porting my bash script for journalling into elisp! The experience of using it is much more pleasant honestly, and the result is something that I had wanted to put together in vim for a long time but I never got around to learning how.

For awhile I've been using automated prompts to assist in my self monitoring records that I keep in my journal. This started when I was attempting to keep a record of my sleep habits, and I realized that recalling the various things I wanted to record required significant effort on top of just writing down the data points. If I had a program to present me with the questions, and then take my response, I could make recording such things significantly easier on my part, and thus improve my habits of consistently recording said data. It works really well for me.

The system I have is I keep a text file with prompts, and read it line by line to prompt for user input and write that data to my daily diary. This made for a great coding project, since I knew the structure I need, all that I need to do is figure out how to do this stuff in elisp.

## It's coding time!
So what I need is:
- read a file line by line
- prompt for user input once per line
- write input in a file
- enter timestamp and other information

### Read a file line by line
This part was easy! I just snagged this function from [ErgoEmacs](http://ergoemacs.org/emacs/elisp_read_file_content.html)
```
(defun read-lines (filePath)
  "Return a list of lines of a file at filePath"
  (with-temp-buffer
    (insert-file-contents filePath)
    (split-string (buffer-string) "\n" t)))
```

### mapcar? mapc?
I've got a list of prompts, and I've skimmed enough elisp docs to know that mapcar is the way to iterate over a list.
I read the docs again and couldn't tell if mapc or mapcar was the function I wanted.
I tried mapc first and when that didn't return what I wanted I tried mapcar and that worked.
mapcar returns the value of the function run.

### Prompt for user input
I used the method for getting user input modeled in this page at [Ergo Emacs](http://ergoemacs.org/emacs/elisp_idioms_prompting_input.html).
It took me a couple tries to figure out that to get the result I wanted from `message` I needed to concat x (the prompt item from the list) with %s, the string that `read-string` returned.

I also had trouble figuring out how to get mapcar to pass the items from the list into my `message` block.
The solution was a simple lambda, so now I finally understand the use of lambda functions!
I find it really interesting how elisp (and I assume lisp in general) separates the logic of iterating over a list from the function performed on each item.
I'm used to using for loops in Bash and Python, where those two things are pretty tightly coupled.

Here's my function for automated collection of self monitoring data!
```
(defun response-gatherer (prompt)
  "Read prompts from file 'prompt' and present them to the user in the minibuffer"
  (interactive)
  (open-diary)
  (evil-insert-newline-below)
  (insert (concat "## " prompt "\n"))
  (time)
  (mapcar
   (lambda (x) (insert
                (message (concat x " %s\n") (read-string (concat x " ")))))
     (read-lines
      (concat "~/wrt/journal/selfMonitoringPrompts/" prompt))))
```

### Bonus: Date stamp!
I forgot about the logic I have in my bash script that ensures I have a date stamp at the top of my journal files.
This was a bit tricky to figure out at first, but it was a good avenue to explore if and unless functions in elisp!
Doing this in emacs also simplifies the code a bit compared to bash, since I don't have to specify the file twice, once for checking for the file and once for writing to it. Just open the buffer, check if the file exists, and insert a date stamp if not.

Here's the function for that bit:
```
(defun open-diary ()
  "Open the diary file for today's date."
  (interactive)
  (find-file (concat "~/wrt/diary/" (today) ".md"))
  (unless (file-exists-p (concat "~/wrt/diary/" (today) ".md"))
    (insert (concat (today) "\n\n")))
  (end-of-buffer))
```

## Conclusion
This was really fun! I finally am feeling more comfortable hacking around in elisp, which is really exciting. I struggle to get my mind wrapped around it for a bit and it was really hard to approach. I should pull the diary directory and prompt directory out into variables to make the functions cleaner/easier to configure sometime.

---
date: 2021-01-09T19:59:30-05:00
title: "Saturday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Work!
2. Worked on the puzzle with Grandpa and Grandma

Next time I would like to do better on:
1. Today was a pretty good day. No obvious places for improvement.

Tomorrow I would like to:
1. Work again!
2. Video call with Family

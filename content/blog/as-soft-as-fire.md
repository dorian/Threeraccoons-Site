+++
title = "as soft as fire"
author = ["Dorian Wood"]
date = 2021-11-24T09:38:00-05:00
lastmod = 2021-12-19T20:47:22-05:00
tags = ["book-review"]
draft = false
weight = 3005
+++

Author: Upile Chisala

Narrator: Upile Chisala

Published: 2020

Read on: 2021-10-29

This is selected poems from several of Chisala's works.
I had read two of them previously, so I was familiar with a fair amount of the poems in this one.
The best part of this is that periodically Chisala chimes in to comment on the situation of her life when she wrote a particular poem, or the themes from her life that it was touching on.
It was a really enjoyable read of her poetry, If you want a place to start with her work I think this is probably it.

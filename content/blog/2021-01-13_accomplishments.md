---
date: 2021-01-13T20:00:57-05:00
title: "Wednesday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Did some premptive blocking on mastodon
2. Went for a walk
3. Worked on a puzzle with Grandma
4. Installed Kodi on Pi4
5. Did exercises with Grandma
6. Practiced Ukulele

Next time I would like to do better on:
1. Not running with cognitive distortions about how Grandpa and Grandma feel about me.
2. Be courageous and vulnerable enough to pull them into some activities they are hesitant about.

Tomorrow I would like to:
1. Finished configuring Kodi on Pi4
2. Set up network file storage on Pi4
3. Read some Foucault (actually going to go do that now, but I also want to do it tomorrow)
4. Work on resume

---
title: "Calmer Email"
date: 2021-03-10T15:00:07-05:00
draft: false
categories:
- blog
---

The other day I read about the increased use of email over the past couple of decades, and how it is having a measurable negative impact on us.

I think one good option to help reduce this is to change the way we use internet communication, specifically to return to an older way of doing things, by using email clients configured to manually check for new messages. Having emails constantly pop up with notifications on phones seems to me to be a big part of their added stress in our lives. And altering our devices such that email is accessed only when specifically requested seems like a really good way to sequester it back into a managable place.

In my own life I've found my most relaxed days are when I've left all devices with push notifications in a separate room. I am still connected, online, and communicating with people, but in the context of intentional communication rather than obtrusive notifications that demand my attention regardless of whatever important thing I happend to be working on at the moment.

Maybe it wouldn't work for some people, or maybe even most people. I still think it's worth an experiment though.

+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-10-30"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Started proper notes on CSS. This helped solidify my knowledge.
1. Finished a sock. Need to start a new one.
1. Took Grandma for a ride.
1. Read some of Dr. Roxanna's recent article.

Next time I would like to do better on:
1. Be more open with my grandfather. Admit that I need to change as well.

Tomorrow I would like to:
1. Start sock, get to heel flap at least
1. Continue to work on FCC product project, along with CSS notes
1. Work with git to properly fork the voidrice repo

+++
title = "Today's Accomplishments"
description = "Got a few things done today"
date = "2020-10-07"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Meditated
2. Drafted an email for the alum org
3. Went for a ride with my Grandparents
4. Made good progress on a sock I'm knitting

I would like to do better:
I would like to make this post the day of, instead of the day after. Because I am struggling to remember what I did. But I'm sure I did more.

Tomorrow I would like to:
1. Send email out to alums
2. Set up Prometheus to monitor my matrix server
3. Fix up my self hosted Element instance

+++
title = "The Heart Cannot Break"
author = ["Dorian Wood"]
date = 2021-10-24T06:37:00-04:00
lastmod = 2021-12-19T20:47:29-05:00
tags = ["poetry"]
draft = false
weight = 3001
+++

The heart cannot split<br>
it simply pours out<br>
that which you fill it with<br>

my heart is a cup<br>
I will pour it out for you if you need<br>
but I too must drink from it<br>

so I fill it with kindness<br>
as much compassion as I can muster<br>
because I need these<br>

I am glad to share<br>
drink from my heart<br>
but I fill it for me<br>

my heart cannot break in two<br>
a wound it may receive<br>
but to itself it must be true<br>

my heart is a whole<br>
whatever it holds for you<br>
it holds for me<br>

perhaps it is strange to say<br>
I will always love you<br>
because I am tired of not loving me<br>

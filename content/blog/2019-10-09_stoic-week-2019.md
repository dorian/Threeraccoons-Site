+++
title = "Stoic Week 2019"
description = "A weeklong course in implementing Stoic practies into daily life."
date = "2019-10-09"
categories = ["blog"]
+++

Here’s a link to the course for Stoic Week. If you’d like to follow along, head over to their website and sign up. https://learn.modernstoicism.com/p/stoic-week

I’m late to the Stoic Week party, but I’m gonna start. As such, I’ll be doing the meditations on incorrect days. Oh well. The theme this year is nature, community, and caring for those things.

My take away from the text for Monday morning is that since virtue promises us happiness and serenity and progress towards virtue is progress towards happiness that mean there are various degrees and qualities of happiness. I find myself fairly often lately wondering if I’m really happy. Trying to judge if I’m doing enough professionally, socially, and personally to achieve what I want. This is perhaps an unhelpful approach to examine my life from. Rather than wondering if I’m really happy, it would help me much more if I can determine if I’m happier. Have I done the things I need and want to do in order to grow as a human being? Being unhappy in a single moment, or questioning if I’m “really” happy, are not helpful questions that give me a path forward.

+++
date = 2020-12-25T00:35:27-05:00
title = "Friday's Accomplishments"
draft = false
categories = ["blog"]
+++

I've neglected this for a bit and so I have accomplishments from over the course of multiple days.

Today I accomplished:
1. Finished a sock! One more to go and I'll have one of my extended family groups done. Then on to the next.
2. Got a mastodon server up and running! I transferred my public account from queer.party over to it. Got some final set up things to finish and then I'm announcing to my fellow alum that it's open for them to join.
3. Had a healing conversation with my Grandfather! My work with my therapist is paying off and that's very exciting.
4. Sketched out a new idea for an article.
5. I got to use a jitsi server I host to hold a Christmas party with my extended family. One positive about the pandmic is my computer skills have given me an opportunity to feel like a useful member of my family in a way that I haven't in a long time.

Next time I would like to do better on:
1. Idk. It's too late for this part.

Tomorrow I would like to:
1. Write email and social media posts announcing mastodon server to fellow alum.
2. Finish next sock.
3. Practice a couple songs on ukulele.

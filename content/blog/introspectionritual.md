---
title: "Ritual for Introspection of the Selves"
date: 2021-04-30T20:09:32-04:00
draft: false
categories:
- article
---

Over the past month or two I have developed this spiritual practice out of a combination of a pagan rituals and Buddhist teaching. There's undoubtedly influence from several other people and traditions in here, but those are the two big sources. The Buddhist teaching that forms the core of this practice for me is the teaching of no self. There is no such thing as "me". There is the collection of sensations from this body in this moment, and there is the collection of thoughts and observations that this consciousness has in this moment. I can say, "I like pineapple," but I can also phrase it just as accurately as, "This body enjoys the sensation of pineapple." I frequently refer to experiences as pertaining to "this being". To explore the variety and depth of my existence without centering the "I, me, mine" of the conscious thinking mind is what this ritual is aimed at.

## This Being
This being is made up of many distinct parts. This being has thoughts and this being has emotions. Emotions aren't the same as thoughts, although they are closely connected and influence each other greatly. This ritual has grown and adapted over time as this being has taken care to notice the ebb and flow of different parts and how they interact.

Here are the distinct parts of the self that we currently focus on. Feel free to use these or come up with your own. This is about an exploration of your selves, so they should feel true for you.
- Body & Emotions
- Conscious thinker
- The capacity and desire for introspection and self examination
- Inner light, source of our being, our compassion
- Creativity and spontaneity
- The Social and outward aspect of our being, our gender
- Our capacity to respond to the unpredictable path of life

## The Altar In Our Room:
This altar began as a place to perform some mirror magic of introspection.
Those rituals didn't immediately offer something rich and deep during their practice, but the huge blessing they did bring us is in simply creating a space that is dedicated to introspection and quiet listening to and communication with our selves.

All items on the altar are found, made, or have spent an extended amount of time with us.
They are familiar and have called to us in some way.
The quest to only use found and second hand items was initially started because it seemed magically appropriate, but it has evolved into primarily a joyful project.
It prompts us to hold attention for selves that we do not yet have represented, to be mindful in everyday moments and listen for when they might call and how they might prompt us to be included.
The altar is a place where we calmly call all the attention of all the selves to the present moment, and express love, compassion, and gratitude for each self in turn.

Having external objects to touch and visualize as aspects of ourself has given us new tools to manage distress in this being.
It is easier to identify where distress is when it occurs, and to ask the conscious thinker to refrain from running anxiously and participating in destructive thought patterns because of the distress of another aspect.
Being able to more quickly identify distress has also mean being more able to sooth and care for it.

## The Ritual
The ritual is performed by approaching the altar and calmly lighting a candle.
For us the candle represents our inner light and compassion, that which is our most core and true self/aspect.
Next we touch one of the objects and call the attention of all selves to the self that is represented by that object.
We usually start with the body and emotions, the foundation of our being.
We take time to feel the body, and to feel the emotions.
Thanking them for their work, for their messages and their wonderful participation in this being's life.
Then we proceed to do the same with the next object and the self that it respresents.
We don't have a formal fixed order of objects or words for this ritual.
Whatever surfaces in the moment is what we go with.

## The Objects
+ A rock.
We picked this rock up when walking in the yard with Grandma one day.
It way lying there from the garden plot we had worked a week earlier.
We noticed it out of nowhere, and so it seemed and excellent candidate for our altar, it had called to us.
It is representative of our body and emotions: the foundational things in us.
It may not suit everyone, but we have found an intuitive connection between the body self and the emotional self.
The body self seems to be the one who feels emotions most truly.
+ A voice recorder.
We bought this item second hand a few years ago to use to record our thoughts and ideas.
We don't currently use it actively so it's a good fit to inhabit the space of our conscious thinker in the altar.
+ A mirror.
This mirror is a makeup mirror that Grandma used.
We snagged it initially to play with some mirror magic.
It represents this being's capacity for introspection and self awareness.
+ A candle.
This candle was initially used for some mirror magic, and has since evolved to represent our inner light, the energy that is uniquely ours and fuels our being.
Having a mental image of the object as a focal point allows us to use it as we go throughout the day, to call to mind our life goals and drives, and to explore if current actions are aligning with them.
+ A doily.
I made this doily two years ago for an art show for Trans Day Of Visibility.
It was crafted to represent my gender.
And so now here it represents our gender, those external, viewable, social aspects of our being.
+ Spoons!
We bought these at a goodwill a few years ago because they looked pretty and they had a nice timbre for playing music!
They respresent our spontenaity and creativiy.
+ A tarot deck.
The only item I bought new on the altar, but it's been with me for five or six years so it has my energy now.
The randomness of the cards is like the randomness of life.
Life will take it's many turns, but we always have the power to interpret it how we will, and to act accordingly.

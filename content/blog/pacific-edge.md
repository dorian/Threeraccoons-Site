+++
title = "Pacific Edge"
author = ["Dorian Wood"]
date = 2022-03-02T10:21:00-05:00
lastmod = 2022-03-05T22:07:43-05:00
tags = ["book-review", "three-californias-triptych"]
draft = false
weight = 3002
+++

## Info {#info}

Author: Kim Stanley Robinson

Series: Part Three of The Three Californias Triptych

Narrator: Stefan Rudnicki

Published: 1990 & 2015

Finished on: 2022-03-01

Link: [Pacific Edge](https://www.hoopladigital.com/title/11269558)


## Review {#review}

Much like the second book in the trilogy, I had some trouble getting into this novel. Although I found the setting and characters far more interesting sooner. Really, just like all the other novels in this triptych, the best part of them is in their ending. In seeing the arc of the story and being able to judge what the journey of the characters mean.

I quite liked this California. It's a utopia, people have their needs met, everyone is provided for. And yet it's not boring, there are still the regular human struggles. Love triangles, disagreements about city land use, a lucky streak batting at the softball game. In particular at the end was the best part of the entire series I think. Robinson admonishes us as readers, if we want utopia we have to fight for it. Not just dream it, think of what ifs and day dream ideal scenarios. We have to fight for it, get involved in our communities, move the society of which we are a part towards a better tomorrow. We have to hope, and try.

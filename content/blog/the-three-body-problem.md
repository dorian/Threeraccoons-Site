+++
title = "The Three Body Problem"
author = ["Dorian Wood"]
date = 2021-11-24T09:37:00-05:00
lastmod = 2021-12-19T20:47:21-05:00
tags = ["book-review"]
draft = false
weight = 3003
+++

## Info {#info}

Author: Cixin Liu

Translator: Ken Liu

Narrator: Luke Daniels

Published: 2014

Read on: 2021-11-23


## Review {#review}

This was a really fascinating read!
I'm normally not one much for hard science fiction, but Liu does it really well.
It's a delightful mix of physics and the social lives of physicists who research in both applied and theoretical realms.

I really enjoyed the view of academics during the cultural revolution in China.
What it takes to make it through times like that, and the role of integrity and faith in that struggle.

The VR game in the novel is so much fun!
It's such an interesting way to introduce an alien race, and I particularly like how it is a plot device to introduce the characters to necessary information while also introducing me as the reader to these things.

I also really like the perspective of the detective, the know nothing common man who relies upon that to avoid utter despair that scientists had been powerless to combat. The conclusion at the end is delightful. I'm very excited to read the next novel!

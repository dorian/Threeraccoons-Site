+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-11-02"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Finished Dr. Duntley-Matos' article
1. Wrote 1600 words for nanowrimo
1. Raked leaves
1. Took Grandma for a ride

Next time I would like to do better on:
1. Use my time more wisely early in the day

Tomorrow I would like to:
1. Write another 1500 words
1. Finished FCC Product project
1. Therapy

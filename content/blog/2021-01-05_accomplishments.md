---
date: 2021-01-05T20:46:45-05:00
title: "Tuesday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Had therapy!
2. Went for a walk and meditated!
3. Attempted to install Arch on my old desktop
4. Got my new pi4 and attempted to install raspbian
5. Cleaned the center island in the kitchen!
6. Read some of the Sysadmin vol. 1 I've been working on

Next time I would like to do better on:
1. Spending time with and engaging Grandma more (playing cards, etc)
2. I didn't write much in my journal today, I think that has me a little discombobulated

Tomorrow I would like to:
1. Get arch installed on the desktop and set up virtual box so I can continue with the Sysadmin book and exercises
2. Start a new sock
3. Go for a walk
4. Write in my journal more.

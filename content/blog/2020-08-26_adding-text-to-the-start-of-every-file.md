+++
title = "Adding Text To The Start Of Each File"
description = "A shell oneliner to modify files."
date = "2020-08-26"
categories = ["article"]
+++

So I just accomplished a really neat task using a for loop in a shell one liner, and I figured I’d share what I did with you all.

I keep my diary in plain text files on my computer, and today I decided I’d like to convert from managing those files in a folder on my own to Vimwiki. Vimwiki has it’s own built-in diary functions, which have turned out to be a great addition to the diary system I already had.

I didn’t have to alter my naming scheme at all, since I’ve been using Markdown .md extensions and I’ve configured Vimwiki to use the same. What I did have to change is that in order to get Vimwiki to name links properly in the diary index I need to add the filename (the date) of each entry as a level one header on the first line. Previously I had just named each file as YYYY-MM-DD.md and then added whatever to the file.

I used this command to extract the date from each filename, and add that to the first line of the file, shifting everything else down one line.

```
for i in $(ls -p | grep -v /); do sed -i "ls/^/# ${i%%.md}/" $i; done
```

Honestly I’m writing this whole post because I am so in love with that one liner! It’s beautiful 😍.

Let’s break it down:

```
for i in $(ls -p | grep -v /);
```

This part establishes a loop that will iterate over every file.
The `$(ls -p | grep -v /)` is a command substitution to filter out any directories, see my post [here](How-To-Play-A-Random-Video-From-The-Command-Line.html) for further details on that.

```
sed -i "1s/^/# ${i%%.md}\n/";
```

The -i flag tells sed to edit the files in place, and make sure the line addressing that comes next operates on each file individually.
1s (That’s one, not L) is Line Addressing and tells sed to do this operation on line 1 in each file.
^ is a pattern for matching sed to the beginning of the line, and then substitutes it with the next pattern.

```
${i%%.md}\n
```

This part utilizes Parameter Expansion (Bash Zsh.)
Curly braces (\${}) signifies parameter expansion to the shell. The expansion above expands \$i (the filename in this case), and uses the operator %% to match the following pattern to the end of the expanded parameter, and delete that portion.
In this case, the pattern it matches and deletes is the “.md” extension from each file, the result is the date of the file to write on the first line.
Lastly I tossed in a newline character (\n) to shift the rest of what was on the first line down to the second.

And there you have it! A one-liner for loop to add the filename sans extension to the first line of each file. What a feat.

---
date: 2021-04-04T19:44:57-04:00
title: "Sunday's Accomplishments"
draft: false
categories:
- blog
---
This is for the past week or so.

Today I accomplished:
1. Built a second huglekulture in the yard!
2. Recorded more songs with Grandma.
3. Started Volume 2 of Sysadmin series of books.

Next time I would like to do better on:
1. Be more conscious of my thoughts and emotions in a way that is systems aware. I.E. be less ego focused and turn thoughts and emotions towards a focus on how they can be communication to aid in balancing the system.

Tomorrow I would like to:
1. Work.
2. Continue Vol 2.
3. Knit some. I really need to finish this sock.

+++
title = "Update and New Project"
description = "I've been busy with some projects, and I think it would help me to communicate about them more."
date = "2020-09-24"
draft = "False"
categories = ["blog"]
+++

I have been working hard on various projects recently, and despite all my effort I frequently find myself feeling like I'm just not accomplishing anything, and feeling like I am just wasting all my time. In order to cope with that and hopefully structure myself to not only feel better about my efforts but also to accomplish more, I decided that I'm going to begin posting regular updates here.

Here is my current structure for regular updates:
1. Report on what I accomplished today, no matter how small.
2. Reflect on something that I would like to do better with next time.
3. One thing I would like to accomplish next.

I've kept it really short because I think in order to get myself to keep up with this it will need to be something I can do very easily. So here's a brief entry for yesterday.

1. I accomplished:
	1. Practiced ukulele
	2. Tested out using the Mosh for Chrome extension to offer shell access to students to an educational tilde server. I don't think it will work.
2. I would like to do better next time:
	1. Not interpret my Grandmother's indecisiveness as a rejection or dislike of me. Rationally I know that's not the case. So I should not allow my mind to run off with feelings as if it were.
	2. I want to work on my planning skills. That's a broad and ongoing project.
3. Next I would like to accomplish:
	1. I want to look into ikiwiki, and see about using it.
	2. Set up my website server to be suitable for being a tilde server. This really just means cleaning up a couple things, setting up the nginx to serve user public_html dirs as sites, and then letting people know.
	3. Develop lesson plans for teaching basic of how to make a website with shell access.

Well. That's it. I think this is a useful format for both reminding myself of what I've done but also directing my energies going forward. Or at least I hope so.

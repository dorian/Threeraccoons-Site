+++
title = "Earth Abides"
author = ["Dorian Wood"]
date = 2022-01-09T10:11:00-05:00
lastmod = 2022-01-09T10:11:46-05:00
tags = ["book-review"]
draft = false
weight = 3001
+++

## Info {#info}

Author: George R Stewart

Narrator: Tim Pabon

Published: 1949

Read on: 2022-01-09

Link: [Earth Abides](https://www.hoopladigital.com/title/14546542)


## Review {#review}

I enjoyed this book far more than I thought I would! It has a really interesting vibe to it. It does properly belong to post apocalyptic sci-fi as a genre, but at the same time it occupies its own space there. Published in 1949, the genre as we understand it was not yet well defined. As such the whole vibe of the book has a much more exploratory feel of speculative fiction than a gritty post apocalypse adventure.

We get the narrative from Ish's perspective, a geographer who had retreated to a cabin up the mountain when a pandemic wiped out most of the human race. He returns to civilization to find nearly everyone gone. Ish sets about living in this new empty world. For awhile the water and electricity still work, but nothing is forever. Ish meets a woman, they gather with a few others who survived. They have children, life goes on. Yet Ish is occupied by that great loss of civilization. What of all the generations who will come after him? How will they live? What will they suffer having lost all the knowledge, tradition, and infrastructure of the human race's domination of the world?

In this is the theme of the whole book; "What is civilization?" What constitutes its parts, and can humans get along without it? Or will they return to lives of suffering, doomed to repeat all the same mistakes as our ancestors in order to gain the same knowledge?

Ish's opinion of civilization is distinct from my own, and much of what he finds important I thought not so much. Nevertheless it was a fascinating journey to go on with him, and I much admired him for his efforts to provide for his descendants the things necessary for a happy and fruitful life.

I was quite moved at the end of the book, where we see Ish in his old age. Dementia gradually takes him, and yet we are constrained to his view. As he loses grasp of what is happening with the tribe, so do we. It is an unnerving experience. To have once been so in command of our faculties and the world we inhabit only to slowly lose it, and to only be barely aware of that loss.

I find this book bordering on a must read for me. It is a great tale of retrospection on what this civilization is, and what might go on after us. Pause and ponder what you would like to leave for those humans that go about living after a collapse of all that makes this life possible.

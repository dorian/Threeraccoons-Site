+++
title = "A Tour Of My Porch"
author = ["Dorian Wood"]
date = 2022-07-26T15:26:00-04:00
lastmod = 2022-07-26T15:26:35-04:00
tags = ["permaculture"]
draft = false
weight = 3002
+++

I was sitting on my porch last night and I was just overjoyed at all the plant life I've got growing out here. So I figured I would snap some pics and write up a little tour.

So here we go! First up is the left half of my porch.

{{< figure src="/images/20220725_212336_web.jpg" >}}

First up is a grape vine! I really need to find this gal a forever home.

{{< figure src="/images/20220725_212456_web.jpg" >}}

I bought a bunch of these purple self watering tubs at the flea market a month ago, they've been working really well! I've got two different kinds of yarrow growing in here (not very visible because their blooms are old and out of frame), a golden shrimp shrub, and a yellow portuluca.

{{< figure src="/images/20220725_212450_web.jpg" >}}

This is the first planter I got set up out here this year, it's just a plastic 5 gallon bucket that I drilled some drain holes in, and planted some Coelus, a pelargonium citrosum, and a lemon balm! It's all grown up so bushy and I love it so much! The pelargonium releases a wonderful citrus smells when you brush or bruise its leaves. I'm attempting to propagate both the pelargonium and the coelus from cuttings currently.

{{< figure src="/images/20220725_212423_web.jpg" >}}

Up next is two more plants I need to find forever homes for! On the left is a Aquilegia I bought at a greenhouse, this cultivar is called "Kirigami" and has beautiful purple flowers. Next to it is an English Lavender I bought last week, I'm going to move some of my hostas in front of the porch and put this fellow in sometime.

{{< figure src="/images/20220725_212405_web.jpg" >}}

The other half of the porch!

{{< figure src="/images/20220725_212347_web.jpg" >}}

We've been growing radish greens to add to our meals! It's super easy, and this is round two of sowing already. We just sprinkle the radish seeds in, and in a week they are ready to pick and add to our meals!

{{< figure src="/images/20220725_212234_web.jpg" >}}

We had an extra self watering bucket so we just tossed in some flower seeds. It was kind of late in the season when we did this, so we might not see any of these flower, but it's worth a shot.

{{< figure src="/images/20220725_212225_web.jpg" >}}

On the left here is a house plant that I really need to move back inside. On the right is a cutting of my pelargonium that I'm trying to propagate. It seems to be doing well, I'm gonna find it a home sometime soonish.

{{< figure src="/images/20220725_212150_web.jpg" >}}

Here's some mason jars where I'm cultivating several lactobacillus cultures! You can read more about that [here]({{< relref "lactobacillus-culture.md" >}}). The two on the left are some fresh cultures I'm starting, and the one on the right is an attempt to see if I can take the culture I grew a couple weeks ago and multiply it by feeding it milk.

{{< figure src="/images/20220725_212136_web.jpg" >}}

Next up is the reason I'm cultivating lactobacillus, a bokashi bucket! I've got this fellow chock full of food waste and other organic material, and I added a generous helping of lactobacillus to break it down. In a day or so I will need to remove the contents and move them to the yard somewhere so they can finish composting.

{{< figure src="/images/20220725_212127_web.jpg" >}}

Here's a Coelus that I trimmed the other day, and my attempts to propagate the cuttings in a mason jar.

{{< figure src="/images/20220725_212311_web.jpg" >}}

A planter with several kinds of succulents in it. I need to move these folks back inside, since the Aloe seems to dislike how much sun she gets on the porch.

{{< figure src="/images/20220725_212251_web.jpg" >}}

Now for the petunias! First up is a veined cultivar that we got so we can mix it with another veined one we already have.

{{< figure src="/images/20220725_212217_web.jpg" >}}

We picked up these two stripped varieties at a greenhouse last week.

{{< figure src="/images/20220725_212412_web.jpg" >}}

This hanging basket is Pete! We've been collecting his seeds and using the trimmings in a vase inside the house, fresh cut flowers!

{{< figure src="/images/20220725_212428_web.jpg" >}}

Last plant is Wirt the Spiderwort! He's been really easy to take care of, and incredibly easy to propagate as well.

{{< figure src="/images/20220725_212507_web.jpg" >}}

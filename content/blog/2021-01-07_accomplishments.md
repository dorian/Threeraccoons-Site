---
date: 2021-01-07T19:57:59-05:00
title: "Thursday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Set up Virtualbox and created a VM for the SysAdmin course
2. Worked on stuff for applying to grad school
3. Renewed my car plates
4. Finished "Wishful Drinking" (and hopefully Cassio Vol 4 after this)
5. Started knitting another sock
6. Watched Homeward Bound with my grandparents
7. Went for a walk
8. Had some productive and healing conversations in a group chat

Next time I would like to do better on:
1. idk. It was a good day.

Tomorrow I would like to:
1. Install Fedora on the VM and complete the next chapter of the SysAdmin course
2. Work on knitting sock
3. Intall arch on pi, maybe set up OSMC on it as well

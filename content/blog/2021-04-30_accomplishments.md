---
date: 2021-04-30T09:33:53-04:00
title: "Friday's Accomplishments"
draft: false
categories:
- blog
---
For today and yesterday.

Today I accomplished:
1. Migrated my Mastodon server to Hometown
2. Took care of some mailing list work
3. Arranged a recording session for the music project
4. Got blood work up done
5. Played cards with family
6. Played some music with Grandma

Next time I would like to do better on:
1. Organization. I need to tweak my organizing routines a little.

Tomorrow I would like to:
1. Record music with John.
2. Attempt to solve mastodon web service issue.
3. Hammer out some better block scheduling routines

+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-10-21"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Completed the first project for FCC's responsive web design cert
1. Took Grandma and Grandpa for a ride
1. Finished a puzzle with Grandpa
1. Finished the heel turn on a sock (I'm gonna knit a bit more before bed as well

Next time I would like to do better on:
1. To continue to minimize my mental narratives of self hatred and defeat. Especially those I know for certain are not true.

Tomorrow I would like to:
1. Finish this sock
1. Finish another project from the cert
1. Listen to some sermons on self
1. Talk to the alum group in our meeting about possibly hosting services
1. Ask Grandpa about looking at old photos

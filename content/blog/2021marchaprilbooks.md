---
title: "March - April Book Roundup"
date: 2021-05-05T14:06:23-04:00
draft: false
categories:
- article
tags:
- book-review
---

Running a few days late with this one, but that's who I am as a person honestly. I managed to get a lot in during March, but April was more full of podcasts and other projects and I did relatively less reading. Unlisted here are academic papers that I've been reading for various reasons. It's not a lot, but I've essentially assigned myself a research paper because I want to learn more about a topic. That may or may not end up posted on this website. On to the books.

## Books
### Nemesis
#### Book; Issac Asimov; Published: 1989; Read on: 2021-03-05
In the grand scheme of things, we are all impotent. That is the lesson I took from this story. This was a great read. I really loved the story, and quite often read longer than I had intended to because I was so engrossed. But, having finished it, I find myself thinking of little more than one of the minor characters. Janus Pitt convinced the people of his settlement to leave the solar system and strike out for a new star system to colonize. He had grand visions of performing a great experiment, of populating and entire system with a homogenous culture that would thereby be devoid of the chaos and political struggles so endemic to the planet of humanity's origin.

Janus' plans were spoiled in a way and ultimately he was powerless. If we strive for power over others in order to enact our plans we will meet the same end. To be able to coerce and cajole either a few or many people to work towards our own goals can certainly feel like a great deal of power and control, but when all is said and done the universe is so much more vast than we can ever be and we must submit to being nothing more than an individual with limited power, control, and life. 
### Cassio
#### Vol. 5: The Road to Rome; Graphic Novel; Stephen Desberg; Published: 2018; Read on: 2021-03-09
#### Vol. 6: The Call to Suffering; Graphic Novel; Stephen Desberg; Published: 2018; Read on: 2021-03-19
A moderately interesting graphic novel with moderately interesting artwork. Again, I have some thoughts, but since they are rather critical I'm going to save them until I've finished the series.

### The Giver
#### Audiobook; Lois Lowry; Ron Rifkin; Published: 1993; Read on: 2021-03-16
I've never read these books, despite having them come up in conversation and in passing many times. The first one was good, but for the most of it I found myself thinking about how cliche it felt. Young Adult Dystopians novels always seem really obsessed with social control. Not that that's a bad thing, and of course that's a set of ideas that is actually really important to young adults. But to me, it made this narrative feel really...trite. I enjoyed the second novel in the series much more.

### Gathering Blue
#### Audiobook; Lois Lowry; Katherine Borowitz; Published: 2006; Read on: 2021-03-26
You know, in some ways Kira would love the city of The Giver. A communal collaborative place. I found Kira's world and experiences much more engaging and compelling that those in The Giver. I suspect most of that is just my personal preference. I enjoy the exploration lower technology guild style society more than the highly structured highly technical communal one. But also Kira felt like a more compelling character. She experiences suffering far more than Jonas did in The Giver, and so I found myself much more quickly rooting for her. I do exceptionally like the contrast of the endings however. Jonas, in an act of courage, fled his home in order to hopefully help his community. Kira's act of courage was to stay.

I'm still mulling over some questions that arose for me while reading these. Why are the dark societies that exert excessive control such a central theme for for YA Dystopian fiction? What is is about that kind of narrative that is useful for us, and useful for children specifically?

### The City We Became
#### Audiobook; N.K. Jemison; Robin Miles; Published: 2020; Read on: 2021-03-22
Oh my. This book is so good! It's such a phenomenal subversion of Lovecraft's notion of horror while also being an excellent piece of Cosmic Horror that people have come to thoughouly associate with Lovecraft. The characters are vibrant and compelling, and so delightfully diverse! They are all very different people and it matters in the story, their diversity is a reflection of the diversity that is essential to the plot. This might be a go to example for me to point people to what meaningful diversity in characters looks like.

And the worldbuilding! I love it so much! What an incredibly cool and thought provoking was to construct a fictional reality. And I'm not entirely sure that it's all that fictional. The worldbuilding is born directly out of real problems and real struggles of communities. The birth of a city both invokes and evokes ideas that I'll be contemplating for awhile: What does it mean to belong to a place? What does it mean to be of it?

### Detransition Baby
#### Audiobook; Torrey Peters; Renata Friedman; Published: 2020; Read on: 2021-04-02
Another just stellar book! 2020 was a great year for some phenomenal fiction that I know I'll be returning to. Aimes as a character really spoke to me personally. His journey of detransition and return to masculinity was a window for me to consider my tenuous relationship with it. As a nonbinary folk I haven't attempted to sever my connection to it as fully as Aimes did during the transition to Amy (at least, not externally/publically). But I recognized a lot of myself in him. The ways in which transition demands authenticity, and how absolutely terrifying that is. And especially how much masculinity in our current society offers the emotional shield of never having to be authentic.

This book is a must read in my opinion. Whether your are cis or trans, and especially if you love someone who falls into one of those boxes which you do not.

### Wings of Fire: The Dangerous Gift
#### Audiobook; Tui T. Sutherland; Shannon McManus; Published: 2021; Read on: 2021-04-26
This one jumped out at me. I was all caught up on this series last year and was surprised to find a new book out as I was scrolling the Hoopla app. It's such a fun series; I highly recommend it. Dragons, magic, and friendship. Plus every novel manages to be a new and different lesson in compassion

+++
title = "Legacy Project: Episode 1"
description = "The first release of a personal history project."
date = "2020-06-18"
categories = ["article"]
+++

Welcome to the inauguration of this project! My grandparents and I are setting about recording parts of our past as a project of therapy, personal connection, and historical documentation. I’ve edited our raw conversation down so it’s a bit more listenable. I hope you enjoy it. Let me know what you think.

[Episode 1](local:../media/audio/legacy-project_episode-01.ogg)

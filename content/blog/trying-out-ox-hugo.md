+++
title = "Trying out Ox-Hugo"
author = ["Dorian Wood"]
date = 2021-10-05T09:31:00-04:00
lastmod = 2021-12-19T20:47:28-05:00
tags = ["tech", "hugo"]
draft = false
weight = 3002
+++

So I've been playing around with my website bits and bobs here and there the past month or so. I initially set up ox-hugo so that I could use my collection of links I've been keeping in an org file as a digital link garden on the site.

That project is going well! You can check it out by clicking on the Links button in the navigation menu. And today since I've been putting off writing up my reviews of books I've been reading I decided to just bang it out using ox-hugo to organize it. It worked pretty well too!

So this is my first post attempting to just write a regular blog post in org and see how it exports. If it goes well I might slowly start migrating all my old posts into an org file, just so that I can have things neatly organized. Or maybe just write future ones in here, that would be way easier.

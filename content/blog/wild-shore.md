+++
title = "The Wild Shore: Part 1 of the Californias Triptych"
author = ["Dorian Wood"]
date = 2022-01-22T14:38:00-05:00
lastmod = 2022-03-05T22:16:13-05:00
tags = ["book-review", "three-californias-triptych"]
draft = false
weight = 3006
+++

## Info {#info}

Author: Kim Stanley Robinson

Series: Part One of The Three Californias Triptych

Narrator: Stefan Rudnicki

Published: 1984 & 2015

Read on: 2022-01-22

Link: [The Wild Shore](https://www.hoopladigital.com/title/11295688)


## Review {#review}

Another post-apocalypse story, like [Earth Abides]({{< relref "earth-abides.md" >}}). This one is thought provoking in very similar ways. What does civilization look like after all the infrastructure has been demolished/taken away? Our main character and narrator is a young man who was born after the bombs went off, and we get to see what it's like to live in a small isolated village where folk have returned to trying to eke out an existence with fishing and agriculture.

Unlike Earth Abides however, there is still civilization out there. And as Hank encounters others from beyond his small village, he must reckon with the dis junction of his pastoral village and outsiders seeking to rebuild America. Tom the Village elder tells all kinds of stories about the old days, back when America was a great empire. He hopes to instill a vision in others of regaining all the good things that were lost. But he also bends the truth, hoping to erase the bad things that also disappeared after the bombs.

I really liked the village Hank lives in. It's full of real people who go about their lives and contribute in their own way. Every exchange Hank has with others reveals more and more and I grew to enjoy it there quite a bit. At the end Hank makes his decisions that go against the majority vote of the village and then he has to deal with the consequences. But the act of making those decisions puts him in a place to relate to adults in the village in ways that he could not previously.

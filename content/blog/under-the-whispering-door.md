+++
title = "Under the Whispering Door"
author = ["Dorian Wood"]
date = 2022-04-28T13:10:00-04:00
lastmod = 2022-04-28T13:10:38-04:00
tags = ["book-review"]
draft = false
weight = 3002
+++

## Info {#info}

Author: TJ Klune

Narrator: Kirt Graves

Published: 2021

Started on: 2022-04-15

Finished on: 2022-04-27

Link: [Under the Whispering Door](https://www.hoopladigital.com/title/13326827)


## Review {#review}

This was such a delightful book! I would say it's on par with the last book I read by Klune,[The House in the Cerulean Sea]({{< relref "house-cerulean-sea.md" >}}).

It opens with a rather unlikable main character, who then goes through some truly life changing growth thanks to those around them, and ends up changing the world for the better on a scale that is impossible to see coming. An accurate description for either book.

I had so much fun on the journey with Wallace as he grew and began to care for those around him. Everyone at the tea shop is so special and lovable as well. The twist was really good this time too! I did not see it coming! I think it has a leg up on The House in the Cerulean Sea on that one honestly.

Also, there's a ghost dog! I highly recommend this book.

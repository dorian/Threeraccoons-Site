+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-10-29"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Worked on FCC project.
1. Knitted more of the sock. I'm about 3/4 of the ways done.
1. Raked more leaves
1. Drove Grandpa to the store

Next time I would like to do better on:
1. Be more open with Grandpa. Hold less anger.

Tomorrow I would like to:
1. Finish sock
1. finish FCC project
1. set up own git repos for dot files. A proper fork of voidrice.

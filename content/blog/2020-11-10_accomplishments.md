+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-11-10"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:

1. Took Grandma to get blood drawn.

2. Had therapy session, today Grandpa and Grandma joined in.

3. Ran errands in town.

4. Started a new puzzle with Grandpa.

Next time I would like to do better on:

1. Engage my grandparents more. For my health and theirs.

Tomorrow I would like to:

1. Get gitea hosted

2. Research privacy some

3. Play ukulele, possibly with Grandpa

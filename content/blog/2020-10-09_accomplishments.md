+++
title = "Today's Accomplishments"
description = "Played around with learning Hugo"
date = "2020-10-09"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Figured out ikiwiki does not seem to have a workflow that I enjoy.
2. Learned basics of making a theme with Hugo
3. Worked on a sock I'm knitting

I would like to do better next time:
Communicating with my mother. Not getting caught up in my own frustration.

Tomorrow I would like to:
1. Continue learning Hugo themes, develop my site theme further
2. Hopefully finish the sock and start another one
3. Get around to a script for automating this

---
title: "Research Update"
date: 2021-06-06T20:21:13-04:00
draft: false
categories:
- blog
---

This is the first update in my research project on using decentralized web technologies to build resilient communities. This week I read five papers:

## Articles
### Beyond Cryptocurrencies - A Taxonomy of Decentralized Consesus Systems by Florian Glaser
This article goes about describing various components of blockchain system. The author does a good job of avoiding technical specifics while laying out the interrelatedness of blockchain components. The most useful bits were tables 3 and 4, as they were the chief product of the work that the paper describes.

I did not find any specific leads for further research in this article, but it is a very useful tool in that it gave me a framework for conceiving of blockchain systems as they might function in practice.

### Contracting for Services in a Decentralized System by Steven Rathgeb Smith and Judith Smyth
This article has nothing to do with blockchain, or even decentralized computer systems. Rather it focuses on the intricacies of the decentralized contracting for substance abuse services in North Carolina in the mid 90's. It is my hope that Social Work research in other areas can help reveal potential benefits and pitfalls of utilizing decentralized web technologies for community building today.

The ideologies of devolution and privitisation that drove the decentralization of healthcare for substance abuse treatment relied on the belief that market forces would induce cost savings and innovation. As communities were given control of where they wanted to spend money contracting for services, the belief was that companies would compete and innovate. The authors found that the opposite was true. Contracts relied very heavily on relationships of trust between individuals, rather than market forces like supply and demand. Moreover, innovation often depended on outside funds like grants and federal funding, since local officials were loath to move funds away from entities that they already had a trust relationship with. Since innovative programs depended on grants and federal money, market forces worked to suppress them rather than cause their flourishing.

I found this article especially helpful in spurring thoughts about the downfalls of decentralization. What kinds of things benefit from have a central driving force? In what cases will a pursuit of decentralization end up with a dispersion of effort and resources that is detrimental? I already have several other articles lined up about decentralized policy implementation and such. I'm excited, I think this is a vital perspective to bring to the discussion about decentralized web technologies.

### Decentralized Applications: The Blockchain-Empowered Software System by Wei Cai et al.
This article is one published by the IEEE, so it's a really great technical overview. It helped me understand the consensus systems that have been implemented a bit better, as well as how those systems come together to enable a blockchain system. Most importantly for me, they have a section that lays out the shortcoming of blockchain based software currently. Reasons why the technology as it stands is ill suited to the various applications that it is currently being put. None of those problems are unsolvable, but they do provide excellent sites of critique. Place to investigate and really think about what it is that decentralization offers, and the places in which it might not actually give us what we want. Decentralization solves some problems with centralized services, but it introduces it's own. Notably, blockchain based software currently struggles with low performance: high latency and low throughput.

This article has cemented my interest in further investigation of federated web services (I'm thinking chiefly of ActivityPub here). I.e. services that do not rely on a single centralized authority, but still utilize a client-server model to provide basic services. This seems to be a good middleground between completely centralized and completely decentralized applications.

### Decentralized Autonomous Organizations (DAOs) as subjects of lawa by Sven Riva
I really enjoyed this one! It's a master's thesis, so quite a bit longer than the other stuff I've read so far, but it was quite easy to read. Like the other blockchain articles it goes into a bit of background, but the meat of it is the ways in which organizations formed on blockchain consensus mechanisms fit into Swiss law. The author found that when organizations employ blockchain technologies but still incorporate under the regulation of some government somewhere that is fairly straightfowrard (Switzerland has legislation in place for dealing with foreign companies). But that is much more difficult for organizations that use blockchain in order to organize themselves outside of any state.

The treatment of Digita Autonomous Organizations (organizations built on top of a blochchain) raises questions for me about what it means to organize ourselves. Why we might wish to do so, and what benefits that brings. My current take on the usefulnees of blockchain organizations is that many of the things that might be exciting to organize on a blockchain are probably better served as not being a formal organization in that way. The immutability and high technical literacy requirements for blockchain in my view make them a poor solution for community led organizations, where human consensus (as opposed to the mathematical consensus on blockchains) and adaptability are of utmost importance.

### Blockchain Imperialism in the Pacific by Olivier Jutel
This is key reading on blockchain IMO. We cannot blindly buy into the promises of technology and venture capitalists when discussing governance and social work. Technology is never apolitical, and it's mere application always carries with it political assumptions and the power structures of its creation. Current blockchain experiments being run in the Pacific are soft power initiatives on the part of imperial forces.

It is my assesment that outside forces developing technological solutions for communities will never produce true autonomy on the part of individuals and the community. Such solutions must be engineered from within, both because only the community itself knows its own needs and technological autonomy requires self sufficiency in order to prevent power imbalances.

This article also points out how blockchain solutions fail to live up to even their own measures of success. These systems do not solve the problems they hope to, and in many cases established technology serves the needs of communities far better (e.g. a centralized database is simpler to run and provides all the benefits blockchain proponents advertise in some cases).

## Conclusion
I have a good mix of research from both social work and technical perspectives on blockchain. The social work perspectives on decentralization in other areas are something that I think will be cruicial in developing useful critical analysis of decentralization in computer systems. As it stands, I don't think blockchain is a viable path forward for positive community building.

I'm very happy with where this research is going so far, and I'm eager to continue. Burnout hasn't set in yet at least.

If you have thoughts you would like to share please contact me on Mastodon (@dorian@litterae.social), or via email (dorian AT this domain DOT com).

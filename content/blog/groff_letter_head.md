---
title: "Groff Letter Head"
date: 2021-01-26T12:10:14-05:00
draft: false
categories:
- article
---

## Summary

I've been working on some resumes lately, and I was really frustrated with working on the typsetting for the cover letters.
The traditional format for the head of letters is to have the addresse's information on the right, and the sender's information on the left.
What this meant is I wanted to have some text aligned to the left and some text aligned on the right on the same line.
I couldn't find any guides on how to achieve this in Groff, and it wasn't straightfoward in LibreOffice either.
Here's what I managed to accomplish with groff and tbl:

![A picture of a mock letter head.](/articles/letter_head.png)

So here's my groff solution.

## tbl


With that taken care of, here's my code:

```
.TS
expand tab(|);
l r.
John Sender|Susie Receiver
1-234-567-8901|1-098-765-4321
john@send.com|susie@receive.net
145 Smalltown, GA|532 Bigcity, NY
.TE
```

`.TS` & `.TE` start and end the table block respectively.

`expand` tells tbl that you want the width of this table to expand to take up the full page, otherwise the table will only expand to accomodate the data inside it.

`tab(|)` sets the cell separator to the `|` character. The default is `<TAB>`, and while that works in most cases, I've found it can sometimes be hard to read the groff code with that. Feel free to set it to whatever character works best for you.

`l r.` defines the table. The period at the end signals to the tbl preprocessor that we are done defining how the cells looks. This definition provides two columns, the first one with its text left aligned, and the second one with its text right aligned. When you write a table with more rows than you have defined, additional rows simply take the form of the last defined row.

## groff

In order to have groff properly compile this table you need to run your document through the tbl preprocessor.
You can do this in two different ways, either by calling tbl indpedenently and piping the output into groff, or by using the -t option when calling groff.
Note that tbl must be run before eqn if you are using that, calling them with the groff options takes care of this for you.
In order to get a pdf of this, run it through groff like so:

```
preconv file.ms | groff -ms -t -T pdf > file.pdf
```

## End

And there you have it! It turned out pretty simple once I figured out tbl would give me the results I wanted. For further reference I recommend checking out the man page on tbl, and the info page on groff.

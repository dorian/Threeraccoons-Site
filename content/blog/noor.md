+++
title = "Noor"
author = ["Dorian Wood"]
date = 2022-02-11T15:56:00-05:00
lastmod = 2022-02-11T15:57:22-05:00
tags = ["book-review"]
draft = false
weight = 3002
+++

## Info {#info}

Author: Nnedi Okorafor

Narrator: Délé Ogundiran

Published: 2021

Read on: 2022-02-04

Link: [Noor](https://www.hoopladigital.com/title/14383137)


## Review {#review}

As always I was totally enraptured by the narrative that Okorafor weaves. AO, our central character, is a young disabled woman. Once we get to know her the action of the novel takes off after she experienced a very traumatic event and has blocked all memory of it. We discover what happened to force her on the run as she does. As she is on the run she meets a young man named DNA, and they trauma bond; both having just had traumatic brushes with death. The world (both governments and transnational corporations) is against them both. Neither of them can continue with the quite lives they had hoped to lead, and all seems doomed. That is until AO gains the power to fight back.

I really enjoyed Okorafor's construction of AO. It was very nice to have a disabled character whose disability is critical to the plot without it being inspiration porn. AO's disabilities are a struggle for others, but never her. It's a part of who she is and she has no shame in accessing the technology that makes it easier for her to navigate the world made for abled bodies.

As with every single one of Okorafor's novels, I highly recommend this book.

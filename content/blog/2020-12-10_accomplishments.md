+++
date = 2020-12-10T18:19:38-05:00
title = "Thursday's Accomplishments"
draft = false
categories = ["blog"]
+++

Today I accomplished:
1. Redid the script that automates this blog! I rewrote the whole thing from scratch. Instead of building the meta data and writing the post via `read` prompts and numerous file writes, I set up an archetype in Hugo and just call hugo-new and then open the file in nvim. Much simpler and way easier to extend in the future!
2. Made a second batch of Christmas cards with Grandpa and Grandma. We'll have to run to the post office to mail them, since one needs to go to France.
3. Planted my Christmas present from Grandpa and Grandma! My sister ordered a bamboo plant online for us. It's pretty cute.

Next time I would like to do better on:
1. Spend some more time engaging with my family. Spend more intentional time with the cat and interacting with Grandma.
2. Take breaks better.

Tomorrow I would like to:
1. Take some peeks at either the Mastodon server or my website, as my passion leads tomorrow.
2. Give Grandma a bath.
3. De bone the chicken we had the other night, get the stock simmering.
4. Make chicken soup.

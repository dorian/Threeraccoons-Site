+++
title = "Today's Accomplishments"
description = "What I got done today, what I hope to do tomorrow"
date = "2020-09-25"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Set up nginx to server the public_html directory of each user on my server. This unfortunately broke my searx instance that I'm hosting so I turned it off.
2. I looked into how to move the searx instance to it's own subdomain. That is half working. It now loads if you navigate to searx.threeraccoons.com/searx, but I haven't yet figured out how to remove the need for the trailing searx.
3. I practice Ukulele

I would like to do better:
I don't have anything for this portion today.

Tomorrow I would like to:
Tomorrow is Saturday, so I'm taking the day off from "work". That said, it would be nice if I did some stuff too.
1. Maybe read the nginx manual and figure out how to remove the need for the /searx.
2. Get myself set up for reading some lines of Homer in the Homerathon on Wednesday. I need to obtain the book and make sure I have the zoom application working.
3. Knit more. I would like to make progress on a pair of socks.

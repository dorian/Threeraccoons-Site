+++
title = "Searching the Internet with Privacy"
author = ["Dorian Wood"]
lastmod = 2022-04-29T09:19:07-04:00
tags = ["tech"]
draft = true
weight = 3001
+++

## TL;DR {#tl-dr}

duckduckgo is the standard go to for privacy respecting search engine. It is the most like what people are used to.

Searx is another option. It is pretty easy to use, but there are many different places to find it.


## Search Engines {#search-engines}

There are so many search engines out there. I'm not going to go through a list because others have done it better than I could given my time restraints. This article lists a good number of privacy respecting search engines: [ItsFoss: Privacy Respecting Search Engines](https://itsfoss.com/privacy-search-engines/).

In this space here I'd like to contemplate together what search engines offer us, and what other tools we might make together that might better serve the needs we have defaulted to using big search engines for.


## Beyond Search Engines: Why Search? {#beyond-search-engines-why-search}

What do you search for typically? Here's mine:

-   troubleshooting some device or tool (e.g. )
-   website that I forgot the address for
-   quick facts

Think of the other places that you use search. Any social media site you use has a search option. Wikipedia has it's own search tool. I often end up on the website StackOverflow when troubleshooting things on my computers. It would be better for me privacy wise if I just went to that website directly and searched my question on their platform rather than searching it on Duckduckgo only to click on the top link to StackOverflow. By searching on Duckduckgo, both companies can track what I was looking for; if I went directly to StackOverflow, only one company would.

This is one way we can shift our searching habits to be more privacy respecting, and quite possibly more useful. We can also go a step further and cultivate our own personal respositories of knowledge!


## Creating your own searchable repository of information {#creating-your-own-searchable-repository-of-information}

What do you do when you come across a link to a useful website or resource on social media? Too many times I have shared it as appropriate, and then found myself months later recalling the resource and wanting to view it again, only to be unable to find it.

This habit of assuming I could easily find anything I wanted via a search engine turned out to be really frustrating. So I started writing such things down. I have a file on my computer that I put such things in. That way if people ever ask me for certain kinds of resources, I don't have to go searching on the internet, I already have a list I can copy and paste into whatever platform we are communicating on. Or I can search that document for my own reference.

I was reading in my local paper earlier this week about a Needle Exchange Service in a neighboring county. I copied the information down in my file for future reference. Just now I attempted to search for this information on that county's website. I can't find it. Even in a quick exercise for this blog post I have found a situation where searching did not give me the information I was looking for, but I have it in my notes.

Please don't just consider the privacy implication of the search engines you use. Consider also how you might collect and preserve information in a way that is more useful to you in the long run. And even how that might be useful for your community, or who in your community has important knowledge about how to access various resources.

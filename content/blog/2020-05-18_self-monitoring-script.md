+++
title = "Self Monitoring Script"
description = "I wrote a program to make self reflection easier."
date = "2020-05-18"
categories = ["article"]
+++

tl;dr Here’s a Bash script for quickly implementing some therapeutic self monitoring: responseGatherer.sh

For the last nine months I’ve been using a Thinkpad x200t as my main laptop. It’s a twelve year old machine, and I have had quite a bit of fun seeing how much use I can get out of such an old computer. Honestly I enjoy using this computer more than my laptop that was made in 2017. But I spend 1/2 to 3/4 of my time in the terminal so I’m not really a typical use case for computers.

A few months ago my therapist asked me to start keeping a sleep journal. I have known sleep hygiene is super important for my mental health but I’ve never kept close track of it before. I struggle with actually remembering to write such things down, so I decided it would be a good project to see how much I could automate and how easy I could make it to keep tract of my sleep habits. I wrote a script to print prompts, and then write my responses for my daily journal that I write on this computer.

This script is a complete rewrite of that first one, I’ve abstracted the work more and made it so I can declare an input file for prompts. I now get to use this program for any self monitoring exercise I want, all it takes is a few minutes to type up the prompts in a text file. I find this workflow really useful, so I figured I would share it. This does require you be running a Unix terminal, and have some version of Bash installed. But at this point I’m going to assume anyone who is interested in using this is going to have those things set up already. This isn’t a pretty app for a phone, but it does the work I need quickly and efficiently. N.B. If you want to use this but aren’t sure how, send me a message. I might be able to help you get it running.

responseGatherer.sh

I keep a folder of prompts on my computer. If you are interested here’s the contents:

sleepJournal:
How many naps did you take yesterday?
When did you last consume caffeine?
Did you exercise?
What was your overall mood yesterday?
What did you do the hour before bed?
What time did you go to sleep last night?
What time did you wake up?
How long did it take to fall aslep?
What was the quality of your sleep?
How do you feel this morning?

stoicThoughtMonitoring:
Stoic Self-Monitoring Sheet
Date/Time; Briefly describe the situation
Feelings (Passions)
Thoughts (Impressions)
Control; Is this up to me or not?

valuesDirect:
Examining Values
What is ultimately the most important thing in life to you?
What do you want your life to “stand for” or “be about”?
What would you most like your life to be remembered for after you have died?
What sort of thing do you most want to spend your life doing?
What sort of person do you most want to be in your various relationships and roles in life?

valuesClarification:
Values Clarification Exercise
Desire : What things do you find most desirable and spend your time pursuing?
Healthy : What do you think are the healthiest character traits a person can possess?
Praiseworthy : What do you find the most admirable or praiseworthy in other people?

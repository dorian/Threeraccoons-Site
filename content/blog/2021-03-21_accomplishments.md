---
date: 2021-03-21T09:14:05-04:00
title: "Sunday's Accomplishments"
draft: false
categories:
- blog
---

Over another month since we last shared again! I think I'd like to return to this daily practice. It was good to take a break I think. But perhaps with some new organizational efforts I will find it useful once again.

This past month I accomplished:
1. Finished volume 1 of the system administrator course. It was good, and I'm excited to start in on volume 2.
2. Started recording and sharing music that my grandparents and I play together. You can find it under the Music link up above.
3. My grandparents and I are fully vaccinated now! Which means I can start work again, and my parents can come visit.

Next time I would like to do better on:
1. Too many things

Tomorrow I would like to:
1. Continue on the Stats Fundamentals course I started
2. Jot down some quick notes about vol 1 of the sysadmin course and start vol 2.

+++
title = "Crafting A Hyperbolic Circle"
description = "Combining crochet and maths"
date = "2020-05-22"
categories = ["article"]
+++

[A blue crocheted hyperbolic circle on a beige background. The curvy and curly nature of the shape looks similar to some coral.](../media/images/hyperbolic-circle.jpg)

Feast your eyes on a strange intersection of math and fiber arts! One of the ways I like to spend some relaxing time is watching maths videos on Youtube, and so on a whim a few weeks ago I searched for how to crochet mathematical objects. [This](http://chalkdustmagazine.com/blog/wonders-mathematical-crochet/) website was one of the top results, and it has several projects you can try to explore some geometry that is actually really difficult to model otherwise.

Hyperbolic geometry is something you end up with when you mess around with Euclid’s axioms. Specifically for hyperbolic geometry, the fifth postulate, which establishes how parallel lines work on a flat surface. The result is some very weird looking shapes. What I love about this is it’s not breaking any rules of math. Axioms are just things held to be true because that is what is observed in geometry. By slightly changing some things we hold to be true, we can get really cool shapes like this hyperbolic circle. And it’s really a circle! The rules for something to be a circle in geometry are that all points along the edge must be the same distance from the center. Every stitch on the last row of my creation above is indeed the same length from the center. The edges don’t lay flat, but that’s not part of the definition of a circle. So we get to make really bizarre things like this where a circle needs three dimensions instead of just two!

My favorite part of this is that making a hyperbolic circle by other means is actually pretty difficult! You can tape together paper to attempt to make a model of what a hyperbolic circle would look like, but it’s hard. Here is a video where they make a square with five corners, by using non-Euclidean geometry. With crochet however, it is really easy! All you need to do to crochet one of these yourself is to double crochet 12 stitches in a circle for your first row, and then increase in every stitch for every row after that. That’s it. That’s the whole pattern. Beginner crocheters can easily make a really weird mathematical concept that’s hard to construct physically by normal means! How cool is that?!

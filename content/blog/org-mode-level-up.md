---
title: "Org Mode Level Up"
date: 2021-08-25T05:17:04-04:00
draft: false
---
The past several days I've explored org mode a bit and it's been really fun!
I already use it for much of my writing and organizational purposes,
but I came across
[this blog post by Diego Zamboni](https://zzamboni.org/post/my-doom-emacs-configuration-with-commentary/)
the other day and combed my way through it.
I think I've heard of literate programming before but this is my first time really experimenting with it.

I have to say,
the ability to write and execute code via an org mode file has really blown my socks off.
Having learned that I could do that I've set up a simple two line tidbit in one of my org files that makes my life a bit easier.

I'm currently learning statistics with the
[Learning Stats with R](http://compcogscisydney.org/learning-statistics-with-r)
book.
It's a great book and I really love it;
I highly recommend if you want to learn stats.
I keep my notes in an org file,
so my workflow to study looks like:
- open statsclasses.org
- launch terminal
  - open learn stats with r pdf
- launch second terminal
  - navigate to data directory
  - launch R
  
Not bad but like, several very simple very repetitive tasks just to start learning.

So I added some shell code to my notes file to do them for me!

```
#+begin_src sh :async :silent
zathura ~/dox/books/edu/lsr-0.6.pdf &
xfce4-terminal --working-directory=/home/dorian/R/lsr-data/data/ --command=R &
#+end_src
```

The first command opens up the pdf,
and the second creates a terminal running R in the appropriate directory.
The `:silent` property tells org mode to not print the results of the code into the document,
and the `:async` property makes it run asynchronously from emacs.

The async part is necessary because
emacs will wait for whatever code you ran to finish before you can do anything else
(emacs is single threaded).
you need the package ob-async in order to do that, it's available in MELPA.

This feature has me in love with org mode all over again honestly.
This is so cool!

+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-10-16"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Patched Surf with homepage and modal
1. Applied to a scholarship to use on some courses at edX
1. Looked into applying for the program at recurses.org
1. Completed the Applied Visual Design modules of the Responvie Web Design cert at freecodecamp.org
1. Got xdg-open to launch surf (finally!!!)
1. Gave Grandma a bath
1. Meditated and did some self reflection in my journal

Next time I would like to do better on:
1. Voice my feelings freely and positively with my Grandfather. I need to work on a HBPS for this.

Tomorrow I would like to:
1. Figure out how to get mpd to scan my music directory properly.
1. Chat with Meg
1. HBPS for voicing feelings with Granpda
1. Work on knitting a sock
1. Practice Ukulele

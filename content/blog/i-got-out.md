+++
title = "I Got Out."
author = ["Dorian Wood"]
lastmod = 2022-08-28T17:41:31-04:00
tags = ["reflection"]
draft = true
weight = 3003
+++

## tl;dr {#tl-dr}

I escaped the clutches of abusive family. I had to leave behind my grandparents.


## Why am I writing this? {#why-am-i-writing-this}

I honestly don't know yet. It has only been two weeks since I fled my blood family, and I have taken very little time for quiet contemplation in that. My normal patterns would be to pressure myself to resume full time work as soon as possible. My partner has encouraged me to keep it at part time and take time to heal and settle. I suppose part of this is keeping my promise to try that.


## Why did I have to flee? {#why-did-i-have-to-flee}

Oh. This would be interesting for me to lay out. Even just for myself. Why did I have to flee? The easiest answer is that I could no longer tolerate living next to my birth mother who sexually abused me for approximately the first two decades of my life. Even though the sexual abuse had stopped there was still ongoing emotional and verbal abuse up to the point when I fled two weeks ago.

I also had to leave because I could no longer adequately care for my grandparents. My grandfather and I were the main caretakers for my grandmother, who has Alzheimer's. I also helped my grandfather manage all his medical care. It was a lot.

I was unable to live my life. In my last week living there I finally started wearing the clothes that I want to. It was clear that my gender expression did not please those around me. Particularly my birth mother. Not only that, but my life plans had to be on hold while I cared for them. I could not enact very many long term plans.


## What does fleeing allow me to do? {#what-does-fleeing-allow-me-to-do}

I have space and room to have my own future for the first time in my life. I do not in any way regret the time I spent caring for my grandparents. I cherished it and would do it again in a heart beat. But it was five years of my life. Five years where I could not make any life changing decisions. Five years of what was essentially a 24/7 job.

In addition to having space to make life decisions, I also have space to relax. Caring for Grandma was a job that I couldn't put down as long as I was in the same house. I always had my ear bent to listen for her coughs even if I was doing something at the other end of the house. It also meant frequent interruptions when I was working on anything as well. That is quite the load to bear.

Fleeing allows me to be authentically myself! I am a nonbinary transfem individual. Dressing the way I want was always going to be an issue while living with my grandparents. Because of the semi-rural conservative area we lived in. Because my parents next door didn't approve. Because even when I dared to wear skirts I would always have that fear in the back of my head. Where I'm living now I have so much more freedom and less fear.

Fleeing means I can genuinely tackle my trauma. I can live in a place that is safe, and being safe provides

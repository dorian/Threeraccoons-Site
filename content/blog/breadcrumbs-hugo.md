+++
title = "Breadcrumbs in Hugo"
author = ["Dorian Wood"]
date = 2021-12-19T20:24:00-05:00
lastmod = 2021-12-19T21:04:47-05:00
tags = ["tech", "hugo"]
draft = false
weight = 3001
+++

## Intro {#intro}

Changes to the site again! I've been interested in implementing breadcrumbs as a navigational tool on my site for a month or so now ever since I ran across a blog post about them (no idea what I've done with the link at this point, sorry). Breadcrumbs are a navigational tool that typically appear at the top of the content of the page, to indicate where in the site hierarchy the user is.

Seemed like a really cool idea when I first learned of it, and since I have it implemented, I really like the feel of it! Using breadcrumbs to navigate the site for me has this feel of browsing text documents on a webserver that provides just the raw directory listing. I like the feel of it so much that I've just removed my traditional sidebar navigation altogether! I personally feel like my website is simple enough that the breadcrumb navigation alone is adequate. But I'd love to hear your thoughts! Let me know if the breadcrumbs seem good enough, or if site feels less navigable without the sidebar with consistent links.


## Explanation {#explanation}

This post is just as much me explaining to myself some details about how Hugo works as much as it is about telling anyone else how this implementation of breadcrumbs works ha. I'm finally grasping how templating using Go works and want to make sure I can remember in the future.

I yoinked this hugo snippet from [here](https://dev.to/faraixyz/breadcrumbs-in-hugo-303k). Gandiya does a great explanation of it, but I'm going to do it again in my own words just for the exercise of it ;).

```nil
{{ with .Parent }}
    {{ partial "breadcrumb.html" . }}
    {{ if .Parent }}>{{ end }}
    <a href="{{ .Permalink }}">{{ .Title }}</a>
{{ end }}
```

```nil
{{ with .Parent }}
...
{{ end }}
```

The `with` statement uses the same logic as an if statement, the block executes only if an if evaluation returns TRUE. That is, only if the provided variable exists. It differs from and if statement in that it also rebinds the scope.

So the code within the `with` block executes as if it were run on the .Parent page, rather than the page this snippet is called from.

```nil
    {{ partial "breadcrumb.html" . }}
```

This bit calls the same partial code again. This recursion means that the block gets run for each page until it ends up with a scope where there is no parent, i.e. the homepage! This one little bit is all that is needed to make sure any page this snippet is called from will return the full hierarchy path from that page to the home page! How cool is that?

```nil
    {{ if .Parent }}>{{ end }}
```

This snippet checks if there is a parent page, insert the separator I've chosen before the link, because we're going to be putting more stuff in front of it.

```nil
    <a href="{{ .Permalink }}">{{ .Title }}</a>
```

This is what inserts the actual link, using the page title as the link text. In the location where this snippet gets called from, I call the `{{ .Title }}` bit one more time, so the title of the starting page will also appear. Like so:

```nil
<nav class="crumbs">
	{{- partial "breadcrumb.html" . -}}
	{{ if .Parent }}>{{ end }}
	<h1>{{ .Title }}</h1>
</nav>
```

---
date: 2021-05-15T10:48:31-04:00
title: "Saturday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Yoga
1. Shower
1. Gave Grandma a shower
1. Took Grandma for blood work up
1. Picked up some new houseplants with mom
1. Uploaded a new music recording
1. Helped mom set up bitwarden on her devices

Next time I would like to do better on:
1. Sharing my feelings with my family.

Tomorrow I would like to:
1. Be present and kind in interactions with my family.

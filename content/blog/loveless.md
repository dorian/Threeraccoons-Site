+++
title = "Loveless"
author = ["Dorian Wood"]
date = 2022-03-18T10:54:00-04:00
lastmod = 2022-04-04T17:06:37-04:00
tags = ["book-review"]
draft = false
weight = 3002
+++

## Info {#info}

Author: Alice Oseman

Narrator: Billie Fulford-Brown, Elizabeth Schenk, Imogen Church

Published: 2021

Started on: 2022-03-12

Finished on: 2022-03-18

Link: [Loveless](https://www.hoopladigital.com/title/14567459)


## Review {#review}

What an absolutely delightful book! It has far more swearing and alcohol than I'm used to coming from books published by Scholastic Press. We follow Georgia as she is nearing the end of Year 13 at her school and is heading off to college with her two best friends. It's a wonderful tale of this young woman trying to find out who she is, including her sexuality, which ends up being Asexual and Aromantic. It's obvious from the beginning if you are familiar with those identities, but it takes Georgia quite a while to learn that. The story takes us through the dramatic ending of friendships, their delightful reunions and amends, as well as several characters finding true love; romantic and platonic.

I found it quite delightful how many references to fanfiction and fanfiction tropes Georgia makes through this novel. And at the end there was a really beautiful short story told from the perspective of two of the supporting characters that fall in love.

+++
title = "Today's Accomplishments"
description = "A record of my daily accomplishments."
date = "2020-09-24"
draft = "False"
categories = ["blog"]
+++

Today's status update:
1. I configured my command prompt on my tilde.institute account. This involved learning more about ANSII color codes as well as prompt building syntax. I have not written my own prompt in perhaps close to a decade. It was a fun little project and rewarding.
2. I would like to practice Progressive Imaginary Exposure more thoroughly in order to progress further in being vulnerable with those I love, rather than just anxiously thinking about what I might say only.
3. Set up nginx to serve user dirs and invite some friends to use it.

---
title: "T500 Repair"
date: 2021-02-04T21:41:39-05:00
draft: false
categories:
- blog
---

I finally got my Thinkpad T500 up and running! All it needed was to replace the cooling fan, which I've had sitting on my desk for a few months. The one trouble I ran into was that the new cooling fan was for some reason a different shape. Not significanly, but just enough so that it did not have the clearance to fit next to the port where the wifi module plugs in. It was about 1mm or so too large. So! After about twenty minutes with some wirecutters and then a grinder my grandfather and I got it fitting in nice and snug!

Currently I'm running my Arch install on it that I just pulled from my X200t, but the next project for it is to get either FreeBSD or OpenBSD installed on it and start playing around with those pals, see what BSD is all about. I have to say though, text is much more readable for me on this 15" screen ha.

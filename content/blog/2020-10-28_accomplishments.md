+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-10-28"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Got a good chunk done on a FCC project.
1. Exercised some.
1. Left a note for Grandpa. We had a conversation.
1. Raked the yard with Grandpa.

Next time I would like to do better on:
1. Listen to Grandpa. Not get defensive. Don't lash out in anger.

Tomorrow I would like to:
1. Finish the FCC project.
1. Finish the sock I'm working on

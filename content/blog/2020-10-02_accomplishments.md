+++
title = "Today's Accomplishments"
description = "Finished modification to the Jitsi server and wrote a blog post!"
date = "2020-10-02"
draft = "False"
categories = ["blog"]
+++

Today's Accomplishments:
1. I finished setting up the jitsi server to be accessible from both the grandvalleyclassicsalum.org domain and my threeraccoons.xyz domain.
2. I also added that information to my personal wiki that I'm growing. That is really helpful even just as a measure of all the things I'm learning.
3. I wrote a blog post doing a super brief intro to the Fediverse and how it's about building online communities in a different way from mainstream social media.

I would like to do better next time:
1. I found myself very exhausted at the end of the day. That's not a problem, but I'd like to be more aware of my energy levels throughout the day to notice what drains me.

Tomorrow I would like to:
1. Play around with ikiwiki
2. Relax
3. Do some self care, maybe take a drive and listen to some books.

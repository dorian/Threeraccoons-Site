---
title: "Thursday's Accomplishments"
date: 2021-02-18T10:36:57-05:00
draft: false
categories:
- blog
---

Wow! I know I've been slacking off on this, but I haven't done one in the entire month of February?! Dang. I kind of stopped doing these because I felt self conscious. They felt somewhat spammy. For whatever that's worth. But I have things I'd like to share!

Accomplishments:
1. Grandpa and Grandma got their second COVID Vaccine shot yesterday! So close to being able to do things again!!! I am very excite.
2. I learned a new song on the ukulele! Donky Riding is a fun Canadian folk tune. I like it a lot. I think I'll try my hand at writing some new lyrics for it some time.
3. Grandpa and I cleaned out the office a bit today! I'm hoping to gain some more usable space, and to rearrange the furniture when we get more stuff cleared off.
4. I installed GNU Guix on my Thinkpad T500 last week. I'm having a lot of fun with this system! I really like the feel of the guix package manager, and it's a handy way to get myself familiar with the scheme language. For now this is just a playground, but I'm planning on making that my full time work machine as I get more familiar with guix/set it up with my suite of programs.
5. I've made great progress on Using and Administering Linux Vol 1 by David Both! I'm about 3/4 of the way through, and even though a lot of this is stuff I've picked up just form using GNU/Linux daily for a few years, there's still little tidbits that I am learning (also it's a great way to boost my confidence in my sysadmin skills lol). I'm doing my best to continue chugging through this. I'm eager to continue learning and improve my sysadmin skills.
6. I got Grandma to sing a chorus of a song with me on the ukulele! I made a recording of it and everything. It puts a smile on my face to listen to.

Things I'd like to do better next time:
1. Sharing my feelings in a nonjudgemental way. I struggled to communicate during a couple calls with my mother today. But I'm making progress.

Tomorrow I'd like to:
1. bah. Tomorrow is for tomorrow.

---
date: 2021-01-04T19:30:41-05:00
title: "Monday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Had a video call with Thord! That did my heart a great deal of good.
2. Worked on publishing toots from an rss feed. I found a suitable python script but I'm opting to write my own shell script to do it instead. So I'm learning the Mastodon API.
3. Practiced ukulele
4. Finished a sock!

Next time I would like to do better on:
1. Go for a walk, I didn't today.

Tomorrow I would like to:
1. Clean the center island
2. Give Grandma a bath
3. Start a new sock
4. Get a script that can authenticate with Mastodon working

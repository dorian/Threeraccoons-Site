+++
title = "Hench"
author = ["Dorian Wood"]
date = 2021-12-31T15:54:00-05:00
lastmod = 2021-12-31T15:56:23-05:00
tags = ["book-review"]
draft = false
weight = 3001
+++

## Info {#info}

Author: Natalie Zina Walschots

Narrator: Alex McKenna

Published: 2020

Read on: 2021-12-31

Link: [Hench](https://www.hoopladigital.com/title/12688073)


## Review {#review}

Sneaking one last book in at the last minute! This is the fifty sixth book for the year! And who boy was this a fun one.

Anna is such a lovable character, striving to make ends meet while working temp jobs for various Villains in a world where the existence of Super Heroes is a normal society wide thing. Walschots does a great job of blending petty revenge, theft, and kidnapping that many villains partake in with a satisfying serving of fighting the man that is the superhero bureau.

The story starts out with Anna landing a temp gig that allows her talents to shine, getting a permanent job and eventually a request from the big boss to be present for a public misdeed. In the encounter Anna stumbles in the way of a hero, who causes her serious bodily harm simply brushing her aside. And so begins Anna vendetta to make all superheros, but one in particular, pay for the pain and suffering they inflict on everyday folk.

Her long recovery leaves her with nothing to do but obsessively calculate the cost in human suffering and property damage caused by heroic idiots. She blogs about her calculations and becomes slightly infamous, which gets her noticed by THE super villain Leviathan. With the massive resources he provides and the team Anna is able to hire, Anna is able to really dig in and make super heroes lives miserable until they destroy their own public and private lives. Turns out Anna's super power is spreadsheets and data. Which she puts to great use doing her best to even the scale.

I think my favorite thing about Anna is how painfully bisexual she is. It's never mentioned or labeled, but as the story is told from her point of view, she shares several of her crushes with the reader. It's always delightful.

The last act of the story gets into some....rather intense body horror. I personally loved it, a great adventure of the hero asshole getting just utterly beaten. But if you're squeamish it might get you.

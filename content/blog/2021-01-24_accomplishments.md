---
date: 2021-01-24T20:07:47-05:00
title: "Sunday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Moved all my rss feeds over to elfeed.
2. Learned how to use elfeed
3. Set up emacs-w3m for reading rss articles
4. Had video chat with family

Next time I would like to do better on:
1. Getting better sleep. I am exhausted today.

Tomorrow I would like to:
1. Sleep.
2. Do a HBPS with Grandpa about the legacy project.

+++
title = "Today's (And Yesterday's) Accomplishments"
description = "Not a lot of progress on projects, but good days nonetheless."
date = "2020-09-30"
draft = "False"
categories = ["blog"]
+++

Yesterday I accomplished:
1. Took my Grandparents to get their flu shots.
2. Took my Grandpa to his doctor appointment.
3. Took my Grandpa to get labs done.
4. Had therapy.

Today I accomplished:
1. Reinstalled Debian on a laptop to use for zoom meeting.
2. Listened to and participated in Homerathon.
3. Cleaning the house a bit.

Something I would like to do better:
1. I struggled to tell my grandpa that I was going to be busy for the Homerathon. I was afraid to speak up about what I was doing. I would like to be more open about my activities and my hobbies with him.

Tomorrow I would like to:
1. Look into setting up a jitsi server for the alum group (ideally I'd run just one and point a couple urls at it, idk if I can set up tls for that though).
2. IF I get that done, send out an email to that alum mailing list.

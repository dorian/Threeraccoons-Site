+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-10-15"
draft = "False"
categories = ["blog"]
+++
Today I accomplished:
1. Polished this script up to work better.
2. Attended Ben's workshop through the Classics dept
3. Found a good tutorial on setting up a mail server. Still need to read through it all.
4. Started fixing Grandpa's jeans, got the zipper back on, just need to order new zipper stops.
5. Installed and started patching surf. The patching doesn't seem to be working, so I need to figure that out tomorrow.

Next time I would like to do better on:
1. Today was a good day.

Tomorrow I would like to:
1. Read through the guide from Ars Technica on setting up a mail server
1. Learn how to patch surf properly
1. Look into scholarships for edx to get some certifications for tech stuff

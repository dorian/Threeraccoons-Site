---
date: 2021-05-04T15:30:08-04:00
title: "Tuesday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Had Therapy
1. Mucked around in Mastodon
1. Took Grandma to the dentist

Next time I would like to do better on:
1. Sharing my emotions with my grandparents instead of letting them fester.

Tomorrow I would like to:
1. Resume study of Statistics

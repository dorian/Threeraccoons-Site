+++
title = "Land of Big Numbers"
author = ["Dorian Wood"]
date = 2022-04-05T16:51:00-04:00
lastmod = 2022-04-05T16:51:28-04:00
tags = ["book-review"]
draft = false
weight = 3002
+++

## Info {#info}

Author: Te-Ping Chen

Narrator: Christopher Naoki Lee, Eddy Lee, Fiona Rene, Matt Yang Kin

Published: 2021

Finished on: 2022-03-30

Link: [Land of Big Numbers](https://www.hoopladigital.com/title/14795693)


## Review {#review}

This one is a set of short stories that all involve a few days in the life of Chinese people from all walks of life. There is the person who left their small town and abusive boyfriend to work in the city. The emigrant to the US who struggles to connect to those around her because of the culture differences. There's even the American Widow who travels to her husband's hometown in China after his suicide.

This was a fun book. Not much of it really stands out on its own, but in the big picture it all folds nicely into a big picture of society and the world. This big mass of dirt and iron flying through space is populated by all kinds of people living all kinds of lives, and it makes the whole thing more special to peek in a what it means to be just one person in a land of billions.

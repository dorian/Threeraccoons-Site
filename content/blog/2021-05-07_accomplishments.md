---
date: 2021-05-07T20:19:42-04:00
title: "Friday's Accomplishments"
draft: false
categories:
- blog
---
For the last two days.

Today I accomplished:
1. Check RSS feeds
1. Check email
1. Work on Knitting project
1. Visit with family & play cards

Next time I would like to do better on:
1. Communicating with Grandpa. And working more to see things from his point of view.

Tomorrow I would like to:
1. Study statistics
1. Work on Mastodon stuff

+++
title = "Death's End"
author = ["Dorian Wood"]
date = 2021-12-16T14:32:00-05:00
lastmod = 2021-12-19T20:47:21-05:00
tags = ["book-review"]
draft = false
weight = 3001
+++

## Info {#info}

Author: Cixin Liu

Translator: Ken Liu

Narrator: P.J. Ochlan

Published: 2016

Read on: 2021-12-15


## Review {#review}

I neglected to mention in the review for the last book: Liu has some...distasteful opinions of what femininity and autism are. They are not major points of the plot and come up mostly just in passing, but nevertheless they are there.

The conclusion of the series was a bit hard to bear at times. It's really depressing, full of twists that are nearly always not the outcome that you want as a reader. That said, it was still a fun read! Each twist left me with renewed hope for another possible positive ending for the characters we were following at that moment. Liu is imaginative in inventing new ways for human beings to strive to continue living, striving all the way to the end of the universe for just a little more life. Such is the nature of life is it not?

My favorite twists were near the very end, not anything to do with what happens to the characters but with a suggestion of the possible ways the universe is constructed. It was an interesting picture of what an edenic state of the universe might be like.

It took me all the way to the conclusion of this grand adventure before I was willing to accept Liu's central theme (look at me talking like I know how to do literary analysis ;P). Perhaps part of this was simply the fact that the theme is less familiar to my American perspective than to Liu's Chinese audience. It is a theme that I have encountered in a good deal of other Asian literature as well: You have a duty to that which came before you, and that which will come after you. We all occupy a place in time, and that very fact puts us in relation to the rest of the universe. As much as we may have our own desires for how we would like to live out our life, we have a responsibility to the rest of existence around us. This responsibility does not mean that we can't live out our lives in happiness, but it does require of us that we do our part to ensure that life goes on after we are done.

---
title: "Use Old Tech, Save The Planet"
date: 2020-12-25T18:50:51-05:00
draft: true
categories:
- article
---

## Use old tech; Save the planet

Making new computing devices is bad for the environment. It requires a lot of resources, from the mining of resoures, to the industrial processes required to manufacture complex microprocessors. And then there's the old tech that gets thrown away leaving heavy metals to leech out of landfills. The consumption cycle of computational devices is a problem for our planet, and it's mostly unecessary. 95% of the work that we need to do on computers can be done by machines that are a decade old. I personally use a twelve year old laptop to do all my writing, communication, and server administration on. How could we shift our attitudes to make the computational devices we already have better serve our needs? After all, no one buys a new computer because they want to harm the planet. We buy new computers because for some reason the old ones are no longer serving our needs.

Software updates are unfortunately a driving factor in pushing people to purchase new devices. As the sofware we use adds new features and releases new versions that frequently comes with a need for more system resources. The thing is that new software doesn't inherently need more resources, but frequently they are built to use more resources simply because developers expect their software to be used on new hardware with extra resources. I use a twelve year old machine but all the software is up to date. I rely on a great many technical and convenience based software developments that didn't exist when this machine was first produced. But what I am not using is software that takes a one size fits all approach. Unfortunately when using Windows or MacOS, the only way to take advantage of the latest features is to install the entire updated operating system. That means not just the security updates and convenient productive features, but also all of the fancy widgets, bells, and whistles that got lumped in there. Those things in and of themselves are neither good nor bad, but there isn't really an option to choose whether or not you want them. By using Free and Open Source software, I get to pick and choose which parts I want. I can tweak and fine tune my system to work and behave exactly how I want.

Part of why Microsoft and Apple take the one size fits all approach to their operating system is that that method is better for their bottom line. They will end up with the sale of the software license whether or not their newest version can run on current hardware or not. But if it doesn't run on current hardware, well then they also get the added bonus of the sale of a new machine. Corporations don't care if technology gets used in a way that further harms the planet or not, their financial records don't reflect harm done to our planet. So if they release new software that because of it's design also encourages the release and sale of new hardware, then that can increase their profit margin without any negative effects to them. Financial incenctives very much encourage software engineering practices which cause greater harm to the planet.

Software design practices aside, hardware manufacturers obviously have a great many incentives to push the sale of new computers and hardware. This is hardly unique to digital tools. Manufactuers of all kinds of goods have for many years sought to make their machines difficult or impossible to repair because that means when they break

* computational devices are tools
* if we take the approach of how to make tools do the work we need, we can approach and utilize the tools we have much more efficiently
* new fancy hardware is not actually necessary in order to accomplish 95% of our daily tasks.
* We get pushed to buy new hardware in several ways:
	* machines that are difficult or impossible to repair
	* software companies that take a one size fits all approach
	* "features" that use a lot of computational resources while not adding equal functionality
	* marketing hype
* Much of this is fueled by the fact that these things serve to make corporations more profit

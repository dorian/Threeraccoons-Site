+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-11-19"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Darned 5 socks!
1. Worked on prettifying a website project
1. Took Grandma for a ride
1. Took Grandpa to medical appointment

Next time I would like to do better on:
1. Nothing comes to mind today

Tomorrow I would like to:
1. Continue knitting socks for Christmas
1. Read either some journal articles or some Madness & Civilization

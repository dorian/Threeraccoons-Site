+++
title = "Today's Accomplishments"
description = "Mostly just doing chores around the house today."
date = "2020-09-27"
draft = "False"
categories = ["blog"]
+++

Today's Accomplishemnts:
1. Practiced Ukulele
2. Make lunch (Grilled Cheese and Tomato soup)
3. Made supper (Zucchini soup in the slow cooker)
4. Practiced more Ukulele

Things I would like to improve:
1. I could make nice suppers easier on myself by chopping up what I need chopped a day before hand. That way I could spread out the labor instead of just having to do it all at once. I'm not sure I will, but it's a thought.
2. Not much honestly. It's a good day.

Stuff I want to accomplish tomorrow:
1. fix the searx url issue
2. make user dirs live and move my blog into a user for me
3. finish a rough draft of the article explaining servers and clients

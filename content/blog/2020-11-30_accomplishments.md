+++
title = "Monday's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-11-30"
draft = "False"
categories = ["blog"]
+++

The redesign is live! I still have some tweaks I want to make here and there, but I think this is done for now. Especially since I really need to be completing other tasks instead of obsessing over this more haha. If you want to read some more info about the code I'm using for this theme you can check that our [here](https://threeraccoons.com/projects/mywebsite).

Today I accomplished:
1. Worked on my website redesign! I think it's ready actually. This post will go up with the new theme.
1. Took Grandma for a ride, did errands with Grandpa at the same time.
1. Set up a second monitor to aid in website development! It works really well.

Next time I would like to do better on:
1. Navigate my feelings of guilt about where I spend my time.
1. Schedule my day better with Grandpa and Grandma to just avoid feeling guilty in the first place.

Tomorrow I would like to:
1. Flesh out my steps for setting up a SMTP server.
1. Set up the Mastodon server for the alum group.
1. Knit sock.
1. Have therapy

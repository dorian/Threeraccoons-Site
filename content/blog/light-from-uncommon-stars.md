+++
title = "Light From Uncommon Stars"
author = ["Dorian Wood"]
date = 2022-09-12T22:07:00-04:00
lastmod = 2022-09-12T22:07:48-04:00
tags = ["book-review"]
draft = false
weight = 3001
+++

## Info {#info}

Author: Ryka Aoki

Narrator: Cindy Kay

Published: 2021

Link: [Light From Uncommon Stars](https://share.libbyapp.com/title/6011269)


## Review {#review}

This book is such a wild mix of genres and I love it so much! And Aoki combines them in such a wonderful way and gives us a happy ending intricately tying them together. I'm still a little amazed at how much I liked every character in this book.

I also was enthralled as Katrina begins to blossom under the care of Shizuka. We get to see this wonderful young trans woman grow in confidence and shine in ways that none of Shizuka's students ever had before.

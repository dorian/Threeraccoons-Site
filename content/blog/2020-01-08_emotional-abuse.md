+++
title = "Emotional Abuse"
description = "Reflections on Men. Abuse. Trauma. by PhilosophyTube"
date = "2020-01-08"
categories = ["article"]
draft = true
+++

I am marking this as a draft. It is still in my site repo, but should not appear on my public website. I am doing this because I would no longer use these words to describe my situation, but I do want to keep this. A personal website is a journal after all, and I want to preserve my own record.

I watched Philosophy Tube’s youtube video titled “Men. Abuse. Trauma.” recently. It’s a really intense video, but in an open, vulnerable, and supportive way. It resonated with me in ways that I did not expect at all. I’m not a man. I’ve identified as non-binary for four years at this point. But I was raised as a man, and I saw myself as a man for the first 25 years of my life. And I was abused during that time. So when men talk about their abuse, it hits home.

My abuser was very different from Philosophy Tube’s, but every feeling of worthlessness and lack of creativity he talked about felt like my feelings. Quite honestly it triggered me. I think it’s a very good video, and totally worth watching. But it was hard for me. I’ve moved on, I moved out of the house I was living in with my abuser (it still feels so awkward to use that word), and I do my best to not think about them much. But Philosophy Tube reminded me, your abuse sticks with you. It affects how you see the world, your reactions to a lot of things, and you have to live with that and manage it. You can’t really move on, you have to just move forward. It’s not really all a dark picture, there’s so much room for healing and growth. It’s just that my abuse is kind of permanently part of my journey through life.

Watching that video, at one point through tears, really had me in a mood. For some reason, I forget why, I thought about my last relationship and my ex. It was not a good relationship. I’m not going to attribute malice to my ex. I have no idea what they were thinking honestly. I do know however that we were highly dysfunctional together. I was so enamored with them the whole time, I really adored them and wanted them to be happy. That made it really easy to engage behaviors I learned because of my abuse. I avoided confrontation like the plague. When I realized my ex and I shared different opinions on anything I carefully avoided ever bringing that topic up. Thanks to therapy I had started around the time we broke up, I felt more comfortable voicing my opinions. My ex and I tried to be friends after our breakup, but the first (and I think only?) time I expressed that I felt differently than them on a topic they ended every shred of friendship and communication we had left. That hurt, a lot. I still feel like I need to diminish that event, talk about how our disagreement was over a fairly serious political position. But honestly? That’s fucked up. To be so unable to have a close friend have even one differing opinion from you that you just end all communication and never speak to them again is extreme and not healthy in the least.

A lot of this lives in the same place that my love of philosophy arise from. I truly believe the space to tell our stories, and thus to reflect on them and search for meaning is a key ability to have in our search for a better life and a future worth living.

+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-10-27"
draft = "False"
categories = ["blog"]
+++

I haven't made a post in about a week. Lots of this stuff is what I've accomplished over the past few days, and not just today.

Today I accomplished:
1. Finished a sock
1. Started a new sock
1. Washed Laundry
1. Took care of laundry
1. Read up on setting up Prometheus
1. Read up on setting up Mastodon
1. Took Grandma to eye doctor apt
1. Took Grandma and Grandpa for a ride
1. Had therapy, brought up stuff that I was hesitant about

Next time I would like to do better on:
1. Speaking up for my needs/desires in therapy in the moment rather than stewing on it.

Tomorrow I would like to:
1. Knit
1. Set up Mastodon
1. Look into EFF resources for teaching Security/Privacy basics

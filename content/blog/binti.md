+++
title = "Binti"
author = ["Dorian Wood"]
date = 2021-08-17T09:29:00-04:00
lastmod = 2021-12-19T20:47:26-05:00
tags = ["book-review"]
draft = false
weight = 3015
+++

Author: Nnedi Okorafor

Narrator: Robin Miles

Published: 2015 - 2018

Titles:

Binti; Read on 2021-08-04

Binti: Home; Read on 2021-08-14

Binti: The Night Masquerade; Read on 2021-08-17

This series was so good. I am really enraptured by the theme of growth, change, and family. Binti takes so many courageous steps and each time becomes more than what she was. Both in personal growth and real tangible ways. Binti, as she goes through the events of these novels changes the world around her in profound ways simply because she dares to take the steps that are uniquely hers. Each climax and resulting shift of the world in resolution of the story is only possible because Binti chooses to grow and accept a dramatic shift and growth in who she is as a person. And yet she never loses herself, it is always growth, not transformation.

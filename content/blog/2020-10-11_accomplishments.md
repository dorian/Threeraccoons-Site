+++
title = "This Weekend's Accomplishments"
description = "A chill weekend, in which I knocked out some easy tasks"
date = "2020-10-11"
draft = "False"
categories = ["blog"]
+++

This Weekend's Accomplishments:
1. Finished HTML intro at freecodecamp.org
2. Finished CSS intro at freecodecamp.org
3. Halfway through the Applied Visual Design at freecodecamp.org
4. Phone call with a friend
5. Took Grandma for a ride
6. Adjusted ram settings on the two main processes in the jitsi server, will have to see how this affects performance.

I would like to do better:
It was a good weekend. Nothing blatant to improve upon.

Tomorrow I would like to:
1. Hold meditation session with peeps
2. Work on website theme
3. Make progress on freecodecamp.org
4. Love myself

+++
title = "Welcome To The Fediverse"
description = "Come and explore ways to build online communities outside of mainstream social media."
date = "2020-10-02"
draft = "False"
categories = ["article"]
+++

## Part 1:

There is actually an entire movement on the internet about building communities and social media in a way that centers user privacy as well as open communication. I bring this up before part two because in some ways, there's just always more to discover, this isn't about a single website that respects you and your privacy, this is about participating in a movement of like minded individuals across the internet, and working with them to build a community worth having. Also, not all projects are connected. I like this stuff, and so I read about it and explore. About once a week or so I learn of another group that's doing some kind of online collaborative community project along these lines. I say that to point out that this stuff is decentralized, there's no one place to go for everything. I will point you to a couple sites that will probably offer you everything you want, but unlike mainstream social media sites that most people are familiar with, at no time is there only one website to go to for something.

## Part 2:

Welcome to the Fediverse. Fedivers is a portmanteau of Federated and Universe. It's used to describe social media websites that operate and communicate with each other in a particular way. That is, social media sites that operate in a federated manner, much like email does. When you sign up for an email account, you can not only email all the other people who have an account with the same company as you, but you can also email people who have email accounts with other providers! Similarly when you sign up for an account on mastodon.social (or any other website that uses the same ActivityPub protocol) you can view posts and interact not only with posts and users on mastodon.social, but any other federated instance using the ActivityPub protocols.

The beauty of this is that anyone can run an instance of Mastodon (A social media framework that functions similar to twitter) or any other ActivityPub social media and particpate with the wider community. If you don't want to host your own, you can look for people who host an instance that lines up with your hobbies, beliefs, or geographic location! This provides an entirely new ways to build communities that simply aren't available on mainstream social media sites. Are a dad who wants to hang out with other dads? Head over to [dads.cool](https://dads.cool/about) and browse some user profiles or sign up for an account with them. If you have an account with another Mastodon server and don't want to create another one, you can follow any public profiles with your existing account and still particpate with that community. Do you live in Glasglow? Check out [glasglow.social](https://glasgow.social/about). Or maybe you and a group of friends want a private social media account just for your group. You can host your own and make all profiles and posts private, not accessible to the outside world. You can tailor your social media to suit your desire and need for privacy and public posts as you want. You can find a non exhaustive list of Fediverse communities at the [Fediverse.party Portal](https://fediverse.party/en/portal/servers).

## Part 3: Bonus!

The Fediverse isn't the entirely of this movement. There are other groups out there building internet communities in ways that empower users rather than treating them as profit sources. [Matrix](https://matrix.org/) offers a way to have modern, rich text, encrypted communication with your friends. If you're interested in using shared Unix systems (it's incredibly fun!) check out the [tildeverse](https://tildeverse.org/).

It's a bit old school, but maybe you just want to run a blog, share it with your friends, and read their blogs. There are infinite ways to be social on the internet, we don't have to settle solely for what big tech will offer for us.

You can find my blog and projects at [Threeraccoons](https://threeraccoosn.com).

You can find me on mastodon at @dorian@litterae.social.

If get stuck with anything feel free to email me any questions at dorian at this domain name (email obscured to prevent bots from spamming me).

---
date: 2021-01-12T20:18:22-05:00
title: "Tuesday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Backed up the mastodon server
2. Made a script for backing up the mastodon server in the future
3. Upgraded the mastodon server
4. Had a call with friends

Next time I would like to do better on:
1. Communicating with my mother
2. Expressing my feelings to my family in general

Tomorrow I would like to:
1. Set up cron to run backup script weekly
2. Make progress on SysAdmin course
3. Read Foucault

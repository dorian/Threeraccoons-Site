+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-11-16"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Grandma's doctor appointment
1. FCC site project
1. Held a weekly together meditation session

Next time I would like to do better on:
1. I don't know. Today wasn't perfect but it was better than recent days. In general I just want to continue making progress on projects.

Tomorrow I would like to:
1. Prettify FCC site project
1. Knit
1. Read some of Madness and Civilization

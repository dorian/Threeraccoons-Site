---
date: 2021-01-11T19:31:41-05:00
title: "Monday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Set up dotfiles on pi4
2. Set up transmission on pi4
3. Performed backup of mastodon server
4. Went for a walk
5. Gave Grandma a bath
6. Knitted some on a sock

Next time I would like to do better on:
1. I'm not sure.

Tomorrow I would like to:
1. Automate future backups for mastodon server
2. Set up samba server on pi4
3. Have therapy

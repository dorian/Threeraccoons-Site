+++
title = "My First Sock!"
description = "I knitted my first sock, now on to the matching one."
date = "2019-03-21"
categories = ["blog"]
+++

# My First Sock!

I finished knitting my first sock today! It's very colorful and I'm pretty happy with it. I've already started on the matching pair to this sock, I'm hoping to wear them around a bunch this summer.

[Sock](local:../media/images/first-sock.jpg)

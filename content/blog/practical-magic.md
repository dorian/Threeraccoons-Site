+++
title = "Practical Magic"
author = ["Dorian Wood"]
lastmod = 2022-07-11T16:48:01-04:00
tags = ["book-review"]
draft = true
weight = 3002
+++

## Info {#info}

Author: Alice Hoffman

Narrator: Christina Moore

Published: 2012

Started on: 2022-07-06

Finished on:

Link: [Practical Magic](https://www.hoopladigital.com/title/11904833)


## Review {#review}

---
date: 2021-01-19T19:33:53-05:00
title: "Tuesday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Resume draft
2. Therapy
3. Practiced Ukulele
4. Went for a walk

Next time I would like to do better on:
1. Self Compassion
2. Engaging others

Tomorrow I would like to:
1. Edit my resume
2. Practice Ukulele
3. Go for a walk

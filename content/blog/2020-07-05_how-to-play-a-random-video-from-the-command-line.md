+++
title = "How To Play A Random Video From The Command Line"
description = "A shell oneliner to play a random video file"
date = "2020-07-05"
categories = ["article"]
+++

So I’m writing this blog post because, well, I’m bored. And I think this is pretty neat. Plus this shows just how incredibly useful pipes are on unix and unix like systems. I would even go so far as to say pipes have the power to fundamentally change how you use your computer.
tl;dr

For those who don’t want the explanation, here’s the command to play a random video file in your current folder:

```
mpv "$(ls -p | grep -v / | shuf | head -1)"
```

Breaking it down

Let’s break down what that one liner is doing.
```
mpv
```

This is the program that’s going to play the video. Mpv is my favorite video player. I use it to watch movies, stream videos from the web, and occassionally play music.

```
$( )
```

This isn’t technically a command, but it’s used to tell your shell to evaluate the commands we give it in and then return the result (it will be a file name) to mpv. The double quotes are necessary because some people put spaces in their filenames. Heathens.
```
|
```

This is a pipe. It’s used to pass the output from one command into the input of another commands. It enables us to do really complex tasks with very simple programs.

```
ls -p
```

Simply ls command to list the files in our current directory. The -p option tells ls to print directories with the trailing / at the end. We need it for the next part.

```
grep -v /
```

Using the pipe to pass the list of file to grep, we tell grep to find all the files that have a / in them. The -v option inverts the selection, so grep will pass on only the files that don’t have a /; i.e. everything except directories.
shuf

This gives us the list of files in a random order

```
head -1
```

The head command is used to trim outputs. By default it prints the first ten lines, so we use the option -1 to tell it we only want the first line (You can use tail to print only the end of results. Useful for logfiles).

The End

And there you have it. A fairly simple shell one liner to play a random video file. Perhaps I’ll make this a series. I could call it “Learning The Power Of The Command Line Through Mostly Useless One Liners”.

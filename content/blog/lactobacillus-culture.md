+++
title = "Making My Own Lactobacillus Culture"
author = ["Dorian Wood"]
date = 2022-07-13T16:29:00-04:00
lastmod = 2022-07-14T19:43:07-04:00
tags = ["permaculture"]
draft = false
weight = 3001
+++

## Why Culture Lactobacillus? {#why-culture-lactobacillus}

Lactobacillus is the primary microorganism used in bokashi composting. They are used to ferment food scraps (I'm curious to see if it works well on other organic material as well) in order to quickly turn them into compost material. Lactobacillus is also used in yogurt and cheese production, but I'm not cultivating it for that.


## How To Get Lactobacillus {#how-to-get-lactobacillus}

You can buy inoculated medium for bokashi online for not too much. In fact I just ordered some earlier today because I wanted to get a bokashi bucket started while I wait for my culture to develop. Look for "bokashi bran" or "bokashi compost starter".

The other way to get the lactobacillus bacteria is to cultivate it yourself! There are three basic steps to get the lactobacillus, and two more steps to preserve it (a useful but not mandatory step!)


### Step 1: Rice Wash {#step-1-rice-wash}

All you need to do is combine equal amounts rice and water in a  container!

{{< figure src="/images/20220713_110615_web.jpg" >}}

Mix it around and the water should turn cloudy. Drain the water off into a suitable container, this is what you will use to cultivate the lactobacillus. Use the rice in your next meal, we no longer need it for this project.

![](/images/20220713_121731_web.jpg)
![](/images/20220713_110729_web.jpg)

Next cover the container with a cloth or otherwise permeable lid to keep foreign materials out, and set it aside in a warm place for a day to go to work.

![](/images/20220713_111225_web.jpg)
![](/images/20220713_111233_web.jpg)

This is as far as I've gotten so far! Tomorrow I'll be adding milk to the mixture to feed the lactobacillus, and then we will need to wait a week or two for them to do their thing.


### Step 2: Add Milk {#step-2-add-milk}

After you let the rice wash sit for a day, it's time to add milk. 10:1 milk to rice wash is the recommended ratio. This process will create gas so you need a way to release that. If you have a securely fastened cap you'll need to release it manually. I've placed a non-locking lid on mine with some weight on it, so that as the pressure builds it can release naturally and reseal to keep out stray microorganisms.

![](/images/20220714_150824_web.jpg)
![](/images/20220714_150833_web.jpg)


### Step 3: Extract The Whey; Feed The Lactobacillus {#step-3-extract-the-whey-feed-the-lactobacillus}

After a week of so curds should develop at the top of the milk. We are interested in the whey however. Either remove the curds to access the whey or use a syringe or siphon.

At this point some kind of sugar needs to be given to the bacteria to feed it. Molasses was recommended by several folks, although I have seen other types of sugar used as well.

This should be a completed lactobacillus culture! It's all ready to use now. You can spray it on compost or as you add organic material to a bokashi bucket. If you want to store it for later use however you will want a medium to adhere it to.


### Step 4: Apply To A Medium {#step-4-apply-to-a-medium}

The medium can be any dry porous woody material. It's often applied to leftover brewing material. Sawdust can also be used, rice husks, etc.


### Step 5: Dry For Storage {#step-5-dry-for-storage}

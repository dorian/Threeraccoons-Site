+++
title = "The Smart Nonprofit"
author = ["Dorian Wood"]
date = 2022-04-17T10:23:00-04:00
lastmod = 2022-04-17T10:23:34-04:00
tags = ["book-review"]
draft = false
weight = 3002
+++

## Info {#info}

Author: Beth Kanter, Allison H. Fine

Narrator: Kim Niemi

Published: 2022

Started on: 2022-03-31

Finished on: 2022-04-12

Link: [The Smart Nonprofit](https://www.hoopladigital.com/title/14845212)


## Review {#review}

I found this book really well put together! I think Kanter and Fine do a great job of communicating the concepts and tools of digital technology, artificial intelligence in particular, in phrasing that makes sense to non-technical folks.

I was really impressed at the variety of ways they pointed out that digital automation can help make non-profit efforts more impactful. And into this they did an excellent job of pointing out the ways in which digital automation can amplify existing biases and do far more damage than humans alone. They have a keen awareness of how the injustice that already exists in the world informs the production of these systems, and how that heritage can result in profound biases built into the core of our social systems.

This book is a great read if you want to think about how we can focus our social change efforts in ways that involve technology in the most ethical way, minimizing the harm we do and maximising the good.

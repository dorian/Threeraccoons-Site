---
date: 2021-01-17T19:49:24-05:00
title: "Sunday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Worked
2. Picked up Grandma's meds
3. Had video call with the family
4. Worked on Resume

Next time I would like to do better on:
1. Sharing my ideas and dreams

Tomorrow I would like to:
1. Cobble together resume draft
2. Make progress on SysAdmin course/book

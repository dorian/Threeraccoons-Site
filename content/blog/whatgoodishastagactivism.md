+++
title = "What Good Is Hashtag Activism"
description = "A great review from Jacobin on a recent publication about Twitter and Hashtags."
date = "2020-12-01"
draft = "False"
categories = ["blog"]
+++

Thoughts on this article from Jacobin: [The Problem With Hashtag Activism](https://jacobinmag.com/2020/12/hashtag-activism-review-twitter-social-justice/)

I think this is a really poignant look at the tools that current social media offers us to affect real political change in our world. It's a solid critique of the shortcomings we see in mainstream social media.

Most of those shortcomings stem largely out of the fact that social media sites like Twitter and Facebook are monolithic privately owned entities. They aren't actually public spaces. We can overcome those shortcomings by building our own social media spaces.

Tools like Mastodon, Peertube, and PixelFed already exist for us to build and develop our own social media communities. These tools can be leveraged in ways that tie concretely to legitimate organization efforts. While I don't naively think this is a magic bullet to solve our political ills, I am convinced these projects can offer us avenues to reap the benefits we've witnessed from social media already, while constructing digital communities that mirror real life ones, thus maximizing their impact while minimizing the harm perpetrated through any single platform.

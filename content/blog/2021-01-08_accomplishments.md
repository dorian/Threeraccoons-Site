---
date: 2021-01-08T19:59:04-05:00
title: "Friday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Reinstalled Fedora on VM with proper iso for SysAdmin course
2. Got ArchLinuxArm running on the Pi
3. Gave Grandma a bath
4. Moved the bamboo plant to a new home! A proper pot that drains.
5. Caught up with news

Next time I would like to do better on:
1. Picked tasks to work on. I didn't get much done today because I tried working on three projects at once.
2. Sleep. I also didn't get much done today because I was up too late last night. When I'm low on sleep I don't work well.

Tomorrow I would like to:
1. Tomorrow I have work, so that's really the only concrete thing on my todo list.

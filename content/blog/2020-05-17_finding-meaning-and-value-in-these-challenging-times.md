+++
title = "Finding Meaning And Value In These Challenging Times"
description = "I attempt to use Stoicism to improve my emotions through contemplation."
date = "2020-05-17"
categories = ["article"]
+++

I am working through the Stoic Mindfulness and Resiliency Training over at learn.modernstoicism.com currently. This week the exercises are all about identifying our values. I find it quite interesting how values are one of those things that easily get pushed to the background of our lives as we try to go about daily life. Work and errands and chores often feel more imminent than trying to lead a fulfilling life, or whatever it is that we might ascribe as our core life values.

Exploring my values is both more difficult and more necessary than I was expecting going into these exercises. The philosopher aspect of me wants to be sure that what I list is what I truly value, and not just a default answer or what I think I should value. It’s easy to live life on autopilot, merely taking care of what needs to be taken care of in the moment so that we can make it to the next. Taking a step back to contemplate the autopilot that life gets on is very enlightening.
Core Values

It’s worthwhile to sit down and put serious consideration into what we value. I encourage you to pause and contemplate and write about these things if you have not done so recently. Not just a passing thought or two.

    What is ultimately the most important thing to me?
    What do I want my life to “be about”?
    How do I want to be remembered?
    What do I want to spend my life doing?

I don’t think there’s any way to answer these questions wrongly, most of their value comes from answering them ourselves.
Does this action…

This was the part of the exercises that really surprised me. The Stoic practice of rationally reflecting on ourselves and asking if we are living according to our values. Does this particular action I want to take match up with my values? By contemplating this for even small actions, I am given a concrete path to walk in order to become the person I truly want to be. Even if I don’t choose to do anything different after such contemplation, the exercise still is transformative. Already, after just one morning of working on these things I have chosen to undertake daily activities with more kindness and compassion. Simply because I understood that I would rather be a kind person than a frustrated person, I was able to set aside frustration and chose kindness. I have performed the same actions, but I performed them in a different way, and in doing so I have become more like the person I want to be.

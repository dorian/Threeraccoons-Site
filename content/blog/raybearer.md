+++
title = "Raybearer"
author = ["Dorian Wood"]
date = 2021-10-18T09:10:00-04:00
lastmod = 2021-12-19T20:47:23-05:00
tags = ["book-review"]
draft = false
weight = 3008
+++

Author: Jordan Ifueko

Narrator: Joniece Abbot-Pratt

Published: 2021

Read on: 2021-10-12

Wow. I was so enamored with this book nearly from the beginning. And I just learned there's a sequel!? Sign me up!

Raybearer is a delightful journey with children destined to be the emperor's council in a high fantasy setting. While it does have court intrigue, those elements always were part of the stories of characters I cared about so it was thrilling to see things unfold. I really loved how this world is built from a Nigerian perspective. I'm quite tired of fantasy stories that take place endlessly in European like settings. It's just old, and exploring this world built from Ifueko's epistemic privilege as a Nigerian American writer was refreshing.

I don't normally like worlds where magic is an innate power one is born with rather than skill, but I really enjoyed this one. The various powers that people had were all sufficiently unique to be interesting, and the main character's was especially so.

I really enjoyed the drama between Sunny and her mother. It was a very poignant portrayal of loving a parent who sees you as an extension of themself and a means to an end.

Sunny's solution to the problem of the novel that was brewing in the background the whole time was truly amazing. I'm so excited to see how it plays out in the sequel.

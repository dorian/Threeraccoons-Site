---
title: "Opting Out of Therapy Tracking"
date: 2021-07-26T14:34:40-04:00
draft: false
---

I have been with one service for therapy for several years now.
I was a bit frustrated to learn recently that they track my use of their services
in order to share data with third party advertisers.
My correspondance with my therapist is confidential according to the laws of my country and they are legally barred from using that content to sell as data to advertisers.
So at least I have a reasonable assurance that my private communication with a therapist isn't being data mined for advertising.

The privacy policy is a bit opaque,
so while it does list what they track and whether it's shared with third parties,
it's not really clear exactly what data they share and with which third parties.
For example, the privacy policy indicates they collect geolocation data and share that with third parties.
Do they share it with emergency services in case someone is a danger to themself?
I have to imagine that one is yes since it's required by law in my country.
But do they also collect location data and sell it to data brokers?

Anyways, after a bit of digging around I found a static link that you can use to access the cookie banner that pops up when you access the site on a new device.
If you use the therapy services of BetterHelp, and would like to opt out of their collection of your data for use via third parties (except as required by law), here are some quick steps:

1. Head over to
[betterhelp.com/opt_out](https://www.betterhelp.com/opt_out/)
and scroll down to the "I want to opt out of tracking" section.
2. The first sentence in that section is about managing your cookie preferences,
and offers a link to click.
Click that.
3. A pop up will come up with several categories.
It is not possible to disable the strickly necessary cookies,
but I personally disable both performance and tracking cookies.

---
date: 2021-03-27T20:45:43-04:00
title: "Saturday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Installed Arch on my T500!
2. Work!
3. Went for a walk.
4. Shared my and Grandma's recordings with a friend!

Next time I would like to do better on:
1. Being mindful.

Tomorrow I would like to:
1. Start a regular meditation practice again. I've been slack on that ever since losing a solid way to track it.

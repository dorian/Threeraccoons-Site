+++
title = "a fire like you"
author = ["Dorian Wood"]
date = 2021-10-18T09:25:00-04:00
lastmod = 2021-12-19T20:47:23-05:00
tags = ["book-review"]
draft = false
weight = 3007
+++

Author: Upile Chisala

Narrator: Upile Chisala

Read on: 2021-10-17

Again. I loved her poetry SO. MUCH. It is really powerful. I absolutely adore the tenacity of self love that Upile brings to her work. It was palbable in "soft magic" and it's just as much so here. I highly recommend this book to everyone, but especially those who were told to be quiet, obedient, and small.

My favorite poem:

A few things you may reconsider:

1.  That knife of a mouth
2.  A liking for the bottle if it comes with a disliking for yourself.
3.  Hiding hot anger under thick laughter.
4.  Talking yourself out of joy more times than into it.

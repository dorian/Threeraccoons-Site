+++
title = "Gathering Moss"
author = ["Dorian Wood"]
date = 2022-03-02T07:15:00-05:00
lastmod = 2022-03-02T07:15:37-05:00
tags = ["book-review"]
draft = false
weight = 3002
+++

## Info {#info}

Author: Robin Wall Kimmerer

Narrator: Robin Wall Kimmerer

Published: 2018

Started on: 2022-02-14

Finished on: 2022-02-20

Link: [Gathering Moss](https://www.hoopladigital.com/title/12215198)


## Review {#review}

I loved this book so much! I really love plants, and the past few years I have been repeatedly delighted by books exploring the plant kingdom. Moss is truly an amazing bit of life on our planet, some of the first photosynthetic life on land! It's truly amazing to think of the smallness of moss and how it thrives despite that. Moss cultivates the kind of climate it needs to grow best! We often think of moss as an inconsequential and limitless thing in the wild. It is so small and unassuming. And yet moss is incredibly old and wise. It is a very slow growing life, sometimes moss you see on a tree will be as old as the tree itself! Take some time to slow down and notice the mossy friends around you.

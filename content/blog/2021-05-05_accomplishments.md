---
date: 2021-05-05T18:53:56-04:00
title: "Wednesday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Resume statistics study
1. Write up book reviews for March and April
1. Write up audio blog
1. Knit
1. Cut up material for compost
1. Put up weekly pills
1. Worked on Knitting project

Next time I would like to do better on:
1. I'm not sure. Today was really good. I wish I didn't waver on trying to pick what to do as much. But the todo list helped, and I think I'm just always going to have a little of that.

Tomorrow I would like to:
1. Work
1. Look at old family photos with Grandpa and Grandma
1. Study stats more
1. Poke at the website a bit, possibly work on refactoring to use taxonomies

+++
title = "Org Capture Templates for Block Scheduling"
author = ["Dorian Wood"]
date = 2021-12-21T09:40:00-05:00
lastmod = 2021-12-21T09:40:55-05:00
tags = ["tech", "emacs", "orgmode"]
draft = false
weight = 3001
+++

## Intro {#intro}

Yesterday I spent several hours hacking away on trying to set up some custom capture templates in Org-Mode trying to streamline my daily block scheduling. I'm not sure what my goal with this post is, I've assumed my audience has not much familiarity with Emacs so it might read a little patronizing. But that's the kind of post I wish I was able to find as I was first exploring Emacs, so that's what I'm doing.

The format for the capture templates is pretty well explained in the documentation, but I really struggled to get it to work properly. A good chunk of that was me messing up syntax, but there was also a fair bit of work I had to do on conceptualizing and actually structuring the template for my uses as well.

So! Onto the code, and I'll point out the stuff that tripped me up along the way.

N.B. Some part of my stack is doing something janky with the code blocks below and overriding my css for the background color of the code blocks. My todo list this morning has working on this blog post, not figuring out that junk :P.


## The Code {#the-code}

Org Capture Templates are lists structured like this:

`(key description type target template properties)`

It looks like a lot but I think it's straight forward once you start building one. `key` and `description` are the items that will be displayed to the user. `type` tells Emacs what kind of thing this template is making. `target` tells Emacs where to put it, and `template` is what you want Emacs to write down. `properties` are extra options, we can ignore it for now.

Let's look at the example given in the [documentation](https://orgmode.org/manual/Capture-templates.html):

```emacs-lisp
(setq org-capture-templates
      '(("t"
         "Todo"
         entry
         (file+headline "~/org/gtd.org" "Tasks")
         "* TODO %?\n  %i\n  %a")

        ("j"
         "Journal"
         entry
         (file+datetree "~/org/journal.org")
         "* %?\nEntered on %U\n  %i\n  %a")))
```

The first line, `(setq org-capture-templates`, is just the bit of elisp that is telling emacs to set a variable. We're setting the variable `org-capture-templates`.

Right below that are the lists of templates being set. In the first one the key and description are `"t" "Todo"`. The key is what you press on the keyboard to select that template. The type for both of these templates is `entry`. There are several other kinds but I haven't used any of them yet. Both of these targets indicate a file (identified by the first string); the first one uses `+headline` to point to a unique headline in the file, and the second uses `+datetree` to tell Emacs to find (or create if not already there) the headline for the appropriate date, defaulting to today if not specified. The string in double quotes (`"`) after that is the template itself, the text that will be put in the appropriate file.

The percent signs indicate expansion that Emacs will do. The coolest one is the `%a` escape, it includes a link back to the place you were when you called the capture template from. This is what makes all of this a "capture". I can open up the file where I have all my projects and their individual todo tasks, point to a particular task I want to do sometime this week and use a capture template to put it in my block schedule with a link. That way when it comes time to work on it I can just look at my schedule and click the link to go directly to the exact spot in the file I need to start doing the work! How cool is that?

So that is the structure of a generic template, here are the templates I made for my block scheduling:

```emacs-lisp
(setq org-capture-templates
     '(("s" "Block Schedule Templates")
       ("s1"
        "05:00 - 09:00"
        entry
        (file+datetree "~/org/blockschedule.org" )
        "* [ ] %a%?\nSCHEDULED:
        <%(org-read-date nil nil org-read-date-final-answer) 05:00-09:00>"
        :time-prompt t)

       ("s2"
        "09:00 - 12:00"
        entry
        (file+datetree "~/org/blockschedule.org" )
        "* [ ] %a%?\nSCHEDULED:
        <%(org-read-date nil nil org-read-date-final-answer) 09:00-12:00>"
        :time-prompt t)

       ("s3"
        "12:00 - 15:00"
        entry
        (file+datetree "~/org/blockschedule.org" )
        "* [ ] %a%?\nSCHEDULED:
        <%(org-read-date nil nil org-read-date-final-answer) 12:00-15:00>"
        :time-prompt t)

       ("s4"
        "15:00 - 18:00"
        entry
        (file+datetree "~/org/blockschedule.org" )
        "* [ ] %a%?\nSCHEDULED:
        <%(org-read-date nil nil org-read-date-final-answer) 15:00-18:00>"
        :time-prompt t)

       ("s5"
        "18:00 - 21:00"
        entry
        (file+datetree "~/org/blockschedule.org" )
        "* [ ] %a%?\nSCHEDULED:
        <%(org-read-date nil nil org-read-date-final-answer) 18:00-21:00>"
        :time-prompt t)
       ))
```

Some of the have two letters for the `key`, that just means first I press `s` and then the appropriate number. Emacs automagically turns that into a nice little sub-menu.

The `:time-prompt` property was one of the bits that gave me a lot of trouble. It makes it so that a calendar pops up in Emacs so you can select time and day. When I tried to include it Emacs kept throwing errors when I tried to use the capture template. Eventually as I was searching the internet about this issue I ran accross [this reddit thread](https://www.reddit.com/r/orgmode/comments/nmgs2i/hey%5Forgmode%5Fusers%5Fshow%5Fus%5Fyour%5Forg%5Fcapture/) where people were sharing capture templates. I saw someone had used the `:time-prompt` property but also included the `t` boolean. I had just assumed because the `:time-prompt` property was clearly boolean simply providing it was enough to assume that I wanted to turn it on haha.

One other bit was difficult, but I found my answer much more easily.

```nil
SCHEDULED: <%(org-read-date nil nil org-read-date-final-answer) 18:00-21:00>
```

I wanted to include the date in the template, so that I could make things automatically be "Scheduled" in Org-Mode and thus show up in the main agenda. The `%t` and `%T` expansions do do this just fine, but they would require me to type out the time codes for my blocks by hand each time, which was THE bit of repetitive work I was trying to avoid with all this from the start. Luckily I found [Sacha Chua's blog](https://sachachua.com/blog/2015/02/org-mode-reusing-date-file-datetree-prompt/) about doing this exact kind of thing. Ironically she did the work of figuring out how to do this because she didn't know about the `%t` expansion. The part I yoinked from Sacha was the `%(org-read-date nil nil org-read-date-final-answer)` snippet. That just pulls the date selected by the time prompt, but does not create the whole timestamp. Ideal for me because I want to construct the full timestamp in the template.

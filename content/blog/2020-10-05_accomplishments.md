+++
title = "Today's Accomplishments"
description = "Today was mostly full of chores and errands."
date = "2020-10-05"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Gave my Grandmother a bath
2. Finish a pair of socks for Grandpa (I technically did this two days ago, but it's more effort than it's worth to go back and fix that tbh).
3. Got my flu shot
4. Finished a puzzle with Grandpa
5. Logged into my matrix server with weechat. I still have to do some configuring before it's usable.

I would like to do better next time:
I am working on not engaging with some really nasty self deprecation. Today and the past two has been a battle. I'm doing better, slowly but surely.

Tomorrow I would like to:
1. Have therapy
2. Fill out my ballot
3. Configure Weechat
4. Phone call with Melissa

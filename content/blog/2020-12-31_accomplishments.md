---
date: 2020-12-31T20:30:54-05:00
title: "Thursday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Made two batches of fudge! Grandma's recipe.
2. Made Baked Apple Cake. Pastor Dan's recipe.
3. Packaged some of up said deserts for my parents and little sister!

Next time I would like to do better on:
1. Listen more carefully for how Grandpa and Grandma communicate their feelings towards me.
2. Figure out how to seek help when I need it.

Tomorrow I would like to:
1. Clean the center island in the kitchen.
2. Work on redoing my website navigation.
3. Work on Use Old Tech article
4. Read some Foucault.

---
title: "May - June Book Roundup"
date: 2021-07-07T07:49:55-04:00
draft: false
categories:
- article
tags:
- book-review
---
Running more than a few days late with this one. I had forgotten all about it until a few days ago honestly. I've read some really interesting books the last two months, although the last two weeks has been mainly podcasts.

## Books
### Dare to Lead
#### Audiobook; Brene Brown; Published: 2018; Read on: 2021-05-03

This book is something that I need to come back to. There are lots of challenges in here about how I need to dare to be vulnerable so that I can have access to those things which I am truly excellent at and offer them to others. The end of the book was also a huge uplift for me. Brene talks about resiliency and some of the habits and tendencies that make for resilient people. Several of the items are things I already do, and things that I have picked up from other sources that those she talked about. That's exciting to me. It's both a sign of my own skill, and also really interesting evidence for the many ways we can learn of, practice, and achieve resilience.

### soft magic
#### Audiobook; Upile Chisala; Published: 2019; Read on: 2021-05-05

A very delightful collection of poems. I was particularly moved by the weight Chisala can put into the insistence of her worth and how deserving she is of love. This poem from near the end of the collection has stuck with me, I think about it often.

> There is a romance brewing here

> Between joy and I

> I deserve her and she deserves me

Give this collection a listen, it'll brighten your day.

### Disappearing Moon Cafe
#### Audiobook; Sky Lee; Grace Lynn Kung; Published: 2019; Read on: 2021-05-20

An interesting narrative of a family of Chinese women spanning from the 19th century wilderness in British Columbia, late 20th century Hong Kong, and Vancover's Chinatown. As if often the case with me and audiobooks, it was difficult at times to follow the intricacies of the narrative. Largely because it spans so many years and generations. Demarcations of time are very evident in text, less so in audio. This is the third or so novel written by an east asian author that I've read in a year that follows multiple generations. Now that I think about it I vaguely remember hearing about that being a comon theme in east asian literature. The place of one individual in the context of the generations of the family, the larger context of life.

### Obsessed
#### Audiobook; Allison Britz; Emily Ellet; Published: 2020; Read on: 2021-05-23

Whew. This story was an intense ride for me. I was gripped the entire time by Allison's fixation on what she had to do, on her full certainty of her irrational obsessions. I saw a lot of my own anxiety reflected in her. I don't have OCD, and I don't have obsessions quite like that. But I'm no stranger to a deep and certain conviction that I've done something wrong and there will be consequences for it. And the wide expansive despair that comes with that. This book was really helpful in seeing my own anxious thoughts for what they were, and to step back and disengage from them.

### Ninefox Gambit
#### Audiobook; Yoon Ha Lee; Emily Woo Zeller; Published: 2016; Read on: 2021-05-27

What a book! It's the first one of the Machineries of Empire series. I'm not typically one for military stories but damn is this an enthralling story. The single most enrapturing aspect of the storytelling is just how intensely delicate all the imagery feels. The description of weapons and battles is consistently beautiful, and conveys a sense of it all being incredibly fragile. The characters all get to be real people as well, who have hobbies in their down time and their own individual reasons for being soldiers.

In addition I'm always a sucker for interesting world building, and this is my favorite one in quite some time. The physics bending magical powers that armies wield is all powered by mathematical calculations based around a shared universal calendar. The army that we follow in this novel is chiefly occupied by putting down heretics who would veer away from the imperial calendar and thus tangibly disrupt the power of the empire.

If you like sci-fi, this book should definitely be on your list.

### Raven Strategem
#### Audiobook; Yoon Ha Lee; Emily Woo Zeller; Published: 2017; Read on: 2021-06-03

I was less gripped by the sequal in the Machineries of Empire series, but the twist at the end really got me. The novel was good, with equally good twists and turns of plot, and interestingly intricate world building. I immensely enjoyed how the various bureaucratic factions of the empire were fleshed out, led by and employing real people who had not only jobs but things they did when they weren't at work. They live real lives, and some of them give up so much of it for their jobs. I'm excited for the next installment.

### Leave the World Behind
#### Audiobook; Rumaan Alam; Marin Ireland; Published: 2020; Read on: 2021-06-10

A normal middle class family from the city books an AirBnB out in the country for a vacation. A chance to be away from it all in a place that is very away from it all. They arrive to find they don't have cell service, but the home does have cable and internet. All is well. Until it's not. The owners show up unannounced, asking to stay, offering to refund the money. They report a blackout in the city. This novel really feels like this is how the world would end from my point of view, living down a dirt road, away from too much but still close enough to easily obtain the conveniences of civilization. I really liked this novel; it's very good. But also I am at my limit for apocalyptic fiction. I do not wish to imagine what it would be like if the world ended in front of my eyes anymore. Content warning for some body horror stuff involving teeth near the end.

### Revenant Gun
#### Audiobook; Yoon Ha Lee; Emily Woo Zeller; Published: 2018; Read on: 2021-06-16

A great ending to this series. Yet again Lee manages to get me invested in all the characters, both old and new alike. Even though it's a story of war between empire and burgeoning democracy that was the least of my concerns. I want these people, from both sides, to be okay. With the introduction of a new sentient species in this novel I was reminded again of just how much I love Lee's worldbuilding here. I never felt like anything was just hand waved away with an explanation of "because science magic", and yet the entire thing revolves around mathematics and science being used to fuel reality bending magic! The ending was so incredibly bittersweet. I'm happy for Charis, and intensely sad for Jidao. I think I might give this series another listen sometime down the road.

## Currently Reading:

Cultivating Communities of Practice by Wenger, McDermott, and Snyder

Education for Critical Consciousness by Paulo Feire

The Last Necromancer by C.J. Archer

+++
title = "Redemptor"
author = ["Dorian Wood"]
date = 2022-04-04T17:05:00-04:00
lastmod = 2022-04-04T17:06:27-04:00
tags = ["book-review"]
draft = false
weight = 3001
+++

## Info {#info}

Author: Jordan Ifueko

Narrator: Joniece Abbott-Pratt

Published: 2022

Started on: 2022-03-18

Finished on: 2022-03-24

Link: [Redemptor](https://www.hoopladigital.com/title/14284400)


## Review {#review}

I was so excited for this book! It's the sequel to [Raybearer]({{< relref "raybearer.md" >}}), which was an amazing piece of world building and character development. The twist at the end of that novel is just really good.

I felt so invested in Tarisai's emotional struggles as she becomes an Empress. The imposter syndrome, the drive to do more simply because she can and should, at least in her own skewed perspective. I felt her emotions strongly at parts of this novel and it was a delightful journey!

Tarisai fell in love with so many people! It felt like a rush, and I'm not sure I remember all of them honestly. But each love was so beautiful! I'm still enamored with how well Ifueko portrays various loves that are all equally deep and intimate and yet still vastly different. What a journey of creating a family with one meets out of necessity. Tarisai must learn to love these people, and she truly does love them.

I was really excited when we got to the point of the journey through the underworld. I was so excited and confident in Tarisai and her abilities that I couldn't wait to see how she navigated these challenges and opponents. And it didn't disappoint! I don't want to spoil the drama at the end, because I do think it was beautifully done.

I will be watching for what Ifueko does next, because this series was amazing.

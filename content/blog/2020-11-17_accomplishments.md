+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-11-17"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Had therapy
1. Learned how to use some of ImageMagick for zine making
1. Finished a sock
1. Organized RSS feeds

Next time I would like to do better on:
1. Nothing today.

Tomorrow I would like to:
1. Layout a short zine on HTTrack
1. Start a new sock
1. Read some Madness & Civilisation

---
date: 2021-01-06T19:48:15-05:00
title: "Wednesday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Got the desktop set up with Arch! It took three reinstalls and three separate window managers (Gnome fudged up and I straight up did not bother to fix it at all).
2. Ran to the hardware store, then took the long way home.
3. Went for a walk/meditated.
4. Made supper.
5. Practiced the uke

Next time I would like to do better on:
1. Idk. Today was pretty good. Hopefully I sleep better tonight.

Tomorrow I would like to:
1. Set up VMs for the SysAdmin book/course
2. Start a new sock
3. Work on a shell script for parsing rss feeds (upon slight investigation seems shell is not a suitable tool for this).
4. Find a tool I want to use for parsing rss feeds.

---
date: 2021-03-30T20:34:25-04:00
title: "Tuesday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Gathered sticks with Grandpa for building a second huglekulture!
2. Chopped up the larger logs to an appropriate length.
3. Took Grandma to her dentist appointment.
4. Revamped my short codes for the audio player in my music pages.
5. Redid all the audio files for those pages as well so they are proper .ogg and .mp3 files instead of .flac.
6. Called my mom and did a Healthy Boundary Positive Statement with her about my Grandma. It went really well.

Next time I would like to do better on:
1. Being patient and loving with Grandma. There were several times today where I was annoyed at things that were...in my control and I simply should have taken care of them.

Tomorrow I would like to:
1. Have Therapy!
2. Start a course for IT support certification.

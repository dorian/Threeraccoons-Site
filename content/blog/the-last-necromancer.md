+++
title = "The Last Necromancer"
author = ["Dorian Wood"]
date = 2021-07-09T09:31:00-04:00
lastmod = 2021-12-19T20:47:27-05:00
tags = ["book-review"]
draft = false
weight = 3018
+++

Author: C.J. Archer

Narrator: Shiromi Arserio

Published: 2019

Read on: 2021-07-09

An interesting setting, and mostly interesting plot. I love the idea of having a conflict between a necromancer and Victor Frankenstein. I feel like the novel however did not live up to my expectations. Charlie was just too...feminine and helpless in a  stereotypically Victorian way for my taste. The first sign of hope for a change in Charlie's way of being came at the end of the novel, so this may change in the following books of the series. I don't think I'm going to find out though. Suffering through one novel of a character simply repeating the same exasperation at her own helplessness was enough for me, and I have too many other books on my to read list to find out if this changes in the next one.

+++
title = "Centenal Series"
author = ["Dorian Wood"]
date = 2021-10-02T09:27:00-04:00
lastmod = 2021-12-19T20:47:23-05:00
tags = ["book-review"]
draft = false
weight = 3009
+++

Author: Malka Older

Narrator: Christine Marshall

Published: 2016 - 2018

Titles:

Infomocracy; Read on 2021-09-18

Null States; Read on 2021-09-25

State Tectonics; Read on 2021-10-02

This series was really good! It takes place in a near future where much of the world has transitioned to a new form of democracy, and the power underpinning this new structure is a global bureaucracy controlling the entirety of the next generation of the internet. While that plot point is really fascinating and I really enjoyed the way it's explored through the series, it initially gave me a little trouble getting into the series. Not because it's bad but because it pretty accurately portrays how network surveillance works and I spend too much of my time not in fiction thinking about how terrible the internet is.

I am however absolutely enamored with the "Narrative Disorder" that features prominently in the character Mishima's arc. Older does an excellent job of constructing a neurodivergency, and of the world building necessary for that. Information, the world spanning bureaucracy managing all the world's data, obviously depends heavily on data analysis. In a world where so much of it functions on the statistical analysis of massive amounts of quantitative data, the natural human tendency to create stories to explain patterns we observe is stigmatized in those who express that trait strongly. The characters who have this trait in the novels genuinely struggle with it, but it also a deep source of strength for them. I think that's a pretty accurate portrayal of how neurodivergencies play out in real life as well.

This is a series where I think it's important to read all of them. The conclusions in State Tectonics seems to me an important one to consider for our times. I really appreciate how well Older avoids being preachy and prescriptive here. Largely because the shift of power dynamics in the novels is one moving towards what our system today looks like, but in a way that gives you an interesting perspective from which to judge both the positives and negatives of such a move.

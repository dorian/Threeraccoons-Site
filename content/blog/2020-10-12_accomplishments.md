+++
title = "Today's Accomplishments"
description = "I got some fun stuff done today!"
date = "2020-10-12"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Fixed my jitsi server
1. Fixed fonts on my arch machine
1. Updated my 2020 media consumption list with better data (author, date published, etc)
1. Had a short meditation session with a friend on jitsi

Next Time I would like to do better:
Be more assertive when caring for Grandma. I need to just make some more decisions, instead of asking her and then just expecting her to be on board with everything. That's not realistic.

Tomorrow I would like to:
1. Have therapy
1. Get audio off voice recorder
1. Create script for this to automate it further

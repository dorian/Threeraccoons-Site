---
date: 2021-01-14T20:12:46-05:00
title: "Thursday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Read "Flint's Emergent Latinx Capacity Building Journey During the Government-Induced Lead Crisis". Great read. Highly recommend. DOI: 10.1080/10705422.2017.1384422
2. Got my grandparents on the wait list for the COVID vaccine.
3. Found out wifi was borked on the Pi4.
4. Reflashed the Pi4 cause that was easier than fixing whatever was wrong with the wifi.
5. Knitted the heel turn and the gusset decrease on a sock. I'm only a month overdue on Christmas gifts but w/e it's hand made.

Next time I would like to do better on:
1. Being vulnerable
2. Being less reactive towards my feelings.

Tomorrow I would like to:
1. Honestly I need a freaking break. But I don't know how to take one. I'm in this house all day, and I will at some point find an urge to work on a project.
2. Oh maybe I'll learn emacs or bspwm. I've been putting those off because I feel it's not a productive project.

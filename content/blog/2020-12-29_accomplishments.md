---
date: 2020-12-29T20:01:06-05:00
title: "Tuesday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Edited and published my review of books read this year.
2. Gave Grandma a bath
3. Had therapy
4. Practiced Ukulele
5. Went for a walk

Next time I would like to do better on:
1. I'm not sure right now.

Tomorrow I would like to:
1. Get familiar with `diff` and  explore Lute's rss feeds compared to mine.
2. Finish my post about using old electronics to help reduce waste.
3. Clean the house with Grandpa.
4. Go for a walk.

+++
title = "New Site, New Look, Same Me"
description = "I've moved from using Wordpress to using Hugo."
date = "2020-09-07"
categories = ["blog"]
+++

I've redone the site. I kept most of the posts from my old wordpress site but I tossed several that just weren't interesting/relevant anymore. I quite like this new look. I was hesitant to use Hugo to generate my static site because I wanted to use minimalist software on the back end.
However I got this up and running in about an hour of work so that was way better than what I've been trying to cobble together on my own with vimwiki and some shell scripting honestly. Mostly it's how easy it is to use templates really, front end web design is not my jam really.
Anyways! I hope you like the site! There may be more modifications coming soon.

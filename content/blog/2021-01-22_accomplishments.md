---
date: 2021-01-22T19:51:28-05:00
title: "Friday's Accomplishments"
draft: false
categories:
- blog
---

So! I've missed a couple days. So this is a bit of a backlog.

Today I accomplished:
1. Practiced Ukulele (not for the past two days but I had a good streak before that)
2. Go for daily walk
3. Meditate daily
4. Watched a movie with Grandpa and Grandma
5. Worked on knitting a sock
6. Gave Grandma a bath

Next time I would like to do better on:
1. Going to be on time. I am really tired today, even with a late afternoon snooze.
2. Initiating conversations with Grandpa

Tomorrow I would like to:
1. Peak into running emacs daemon
2. Go for a walk
3. Practice ukulele
4. Work on SysAdmin book/course

+++
title = "Today's Accomplishments"
description = "Got some tasks done today that I've wanted to for awhile."
date = "2020-10-13"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Had therapy
2. Recorded items off from my dictaphone and cleared out it's memory
3. Dropped off my ballot
4. Went for a nice drive with Grandpa and Grandma
5. Wrote a script to do this!

Next time I would like to do better:
1. I did not take breaks today, those are really important for me. Next time I would like to take proper breaks.
2. Recognize that my frustration with Grandma at times like that has nothing to do with whatever I'm annoyed about and everything to do with the fact that I need a break.

Tomorrow I would like to accomplish:
1. Take Grandpa to his appointment
2. Improve the script for this (it failed half way through).
3. Knit a sock, I'd like to get to the heel flap at least.

+++
title = "Akata Witch Series"
author = ["Dorian Wood"]
date = 2021-09-12T09:27:00-04:00
lastmod = 2021-12-19T20:47:24-05:00
tags = ["book-review"]
draft = false
weight = 3010
+++

Author: Nnedi Okorafor

Narrator: Yetide Badaki

Published: 2018 & 2019

Titles:

Akata Witch; Read on 2021-09-06

Akata Warrior; Read on 2021-09-12

I really love everything I've read by Nnedi Okorafor! It's so hard to resist the urge to just binge everything she's published (maybe I have already, I haven't checked). Her characters and perspectives are just so fascinating! While I like the story of Binti better, I have been recommending these novels to lots of folks because I feel they accomplish many of the things that the Harry Potter series fell short in.

Sunny is an incredibly powerful and gifted child with magical powers she discovered in middle school, but her friends are also varied and interesting in their own rights. They are not just sidekicks that get used to flesh out Sunny's story (one of my big frustrations with that other magical kid series). At the same time, adults in this series do very much send children into life or death situations and expect them to save the world, but at least the adults in this story are honest about it to the children's faces. And instead of magical powers creating and reinforcing this class structure where everyone without is less than, the story deals seriously with the strain that having to keep magical secrets puts on relationships; while also highlighting the agency magical powers can provide to protect loved ones and strengthen those same relationships.

+++
title = "The Self Construct And Bringing Compassion To Ourselves"
description = "Examining how we think about ourselves to help us be more compassionate."
date = "2020-03-24"
categories = ["article"]
+++

## Self Construct

How I understand the self has been really instrumental for powerful and actionable introspection. We are beings of the present moment. As much as we can get sucked into worrying and obsessing about the past and the future we are still bound to the present. We can only act here and now, we can only feel here and now. Meditation has been so powerful for me because it is about being radically present here in this moment.

What I’m hoping to offer you here is a framework for executing a mindfulness routine in your daily life that can be truly transformative. A huge part of that for me has been how I relate to myself. This is heavily influenced by the Buddhist doctrine of no self, although I’m not advocating for that and I think this can work for anyone regardless of religious beliefs. There is a certain sense in which myself, what I think of when I think of “me” is distinct from this present ongoing consciousness.

Most of us already behave this way, even if we don’t consciously think it. You’ve probably worked to build some kind of habit at some point in your life. Every person I’ve ever heard talk about building habits has always emphasized routine, and just doing small things every day. We build up habits by taking small actions regularly over the course of time. Working out and building physical skills and ability takes time. It is not something we can decide to do and have it be done instantly. The part of us that has habits, good or bad, is something other than the part of us that experiences the present moment. The you that is reading this is different from the you that gets up every morning and does your morning routine.

For me what this demonstrates is that this is an aspect of self that is distinct from this present conscious experience. Meditation has been really powerful for me largely because it has helped me build a deeply compassionate relationship between those two aspects of myself.

What follows are two phrases that I have found incredibly helpful in developing a deep compassion for myself, which has empowered me to see myself in new lights and take actions for positive self development.

## Phrases for Building Self Compassion

### “This is what it feels like to be…”

The first phrase that has really helped me build self compassion is what I like to refer to as, “Declaring the emotion.” It really helps the most if you physically say it out loud, just declare to yourself and the world that, “This is what it feels like to be sad.” Or, “This is what it feels like to be bored.” Identify your emotion, give it a name, and then declare it. As you declare, check in with your body. What does it feel like to be sad? Is there tightness in your chest? What does it feel like to be bored? How does your body move and change itself when you are anxious?

We are embodied beings. It’s really fun to read cyberpunk scifi where hackers get to hook their brains into the internet and hack unbelievably intricate computer systems, but that is not the world we live in. You and I are bound to these bodies, for better and for worse. And the more I practice meditation the more I am convinced that is not a bad thing. This body is a fantastically intricate tool at your disposal, and I encourage you to use it. By bringing our attention to the physical sensations of emotions we break their cyclical self absorbed nature and give ourselves access to the present moment.

For me, feelings are frequently overwhelming and disabling. Both positive and negative feelings. And the one tool that has been the most effective for helping me deal with those overwhelming feelings is meditation and it’s emphasis on the present moment. When we ground ourselves in the present moments we no longer get caught up in intense emotions. We are able to see things more as they are, and not as our judgements of them. From that perspective we have much more freedom to address the world as it is in order to achieve the outcome we really want.

### “That’s interesting.”

The second phrase I have for you to give a try today is much shorter, but it works in much the same way as the last one. This one I find more useful to apply to situations rather than feelings. As you notice yourself feeling intense emotions or reacting reflexively to events take a moment to step back, and say to yourself, “That’s interesting.”

### “That’s interesting that I get angry at my grandmother’s reluctance to ask me for help.”

Emotions and reactions to the world around us don’t come from nowhere. Over the course of our lives we build up beliefs about the world and other people. We see events happen and people do things, and that activates our beliefs, which in turn tells us how to feel. I believe helping other people makes my life meaningful. I believe that if people don’t want my help it is because they don’t like me or think I am bad. So when my grandmother is reluctant to ask for or accept my help that activates my beliefs, and I get angry because I feel like I am failing at my duty as her caretaker. One of those beliefs is helpful, the other one is a toxic belief that I’ve built because of various experiences in my childhood. Can you tell which one? After I dug deep and pulled out those beliefs it’s pretty obvious! But until I had access to the present moment and a gentle non judgemental curiosity they were inaccessible to me. I was stuck simply experiencing the event, and then jumping to my reaction and the negative feelings that generated.

## Bring Kindness, Gentleness, and Affection to Yourself

These two phrases are part of the foundation of how I have managed to love myself. This topic really deserves another blog post of it’s own, but for now I want to bring it up as a goal of sorts.

You are doing such a great job. I have written all this out so that you can have effective tools to bring awareness to yourself, and with that an awareness that you are worthwhile, you are valuable, and you are enough just as you.

May you be safe from inner and outer harm. May you have joy. May you have peace. You are doing powerful work. <3

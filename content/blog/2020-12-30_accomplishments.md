---
date: 2020-12-30T20:12:46-05:00
title: "Wednesday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Researched some graduate programs
2. merged some of Lute's rss urls into mine
3. Went for a walk
4. knitted some on a sock

Next time I would like to do better on:
1. engaging Grandpa with a project
2. picking a cleaning project to work on

Tomorrow I would like to:
1. clean the center island
2. read some Madness and Civilization
3. make fudge
4. make apple cake dessert

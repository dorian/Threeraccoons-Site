---
date: 2021-01-26T20:01:33-05:00
title: "Tuesday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Wrote two articles for the site, one on Groff and one on the Linksys WRT54G
2. Had a HBPS conversation with Grandpa. Things are looking positive there.
3. Had therapy.
4. Make good progress on a sock I'm knitting.

Next time I would like to do better on:
1. I don't know. Today was a good day.

Tomorrow I would like to:
1. Go for a walk
2. Make progress on SysAdmin course
3. Get tags attached to socks to send out for gifts

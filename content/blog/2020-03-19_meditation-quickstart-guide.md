+++
title = "Meditation Quickstart Guide"
description = "How to meditate 101"
date = "2020-03-19"
categories = ["article"]
+++


So you’ve decided to explore meditation! Awesome! Here are some things that I’ve learned over the years that I’ve found really helpful for practicing simple and effective meditation.

## There’s no way to do it wrong.

Meditation is a practice, not an event. If you find some particular way of meditating is difficult, there’s two things to take from that.

First I have to say, congratulations! You’ve been mindful of how your meditation practice is going! The key of meditation that makes it such a useful tool for growth and change is that it allows us to notice things in our own mind.

Secondly, meditation is not a fixed tradition that you have to practice in some particular way. If you notice that it is difficult then you have the opportunity to adjust your practice and try new things that might be less difficult. Meditation is a single word that covers a huge variety of contemplative practices, don’t be afraid to look up and try new ways to meditate. Feel free to find the kind of practice that best suits you.

## There are two main components of meditation: Mindfulness and Concentration.

Mindfulness is simply being mindful and noticing. Many of us go through most of our lives on autopilot. Life requires us to do so many things so often that it is much easier mentally to do them on autopilot. This is not bad. Such cognitive shortcuts allow us to do everyday tasks with less mental effort. But also it is not good. By practicing mindfulness we can begin to acknowledge those things that happen automatically in our mind, and once we notice them, we are able to see whether they help or hinder us in achieving our goals. Either way, we can’t address things unless we first notice them.

Concentration is just our ability to focus. Breath meditation and Transcendental meditaion are probably the most commonly known methods in the United States, both of these feature concentration as the main tool of the practice. Concentration can be very useful for grounding us in the present moment. Being grounded in the present moment instead of fixated on the past or future is key to being able to be mindful, and therefore able to address whatever emotions, anxieties, or distress that we experience in our lives.

## How these things look in practice.

One of the things I hear quite often from people beginning their meditation practice is frustration over stray thoughts coming into the mind and being distracting. I encourage you to shift your perspective from this being a bad, and therefore frustrating thing, to congratulating yourself. You noticed that a thought came into your head when you did not want it to! That noticing is you being successful at meditation!

Those stray distracting thoughts aren’t actually something I want to happen during my meditation practices however, so I do seek to minimize them. The best first step towards that is to work to remove the negative reaction you have towards them. Stray thoughts are not you being bad at meditation, they are simply stray thoughts. Once you notice and recognize stray thoughts for what they are, then you can implement strategies to minimize them. An incredibly useful strategy for me has been to acknowledge the stray thought, and then set it aside in a value neutral way. I do this by thinking, “Okay thought, but not right now. Maybe later.” And as I think that I visualize myself setting the thought aside.

Concentration, or the ability to focus without stray thoughts entering our mind, is a skill we can develop and grow. There are various ways and tools we can use towards this end. Transendental meditation uses a phrase that you repeat in order to push stray thoughts out of your mind (I personally have never practiced this method). Breath meditation, which I practice, uses the breath as an ever present feature of the present moment towards which you can direct your attention. One strategy I used for a little while was counting the number of breaths I could go before a stray thought entered my mind. This was very useful for measuring my concentration, to set a goal for improving it, and to see if I was indeed improving. I soon found however that the striving to go a particular number of breaths without stray thoughts led me to be stuck in the future, fixated on making sure I was pushing out stray thoughts, instead of being grounded in the present moment and actually focused on my breath. I have since adapted my practice. Now as I count my breaths, I count to one and then start over. One breath in, one breath out. This serves my goal of being grounded in the present moment during meditation much better.

If you find focusing on your breath uncomfortable, feel free to adjust! Ideally, whatever you focus on should be continually and always present to you. It also helps if that thing is somehow cyclical in nature. Breathing is a popular choice because it changes frequently, and yet is highly repetitive.

## So how do I start?

Starting meditation is super easy! Grab a timer, seriously any timer, and set it for five minutes. As you continue this practice you might find it useful to explore various websites and phone apps that provide timers, but the most important thing is just to start, use whatever timer is immediately available (The only app recommendation I have: I use the Meditation Helper+ app on the Android store currently). Longer isn’t necessarily better with meditation. I’ve been meditating for about 10 years and I only meditate for 10 minutes at a time. Start small, exercise your mental muscles of concentration and mindfulness, and adjust and explore as you grow.

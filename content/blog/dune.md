+++
title = "Dune"
author = ["Dorian Wood"]
date = 2021-10-18T09:29:00-04:00
lastmod = 2021-12-19T20:47:26-05:00
tags = ["book-review"]
draft = false
weight = 3016
+++

Author: Frank Herbert

Narrators: Scott Brick, Orlagh Cassidy, Euan Morton, Simon Vance, et al

Published: 1965

Read on: 2021-08-01

Just felt like rereading this classic. It took me a bit to get into, and even then I managed that mostly just because I wanted to get to the fun parts that I remembered haha. Looking back on it honestly...this is a pretty okay book, but it's just not something I'm ever going to recommend to anyone.

---
title: "Emacs Profiler"
date: 2021-08-30T06:36:49-04:00
draft: false
---
More Emacs learnin!

Since I redid my Doom Emacs config last week with org mode and literate programming stuff I've been struggling with some significant lag in Emacs.
It happen particularly with a large org mode file I was using to take some notes in.
I would type and it would take a second or two for the words to show up.
Moving around in the file and opening trees would sometimes take several seconds.
It was bearable, but it was really annoying.

A few quick internet searches brought up the emacs profiler tool, started with `M-x profiler-start`, or `SPC h T` if you are using Doom Emacs.

Since I'm using Doom Emacs I went with that option; which toggles the cpu+mem profiler on, and then when toggled off brings up the report. It was a little hard to decipher at first largely because it opens up two windows, one for the cpu profiling and one for the mem profiling, but neither have clear labels. My other difficulty was that it split my screen vertically to display the windows, but I do not have enough horizontal space to display both. Rather than shrink the width of the text it simply cuts off the important information.

Once I solved the ordeal of figuring out what I was looking at my problem was easily solved! It was the `doom-modeline-segment--word-count` function that was sucking up massive amounts of cpu cycles. That was a feature that I enabled from seeing it in Diego's emacs configuration that I discussed in the last post. Makes sense because this issue popped up sometime after I redid all that last week. Turning that setting off has solved my laggy input issues.

I'm a little sad because that was a really nice feature. I particularly liked the ability to see the word count of a selection without having to call `count-words`. I might look into adding a leader key map to toggle that feature, so I can have it when it'd be useful but not otherwise.

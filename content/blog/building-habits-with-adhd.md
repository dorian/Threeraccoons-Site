---
title: "Bulding Habits With Adhd"
date: 2021-05-22T16:46:31-04:00
draft: true
categories:
- article
---

During the pandemic I've had a lot of time to work on establishing various habits, so I'm writing down my thoughts about what goes into effective habit formation in order to hopefully organize my efforts a little better.

## The Struggle: I'm bored.
Lots of advice that's I've seen over the years identifies 14 to 21 days as the time that it takes to develop a habit. This gets quoted just before advice to just stick with it for the short while and then you'll have the habit developed and it'll be so much easier. I personally have never found that to be the case. In fact it's often the exact opposite. Setting out to develop a new habit is pretty exciting! The first 14 to 21 days I find it pretty easy to engage in a new activity. It's new! The novelty is inherently interesting, and it also comes with the exciting plans of building a new habit. After a few weeks that novelty has drained away. Now a task is a chore that I have to summon up extra energy to push myself to engage in. It's often around three weeks in that I start wanting to take a day off, just to have a little break, hoping that the next day I'll have renewed vigor for the task I want to build into a habit.

## My Tactics
I've got a few working strategies that I've been using to combat this.

### Task Initiation is the Goal
An important tool that I've been using is to set the bar for success really, really low. When it's really difficult to push myself to do an activity, it's very helpful to make my goal simply to start it.

### Focus on the Feeling at the End
Most of the habits that I've been trying to established have been linked with self care in some way, so focusing on their positive effects that make them desirable in the first place is helpful. It's helpful to motivate myself to do yoga to focus on it as a self care project. Noticing the ways in which my body does not currently feel good, which yoga would help, and to think about how in the past I know the tasks I have planned after yoga are much more enjoyable and easy because of it.

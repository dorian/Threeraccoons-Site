+++
title = "Fearless"
author = ["Dorian Wood"]
date = 2022-04-28T13:15:00-04:00
lastmod = 2022-04-28T13:15:38-04:00
tags = ["book-review"]
draft = false
weight = 3001
+++

## Info {#info}

Author: Shira Glassman

Narrator: Grace Keller Scotch

Published: 2021

Started on: 2022-04-27

Finished on: 2022-04-27

Link: [Fearless](https://www.hoopladigital.com/title/14328479)


## Review {#review}

What a delightful short story! It's only 1 hour long, a great afternoon or evening listen. It's incredibly wholesome too! We follow a freshly divorced lesbian in her 40's as she tries to get a handle on being out, a parent, and single in a dating pool she has zero experience in. The story takes place at her daughter's orchestra conference, where there's an instructor who happens to be an incredibly hot butch.

Give it a listen!

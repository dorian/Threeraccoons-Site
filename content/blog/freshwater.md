+++
title = "Freshwater"
author = ["Dorian Wood"]
date = 2022-05-21T17:16:00-04:00
lastmod = 2022-05-21T17:16:50-04:00
tags = ["book-review"]
draft = false
weight = 3002
+++

## Info {#info}

Author: Akwaeke Emezi

Narrator: Akwaeke Emezi

Published: 2018

Started on: 2022-05-04

Finished on: 2022-05-11

Link: [Freshwater](https://www.hoopladigital.com/title/12047232)


## Review {#review}

This book is full of lots of stuff that I really wished was warned for when I started the book. It's all very good, but there is also lots of trauma events that the viewpoint character endures. There is detailed descriptions of rape, eating disorders, and self harm via cutting in this book. Many spoilers to follow.

Ada, our viewpoint character, had something go amiss during birth. The spiritual doors that are normally closed to keep the entities that make up the soul from leaving were left unbound. The spirits thus never cohered into a single self, and Ada has to journey through life while co-inhabiting her body with several others who remember existing before Ada's birth. They turn out to be old African gods, and they a quite contentious towards Jesus, the god that Ada worships.

Exploring their relationship is utterly fascinating. We follow Ada through her life, but it isn't just hers, it is also that of the being she shares the body with. Often times they are not kind to the body, since they do not recognize it as theirs, and as deathless being not accustomed to being bound to a single body, they do not care much for it's comfort or needs.

The narrative is a wonderful exploration of dissociative identities, although Ada is expressly not that in the novel. DID is acknowledged as something that from the outside appears to fit Ada, but the old gods refuse that label. Perhaps that is true for them. Perhaps is it part of a delusion. It is a fascinating journey either way.

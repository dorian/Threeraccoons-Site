+++
date = 2020-12-27T20:59:03-05:00
title = "Sunday's Accomplishments"
draft = false
categories = ["blog"]
+++

I forgot to fill this out yesterday so this is for the past two days.

Today I accomplished:
1. Gave Grandma a bath
2. Tinkered with my site (I did this both yesterday and today. I modified the css of some buttons on my site, and I think I'd like to redo my site navigation to something a little more elegant. Now that I've got a good base this is really fun to tinker with and adjust slightly from time to time!)
3. Made gilled chesse sandwiches and cream of mushroom soup for lunch, yum. Had Grandpa help me with the grilled cheese.
4. Knitted more on a sock.
5. Had video chat with my family
6. Navigated a really difficult situation with Grandma and dementia

Next time I would like to do better on:
1. Ask more exploratory questions with Grandma when we have a difficult dementia moment instead of just trying to convince her that everything is okay.
2. Ask for help when I need it with less hesitation.

Tomorrow I would like to:
1. Clean the kitchen with Grandpa
2. Redo website naviation
3. Set up a way to post to mastodon from the rss feed for this site, so that I can auto share my posts.
4. Edit and perhaps finalize my post reviewing all the books I've read this year.

+++
title = "The Unlikely Adventures Of The Shergill Sisters"
author = ["Dorian Wood"]
date = 2022-07-11T16:39:00-04:00
lastmod = 2022-07-11T16:39:44-04:00
tags = ["book-review"]
draft = false
weight = 3002
+++

## Info {#info}

Author: Balli Kaur Jaswal

Narrator: Sonella Nankani, Soneela Nankani, Deepti Gupta

Published: 2019

Started on: 2022-05-24

Finished on: 2022-06-05

Link: [The Unlikely Adventures of the Shergill Sisters](https://www.hoopladigital.com/title/12195183)


## Review {#review}

I find the tension between the generations of sisters really fascinating. Perhaps because there is a similar age gap between my older sisters and I. So I know what it is like to have older siblings around who occupy that liminal space between sibling and parent. The dynamics between the sisters is truly griping. Their mother demands that they make a trip to India in order to better understand their culture. But the greatest thing they learn on the trip is not ancient wisdom from their heritage, but rather an understanding of each other and the ways in which their experiences with their mother was different, but also similar. This allows them to be present for and support each other in ways they were previously incapable of.

---
date: 2021-01-03T11:35:57-05:00
title: "Sunday's Accomplishments"
draft: false
categories:
- blog
---
Again this is for the past several days.

Today I accomplished:
1. Site navigation has been tweaked! I moved the nav bar, and it now has an "active" setting that highlights the nav link for the section of the site you are in.
2. Redid my content lists, They now have a max width and tile horizontally as screen space allows.
3. Brought cake and fudge to our neighbor for plowing our drive.
4. Started reading a Linux SysAdmin book, great fun!
5. Read some Madness & Civilization!

Next time I would like to do better on:
1. I've really done a great job the past few days at realizing when I'm stuck in some emotional reasoning cognitive distortions. I want to continue improving at letting them go when I realize it in the future.

Tomorrow I would like to:
1. Read! Reading nonfiction makes me so happy.
2. Clean the center island (this has been a todo item for a week now haha).

+++
title = "The Nature of Oaks"
author = ["Dorian Wood"]
date = 2021-12-28T09:44:00-05:00
lastmod = 2021-12-28T09:45:32-05:00
tags = ["book-review"]
draft = false
weight = 3001
+++

## Info {#info}

Author: Douglas W. Tallamy

Published: 2021

Read on: 2021-12-26


## Review {#review}

This was such a fun read! Pretty quick too, due to the thick pages. Tallamy goes through the year month by month, describing the various life that would exist on and around an oak tree during that period of time. My initial thoughts about examining the life of an oak tree would have been thinking about the tree itself over it's lifespan. What kinds of things does the tree do when it is young, mature, and old. Now those details are included to some extent, but the focus really is all the kinds of life that depend on an oak through a single year.

Tallamy is an entomologist by profession, and this approach makes a lot more sense when I learned that. The book is largely about the various insects that depend upon an oak tree (with examples of the insects that depend on his specific oak tree from his yard!). I did not realize that so many species of animals have evolved to rely specifically and only on oak trees. Tallamy frequently touches upon how simply planting trees is not an adequate response to help the environment. They must be the right kind of trees, particularly native trees that support other native life. I am exceptionally fond of this more holistic view of environmental action. We need to structure our built environment in ways that promote and protect diversity of life.

I was surprised to learn that song birds depend largely upon a diet of caterpillars during the winter months. I was not aware that so many caterpillars winter on trees like oaks. I guess I had just assumed that eggs would be laid and then hatch in the spring. This makes me feel a lot better about not wanting to spend the money to keep my bird feeder full of seed all winter long!

I have a big old oak in my yard, and I'm really excited to take this new knowledge and be more aware of the life that goes on around it during the year! I'm also considering taking an acorn from it and planting it in an open section of my yard that could use more tree cover.

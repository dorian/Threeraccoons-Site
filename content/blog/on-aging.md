+++
title = "On Aging"
author = ["Dorian Wood"]
date = 2022-01-22T14:52:00-05:00
lastmod = 2022-01-22T14:52:09-05:00
tags = ["reflection"]
draft = false
weight = 3001
+++

I'm just throwing out some thoughts I've been mulling over about aging. I am a caretaker for my grandparents, primarily my Grandmother, who has Alzheimer's, so this is something that I ponder fairly often.

People have good things to offer others, even in their old age. My grandparents contribute so much to my life, it simply wouldn't be the same without them. There's the obvious things, the time we get to spend together playing cards, sharing stories from our lives, meal times together. Those things are positives in my life that they contribute.

Perhaps less obvious is how them needing my help is a benefit for me. It has required me to grow as a person, to acquire the skills and attitudes I need to care for them. I have become a better and more well rounded person than I would have without caring for them. Giving help also transforms and helps the giver. Needing help as we age is not a bad thing.

You cannot know what your life will be like decades in the future. It is impossible to know all the ways in which you might have a positive impact on others. If you declare now that future you will have nothing to live for, what will happen when you finally get there?

Do not plan a lack of joy.

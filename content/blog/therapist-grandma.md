+++
title = "Grandma as Therapist"
author = ["Dorian Wood"]
date = 2022-04-09T19:05:00-04:00
lastmod = 2022-04-09T19:05:43-04:00
tags = ["reflection", "therapy"]
draft = false
weight = 3002
+++

My partner and I got to talking about therapy theory the other night. They brought up Carl Rodgers and Person Centered Therapy (PCT). The three core concepts of PCT are unconditional positive regard, congruence (authenticity of the therapist), and accurate empathetic understanding. This approach really lines up well with both my partner and I, we both learn towards this kind of humanistic approach to things.

As we were talking about this it occurred to me that Grandma also possesses these qualities. My grandfather and I are the primary caretakers for my grandmother, she has dementia and so needs support to complete many daily tasks. I've been doing this for about five years now, and it has become incredibly important to me that I make sure to highlight and emphasize the healing powers that Grandma has available to her. Just because she has significant dementia does not mean that she only needs support and never contributes to our family system.

Grandma is excellent at unconditional positive regard. She always thinks the best of everyone, and even when people are short or rude with her she has a smile for them. Everywhere we go Grandma makes friends because she is quick to tell people they are doing great and that she is happy to see them. Every nurse and receptionist we interact with at medical appointments remarks how much they love working with Grandma because she's so kind and encouraging.

Grandma cannot help but be congruent either. Whenever you have a conversation with her she can only be who she is. She simply does not have the cognitive capacity to do otherwise. What you see with my Grandmother is what you get.

Accurate empathetic understanding might be difficult for Grandma at times. She might not be able to grasp all the reasons why I'm upset, but she has shown me time and time again that she is keenly able to see and understand the emotions I'm feeling. She is aware of me as a person and is willing to see and participate in whatever emotions I'm having at the time.

Grandma has been a huge help to me, particularly during the pandemic when things have been incredibly tough and I lost a lot of my social supports during quarantine. Many times I have gone to Grandma for comfort and support when I was feeling overwhelmed and she was able to bring all of these qualities to the moment at hand and do a great deal to help me soothe difficult emotions. All human beings have dignity, and all human beings have the capacity to heal those around them. I'm so thankful I have had the opportunity to journey with my grandmother in this.

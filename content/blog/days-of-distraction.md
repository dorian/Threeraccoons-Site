+++
title = "Days of Distraction"
author = ["Dorian Wood"]
date = 2021-09-01T09:28:00-04:00
lastmod = 2021-12-19T20:47:25-05:00
tags = ["book-review"]
draft = false
weight = 3012
+++

Author: Alexandra Chang

Narrator: Greta Jung

Published: 2020

Read on: 2021-09-01

What a journey! It follows the post college coming of age of the narrator as she tries to make her way in the world, get a raise, follow her boyfriend across the entire United States, and figure out what the hell she's even doing it all for. The prose was so tender, poignant with the struggles of young adults in the information age. Yet it still finds moments to be humorous and heavy, complete with the temptation to cheat and the burden of placating parents. It's not my usual sci-fi/fantasy brand of novel, but I quite liked it.

+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-11-29"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Lots of work learning Hugo and developing a new website theme. It's almost in a place that I'm happy saying it's "complete" (i.e. good enough for the time).
1. Practiced Uke
1. Got groceries
1. Did video chat with family
2. Finished a sock! Started a new one!

Next time I would like to do better on:
1. Continue reframing my frustrations with Grandma's responses to understand her perspective on the matter.
1. Come up with a priority for todo lists. Something to help me focus on more important items.

Tomorrow I would like to:
1. Flesh out SMTP server checklist
1. Set up Mastodon server
1. Knit

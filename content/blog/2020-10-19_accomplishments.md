+++
title = "Today's Accomplishments"
description = "Getting Stuff Done!"
date = "2020-10-19"
draft = "False"
categories = ["blog"]
+++

Today I accomplished:
1. Led Together Meditation
1. Took Grandma to a doctor appointment
1. Complete the Accessibility subsection on freecodecamp.org
1. Finished a sock and started a new one!

Next time I would like to do better on:
1. Talking to Grandpa

Tomorrow I would like to:
1. Therapy
1. Either another subsection of the course at freecodecamp or revamp this site (it really needs it)
1. Finish the heel flap for this sock possibly.

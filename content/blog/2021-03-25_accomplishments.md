---
date: 2021-03-25T20:49:09-04:00
title: "Thursday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Published more recording of Grandma and I playing music!
2. Started transfering Grandma's recipe book to text files. Eventually I want to publish them online, as well as get a printable format for my family members.
3. Worked!
4. Continued on with more Stats videos.

Next time I would like to do better on:
1. I'd like to push past my anxiety about bringing my uke to schools and playing it with students.
2. Today was a little difficult with some interactions with Grandma.

Tomorrow I would like to:
1. Take care of some medical paperwork.
2. Do some minor car upkeep tasks.
3. Continue with stats videos.

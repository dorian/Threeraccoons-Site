+++
title = "Gender Doily"
description = "Trans Day Of Visibility Art Showcase 2019"
date = "2019-03-28"
categories = ["article"]
+++

# Gender Doily: Trans Day Of Visibility Art Showcase 2019

My local pride center hosted an art showcase to kick off their week of events preceding this year’s Trans Day of Visibility. I had a piece on display in it! This is the first time I’ve every shown a piece of art that I made, and I’m very thrilled to have this opportunity.

![A crocheted doily starting with white in the center, then blue, pink, white, and purple in a solid double crochet stitch. It then has a yellow lace edging, after which the crochet veers off a doily pattern, including some small yellow crochet flowers that hang over the edge of the display. A purple strand is crocheted across the surface of the doily, turning into a vertical piece of purple, pink, white, blue, yellow and black.](../media/images/gender-doily.jpg)

Gender Doily

This piece is a metaphor of my journey with gender during my life. The colors represent a particular kind of gender, as well as what time of my life it occurred. I was raised as a boy, but as I grew found that fitting me poorly. I tried to be a girl briefly in college, but that didn’t fit either. Being neither boy nor girl was increasingly attractive. I don’t fit the binary, and that means far more than just existing between pink and blue. There are more than just those two colors, and there are more ways to be than to follow a pattern.

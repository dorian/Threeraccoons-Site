+++
title = "Setting up a Matrix server"
description = "A detailed guide on setting up matrix-synapse on a Debian 10 server."
date = "2021-04-23"
draft = false
categories = ["article"]
+++

## Introduction
[Matrix](https://matrix.org/) is an open standard for decentralized, real-time communication.
[Matrix-Synapse](https://github.com/matrix-org/synapse/) is an server implementation of the matrix protocol written in Python.

This tutorial is about setting up your own Synapse server. You will set up a modern rich text communication platform for yourself, your organization, or community that you can use by itself, or connect with the larger matrix server federations. You will configure _Debian 10_ with _Nginx_ and _LetsEncrypt_ to direct traffic to your _Synapse_ server. When you're finished, you will be able to invite friends or coworkers to register an account on your server, and communicate privately and securely.

## Prerequisites

Before you begin this guide you'll need the following:
* One Debian 10 server with at least 1 Gig of ram. If you are planning on more than a dozen users start at 2 Gig.
* A non-root user with sudo privileges. The [Initial Server Setup at Digital Ocean](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-debian-10) explains how to set this up.
* Nginx installed on your server, as shown in [How to install Nginx on Debian 10](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-debian-10)
* A domain name configured to point to your server.


## Step 1 - Setting Up DNS For Subdomains
We will make use of `YOURDOMAINHERE.COM` and `matrix.YOURDOMAINHERE.COM` You will need to set those two DNS records before continuing.

## Step 2 - Setting Up Reverse Proxy With Nginx
You will set up two reverse proxies with Nginx, one for your base domain name, and one for the subdomain pointing to the matrix server.

First open the file for you base domain name in the Nginx sites-available directory.
```
sudo nano /etc/nginx/sites-available/YOURDOMAINHERE.COM
```
And paste in this configuration, being sure to replace the YOUDOMAINHERE.COM field with your domain name and the root field with the location of your site directory.

```/etc/nginx/sites-available/YOURDOMAINHERE.COM
server {
	listen 80;
	listen [::]:80;

	server_name YOURDOMAINHERE.COM;

	root /var/www/YOURDOMAINHERE.COM;
	index index.html;

	location / {
		try_files $uri $uri/ =404;
	}
}
```

Next do the same for the matrix subdomain you created earlier with you DNS records.

```
sudo nano /etc/nginx/sites-available/matrix.YOURDOMAINHERE.COM
```

And paste in this configuration. The location field in this one tells Nginx that when requests come here using the subdomain matrix.YOURDOMAINHERE.COM to redirect those requests to port 8008 on the same machine, the default port that matrix-synapse listens on.

```/etc/nginx/sites-available/matrix.YOURDOMAINHERE.COM
server {
	listen 80;
	listen [::]:80;

	server_name YOURDOMAINHERE.COM;

	location / {
		proxy_pass http://localhost:8008;
	}
}
```

Link both of those to files in `/etc/nginx/sites-enabled` directory to tell nginx to serve those web pages when requested.

```
sudo ln -s /etc/nginx/sites-available/YOURDOMAINHERE.COM /etc/nginx/sites-enabled/YOURDOMAINHERE.COM
sudo ln -s /etc/nginx/sites-available/matrix.YOURDOMAINHERE.COM /etc/nginx/sites-enabled/YOURDOMAINHERE.COM
```

## Step 3 - Setting Up TLS With LetsEncrypt
You will set up certificates with _Certbot_ to make communication on your matrix-synapse server secure.

First install the _Certbot_ plugin for _Nginx_. _Apt_ will install all the dependencies and _Certbot_ itself as part of this.

```
sudo apt install python3-cerbot-nginx
```

Next run certbot for your base domain name and for the matrix subdomain you set up. The --nginx flag says we want this to work with nginx, and the -d flag is used for specifying what domain to get a certificate for.

```
sudo certbot --nginx -d YOUDOMAINHERE.COM -d matrix.YOURDOMAINHERE.COM
```

You will need to enter your email address for contact purposes in case there is ever trouble with your certificate.

## Step 4 - Installing Matrix-Synapse
This guide will be using packages for Debian 10 provided by Matrix.org. The packages in the Debian 10 repositories require dependencies newer than those currently packaged by Debian.

In order to do that, you will need to use wget to download the gpg key from matrix.org, and then tell your server to trust packages from that souce that match the key.

First install wget to pull the key from matrix.org

```
sudo apt install wget
```

Then use wget to pull the key and put it in /usr/share/keyrings.

```
sudo wget -O /usr/share/keyrings/matrix-org-archive-keyring.gpg https://packages.matrix.org/debian/matrix-org-archive-keyring.gpg
```

Next add a file in /etc/atp/sources.list.d to inform apt of this new key.

```
sudo nano /etc/apt/sources.list.d/matrix-org.list
```

And paste in this line:

```
deb [signed-by=/usr/share/keyrings/matrix-org-archive-keyring.gpg] https://packages.matrix.org/debian/ buster main
```

Finally update apt and install matrix-synapse with apt

```
sudo apt update && sudo apt install matrix-synapse-py3
```

## Step 5 - Enabling Registration
By default new registrations on the matrix-synapse server are disabled. If you want someone to be able to register an account on your server you will have to enable registration.
You will be editing homeserver.yaml, the configuration file for matrix-synapse. There are many options, but you only need to change one. The homeserver.yaml file is well documented, and it is a good idea to read through the whole file at some point, as it will let you know what matrix-synapse is capable of and inform you of features.
First, open homeserver.yaml with nano

```
sudo nano /etc/matrix-synapse/homeserver.yaml
```

Then press `CTRL+W` to bring up the search function, type `enable_registration:` and hit `ENTER`. Delete the `#` to uncomment the line and change it from the default of `false` to `true`.

Restart matrix-synapse for the new settings to take effect.

```
sudo systemctl restart matrix-synapse.service
```

### At this point you have a functional Matrix server. The next few steps aren't strictly necessary, but they set up your server to be suitable for production use.

## Step 6 - Set Up Federation

In order to let the rest of Matrix know how to find your server, you need to publish a file at `https://YOURDOMAINHERE.COM/.well-known-matrix/server`.
First create the directory in the root of your website. The `-p` flag tells `mkdir` to make parent directories as necessary.

```
sudo mkdir -p /var/www/YOURDOMAINHERE.COM/.well-known/matrix
```

Then open a file in that directory called `server`,

```
sudo nano /var/www/YOURDOMAINHERE.COM/.well-known/matrix/server
```

And paste this line and save:

```
{ "m.server": "matrix.YOURDOMAINHERE.COM:443" }
```

## Step 7 - Migrate The Synapse Server To A Postgres Database
By default matrix-synapse uses *SQLite*. This is fine for testing purposes and very small servers, but any server in production use should be switched over to use a *postgreSQL* database.

First you need to install *postgreSQL*

```
sudo apt install postgresql
```

Next you will switch over to the *postgres* user to create the database and users for it.

```
sudo -u postgres bash
```

You are now logged in as the postgres user on the Debian 10 server, next you will create a user for *postgresSQL*. This is a user managed by *postgresSQL*, not by the GNU/Linux system.

```
createuser synapse
```

In order to create the database you will open a terminal based front end for *PostgreSQL*. Open the *PostgreSQL* interactive terminal with this command.

```
psql
```

Now you will enter a command to create a databse. The commands will not be executed until you end a line with ;.

```
CREATE DATABASE synapse
	ENCODING 'UTF8'
	LC_COLLATE='C'
	LC_CTYPE='C'
	template=template0
	OWNER synapse_user;
```

Then type `exit` to get out of the *postgreSQL* interactive ternimal, and `exit` again to log out of the postgres user.

Next you need to enable the syanpse postgreSQL user to access databases by editing `/etc/postgresql/11/main/pg_hba.conf`. The order of entries in this configuration file matters, so you will need to add

```/etc/postgresql/11/main/pg_hba.conf
host	synapse		synapse	::1/128		ident
```

ABOVE

```/etc/postgresql/11/main/pg_hba.conf
host	all		all		::1/128		ident
```

Lastly you will tell matrix-synapse about the new database. You will edit the homeserver.yaml file again to do this.

```
sudo nano /etc/matrix-synapse/homeserver.yaml
```

Again press `CTRL+W` to open the search function and search for `database:` and press `ENTER`. Scroll down below the comments and remove all entries associated with `sqlite3`. Then add in these lines.

```/etc/matrix-synapse/homeserver.yaml
database:
  name: psycopg2
  args:
    user: synapse
    database: synapse
    host: localhost
    cp_min: 5
    cp_max: 10
```

## Step 8 - Change The Max Filesize Allowed

Matrix-synapse allows you to send files with a max size of 10mb by default. Nginx however has a default max filesize of 1mb. Since Nginx sits in front of your matrix-synapse server, its cap is applied first.

In order to change Nginx's max filesize open `/etc/nginx/nginx.conf`.

```
sudo nano /etc/nginx/nginx.conf
```

In the `http {` section:
```/etc/nginx/nginx.conf
http {

	##
	# Basic Settings
	##
```

Add the line
```/etc/nginx/nginx.conf
client_max_body_size 10M;
```

If you would like to set the max size larger than 10mb, you will need to also increase the limit in the `homeserver.yaml` file for matrix-synapse.

```
sudo nano /etc/matrix-synapse/homeserver.yaml
```

Press `CTRL+W` to bring up search, type in `max_upload_size:` and press `ENTER`. You will see this line:

```/etc/matrix-synapse/homeserver.yaml
#max_upload_size: 10M
```

Uncomment it and change it to what you want. Keep in mind that if you set it higher than the max filesize allowed by Nginx it will be limited by Nginx.

## Step 9 Restart Services to Reload Configs
In order to make sure all the different settings take affect you will restart the various services you have configured.

```
sudo systemctl restart postgresql@11-main.service
```

```
sudo systemctl restart matrix-synapse.service
```

```
sudo systemctl restart nginx.service
```

## Conclusion

In this article you installed and configured Nginx, TLS, and matrix-synapse to host your own Matrix server. You can now create an account on your server using your preferred Matrix client, host private chats, or participate in public chats across all other federated Matrix servers.

More detailed information and other installation methods can be found at [Matrix.org's Synapse Repo](https://github.com/matrix-org/synapse)

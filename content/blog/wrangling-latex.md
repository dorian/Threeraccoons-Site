---
title: "Wrangling Latex"
date: 2021-07-24T10:49:35-04:00
draft: false
---

## Prequel
This week I spent several days going on an adventure of trying to create a nice looking resumé for a job application
I've been keeping an org mode file of all the info I want to put into resumés for awhile.
My main reason for this is that org mode make it easy for me to work on documents with the folding of headings.
So I thought it would be great if I could simply use the builtin org-mode export to PDF function in order to produce a nice PDF for my resumé!
Since I spent a bit more than a day pulling my hair out over this, I figured I'd record my struggles.
Perhaps at the very least I can avoid this particular anxiety again in the future!
The chief problem I would run into with this is that I know next to nothing of LaTeX,
meaning when I run into problems not only do I need to decipher the problem
I also need to then decipher whether the fix needs to occur in org-mode or LaTeX.

## Org mode
I began this all with a brief internet search for "org-mode resume", and
[Aidan Scannell's](https://www.aidanscannell.com/post/org-mode-resume/)
blog post about how they manage their resumé in org-mode and publish it via LaTeX was one of the first results.
Jackpot! This is exactly what I wanted to do, and I like the look of that resumé layout to boot.

So I read through the post,
and then followed all the directions of configurations that need to be set and entries to be made in the org file itself.
I use Doom Emacs, and it was great to see that Scannell does as well.
I felt confident this would work with minimal or no tweaks to the code he provided.

And it didn't work.
The main problems I faced were the `use-package!` functions for ox-extra and ox-latex.
I had assumed that I would also need to install those packages via the Doom Emacs `package!` function since we were calling them.
I did not realize they were bundled with org-mode and this was unnecessary.
Because of this issue and several other minor typos I was unable to get org-mode to produce a nicely formatted pdf.
I was under a deadline to get a nice pdf for my application, so I abandoned this avenue for the time being.

## Fallback: Plain LaTeX
I tried pulling the LaTeX template that Scannell linked to in his blog post,
but when I tried to run pdflatex on it to compile I got errors.
The first was about not finding the altacv.cls file.
An internet search popped up a github page hosting said file, so that was an easy fix.
Then it complained about fonts. Ugh.
Back to the internet again, found reports that the texlive-fontsextra package should solve this.
Luckily it did! Yay!
Then I had other errors come up
which at this point I don't remember,
but deadline constraints meant I felt it was time to try avenue again.
I was getting worryingly close to having to just cave and use LibreOffice for this.
The Horror!

So Ihttps://www.latextemplates.com/template/freeman-cv headed out searching for LaTeX templates for resumés.
Links to Overleaf
(the same site that Scannell linked to)
were pretty prominent,
but I wanted to avoid that site
in order to forego running into more headache inducing dependencies.

I came across
[latextemplates.com](https://www.latextemplates.com/)
which has several nice yet simple templates.
Nothing as fancy as some you can find over at Overleaf,
but they provide a zip with all dependencies!

I selected the
[Freeman](https://www.latextemplates.com/template/freeman-cv)
template.
When I opened the template I was overjoyed!
The comments in this one are so clear,
sections are demarcated clearly,
and it is very easy to add entries and move things around.
Other templates were commented fairly thoroughly,
but this one is just phenomenally beautiful documentation in my opinion.

## What's Next
So I played around with that template and got my application in.
Cool stuff.
But I am far from satisfied with the setup.
I may have gotten a resumé together for the current particular need,
but I do not have a good way to manipulate this information going forward.

I did get the features from ox-extra that Scannell mentioned working in my org file.
So I'm going to revisit Scannell's blog post soon,
and see if I can get their code working.
If not I'm going to attempt to build the same thing myself.
I know enough about LaTeX now that I can start building it myself using LaTeX export blocks in org mode.
I also intend to investigate some LaTeX workflows for academic articles and thesis writing.

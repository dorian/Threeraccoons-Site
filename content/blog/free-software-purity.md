---
title: "Free Software Purity"
date: 2021-03-29T07:37:12-04:00
draft: false
categories:
- blog
---

I've been thinking about my own personal use of free software a lot this past few weeks, ppurred on by the reinstatement of Richard Stallman to the FSF of course.
Many other blogs have been penned on that topic already, and I'm not really interested in discussing Stallman at all.
I've had my fill of those conversations on social media, and I think there are more constructive conversations to be had at this point.
The FSF is what it is.
While I hope the board will change and grow to make wiser decisions in the future, I'm not going to wait around for it.

There's been some very excellent discussion I've been following on Mastodon about how to move forward.
We've been examining the ways in which the freesoftware and opensource movements have fallen short of our ideals, what ways they work, and specifically how we can improve.
Here's a page on [communal software](https://communalsoftware.codeberg.page/) that has been developed as a result of this conversation.

I particularly love the point about how what's really important is the freedom of people, not software.
People need to be free from the coercion that proprietary software entails.
When working on spreading the cause of free software, focusing on the four freedoms of software before we even ask people the ways in which they've suffered from bad software defeats the whole purpose.
The values that drew me to the free software movement in the first place are those of liberating people, not desperate puritan witch hunts of proprietary blobs.
There's also a freedom of the heart, and I have found that focusing too much on the freedom of my personal software has led too some unpleasant tendencies in my own psyche.
Specifically the tendency to see my own choices and my own computing habits as better than others' choices when I focus exclusively on software freedom.
That is a really unpleasant place to live emotionally if I'm honest; I did not find it leading to happiness.
I also found it directly contradicting my values about free software that I actually hold.

Communal software has the potential to enable us to build the tools our communities need in a way that is specifically tailored for them and does not exploit them for profit.
We have the power to help each other, and we all have skills that we can use to provide for other human beings.
Communal software has the power to enable us to build reslient self determined communities.
Most importantly for me at this point, approaching software from the perspective of it's communal nature causes me to prioritize people.
By prioritizing people and how we can help each other I find that my psyche is more naturally led to compassion and understanding, which I was severely lacking when I was focusing on the freedom of my software.

The broad discussions of what the free software movement ought to be about that have been circulating over the past week have been wonderful.
Best of all, these discussions have connected me to a number of people who are thoughtful, compassionate, kind, and deeply invested in how we can leverage communal software to better people's lives.
And it has renewed my passion to be a part of this community and to help people, regardless of the freedom of the software they use.

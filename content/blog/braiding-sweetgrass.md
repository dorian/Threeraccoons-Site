+++
title = "Braiding Sweetgrass"
author = ["Dorian Wood"]
lastmod = 2022-07-11T16:31:32-04:00
tags = ["book-review"]
draft = true
weight = 3001
+++

## Info {#info}

Author: Robin Wall Kimmerer

Narrator: Robin Wall Kimmerer

Published: 2016

Started on: 2022-06-09

Finished on: 2022-07-03

Link: [Braiding Sweetgrass](https://www.hoopladigital.com/title/11672096)


## Review {#review}

I read the book Wall Kimmerer wrote after this one, Gathering Moss, a couple of months ago. Even more so this time I found myself enamored with Wall Kimmerer's perspective of the world and its complexity. Non-human beings deserve fair treatment and equal participation in our society as much as humans do! We all strive together as a basic function of life.

Her conception of the Honorable Harvest really drew me in. We owe our entire existence to the hard work of plants and fungi, and our use of those things should be accompanied with appropriate reciprocity. What can we possibly give back to beings who fuel our entire existence? In some situations the gift of gratitude is enough. But there is also a gift that we as humans are uniquely situated to give: our rational capacity to learn and plan. Wall Kimmerer tells a story of how one grad student took on a project that was much aligned by her advisors, and the result of the study was excellent evidence that showed sweetgrass thrives best when it is harvested appropriately (i.e. not overharvesting). We as a species have developed intensely close relationships with many non-human beings such that they rely on our harvest of them in order to set them up for success and proliferation. Cultivation and the Honorable Harvest are two gifts of reciprocity that we as human beings can uniquely offer.

This has given me an even greater passion for permaculture gardening. I am excited to live in concert with the land where I reside, doing my best to nurture it and cultivate its thriving as all the non human beings that reside here also contribute to my thriving. I learned of the wisdom of the three sisters from Wall Kimmerer, and have tried my hand at planting a three sisters garden. I was a bit late in the season when I did it, but I'm still hopeful for a bountiful harvest.

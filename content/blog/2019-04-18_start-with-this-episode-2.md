+++
title = "Start With This Episode 2"
description = ""
date = "2019-04-18"
categories = ["blog"]
+++

Creators of Nightvale Joseph Fink and Jeffry Cranor have started a new podcast about creating. It’s called Start With This and each episode they give a thing to consume and a project to create. The project for episode two was to open up lists of the top 100 bluegrass songs, the world’s largest cities, and the worlds best foods, and using a random number generator to pick one item from each list and write a two minute monologue. I’m too busy/lazy to record the monologue, so I’m just going to paste what I’ve written here. It’s somewhat amusing. http://www.nightvalepresents.com/startwiththis

Bluegrass song: 72 I found a hiding place by Carl Story
City: 44 Dar Es Salaam
Food: 4 Sushi

Religion has often been a hiding place for many people. A place of refuge and protection. When the city that I got turned out to be in Tanzania I have to admit my first thought was how many Americans move abroad and become expats as a refuge from the high cost of living, from the oppressive speed and rude culture of America. I wish for a refuge from capitalism most days. I think one of the worst aspects of having to seek refuge however is missing out on your favorite food. Sushi isn’t one that I think I’d miss excessively, but I certainly would miss it if I had to flee somewhere where it wasn’t available at all. Although Dar Es Salaam is a port city, they have to has sushi no? It’s weird to think how something have become so widespread they are ubiquitous. As a student of classics and philosophy, of ancient histories, I know how long it takes ideas and customs to travel. It is truly phenomenal how in our age things have become able to travel so fast that they have become known everywhere. Do you suppose there are any secrets left? Any things that are unknown to the world at large simply because they have not been connected, no one has arrived or left to carry the secret far away? I know the ancient world still holds many secrets. We have access to perhaps one percent of the content that was produced in the Roman empire. New things get found every year, but there is still much that will never be known again. Are there new things that no one knows? I know when I think I’ve come up with something new I want to share it on social media. I doubt many people around the globe, if any at all, don’t get excited to share things they discover or invent with their social circles online.

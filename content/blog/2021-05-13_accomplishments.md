---
date: 2021-05-12T05:05:45-04:00
title: "Wednesday's Accomplishments"
draft: false
categories:
- blog
---

Today I accomplished:
1. Redid the site CSS! Hello cutsey colors
1. Got my oil changed
1. Made macaroni salad for tomorrow
1. Started making a braided belt
1. Studied stats
1. Entered some more recipes from Grandma's book

Next time I would like to do better on:
1. Not much, it was a pretty good day!
1. Oh! Perhaps work on planning, getting more out of my day.

Tomorrow I would like to:
1. Therapy!
1. Continue adding Grandma's recipes to computer
1. Possible draft site CSS for recipes

+++
title = "Dotfiles"
description = "My unix dotfiles are available in a public repo."
date = "2020-11-30"
slug = "dotfiles"
+++

I use a minimalist terminal based workflow on my computer. That means all the configurations for the programs I use are stored in plain text files. If you want to check them out and see how I style the programs I use you can clone this git repo.

[Dotfiles](https://tildegit.org/mxd7428/dotfiles)

```
git clone https://tildegit.org/mxd7428/dotfiles
```

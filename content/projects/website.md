+++
title = "My Website"
description = "I have developed my own website"
date = "2020-11-30"
slug = "mywebsite"
+++

Here's a link to the git repo for the the Hugo theme that I use for this website:
[hugo-theme-dor](https://tildegit.org/mxd7428/hugo-theme-dor)

I recently completed the Responsive Web Design certification at [FreeCodeCamp](https://www.freecodecamp.org/).
I learned a lot from it and I highly recommend it for anyone looking to get into front end web design.
With the skills I gained from that I have designed my own website from the ground up.
All CSS and HTML are have been written by me.
It's perhaps not the most exciting looking website, but I really like minimalism.
Also it's pretty cool to be able to say I've made everything you see here.

I also made extensive use of the developer and reference resources at [W3Schools](https://www.w3schools.com/) and [Mozilla Developer Network](https://developer.mozilla.org/en-US/).

The color scheme I'm using is [Solarized](https://ethanschoonover.com/solarized/).
I have pretty as many thing as possible on my computer using this color scheme, it's great for being readable with minimal eye strain.

This website is developed and deployed using [Hugo](https://gohugo.io/), a really fun static site generator written in Go.

+++
title = "Health"
author = ["Dorian Wood"]
lastmod = 2022-08-23T13:57:56-04:00
draft = false
weight = 2001
+++

## Trans Resources {#trans-resources}

-   [Trans.chat](https://trans.chat/): Community collected resources on accessing care, education, etc
-   [r/TransDIY](https://reddit.com/r/TransDIY)
-   [r/asktransgender](https://reddit.com/r/asktransgender)
-   [r/ask_transgender](https://reddit.com/r/ask_transgender)
-   [hrt.cafe](https://hrt.cafe): Resources on HRT
-   [Transgender Michigan](https://www.transgendermichigan.org/): Resources for Trans folks living in Michigan, USA
-   [Lilith's Lair](https://www.lilithslairgr.com/): Queer owned salon
-   [River City Counseling](https://www.rivercitypsychological.com/): Queer affirming counseling


## Mental Health {#mental-health}

-   [Hearing Voices Network](https://www.hearing-voices.org/): Website/support network for people who have unusual perceptions
-   [National Survivor User Network](https://www.nsun.org.uk/): Membership organization for peer support and advocacy
-   [Power Makes Us Sick](https://pms.hotglue.me): Feminist Healthcare Collective


## Physical Health {#physical-health}

-   [Safe Needle Disposal](https://safeneedledisposal.org)
-   [Disability At Home](https://www.disabilityathome.org/): A collectin of photos of the tips and tricks disabled people and their caregivers have used to make life at home easier.


## Healthy Relationships and Boundaries {#healthy-relationships-and-boundaries}

-   [Ouch! and Oops!](https://www.diversityinclusioncenter.com/archives/ouch_files/Archives/Ouch_Vol5No1.html)


## Reproductive Rights {#reproductive-rights}

[Plan C Pills](https://www.plancpills.org/find-pills)

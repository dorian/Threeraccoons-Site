+++
title = "DIY"
author = ["Dorian Wood"]
lastmod = 2022-11-03T21:56:23-04:00
draft = false
weight = 2004
+++

## Furniture {#furniture}


### [Platform Bed with Drawers](https://www.instructables.com/Platform-Bed-with-Drawers/) {#platform-bed-with-drawers}


### [$35 Shoe Rack](https://thepoorswiss.com/how-to-build-nice-shoe-rack-35-dollars/) {#35-shoe-rack}


### [Bench with storage](https://jaimecostiglio.com/diy-shoe-storage-bench/) {#bench-with-storage}


## Yard {#yard}


### [10' X 14' Greenhouse](https://howtospecialist.com/garden/greenhouse/free-greenhouse-plans/) {#10-x-14-greenhouse}


## Tools {#tools}


### [Fully Adjustable DIY Grow Light](https://www.instructables.com/Fully-Adjustable-DIY-LED-Grow-Light-and-Grow-Box-C/) {#fully-adjustable-diy-grow-light}

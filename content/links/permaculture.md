+++
title = "Permaculture"
author = ["Dorian Wood"]
lastmod = 2022-08-23T17:46:47-04:00
draft = false
weight = 2005
+++

-   [Earth Care Library](http://www.earthcarelibrary.org/): Free permaculture texts
-   [Plants For A Future](https://pfaf.org/user/Default.aspx): Database of plants and their potential in a food forest system
-   [North Carolina Extension Gardener Toolbox](https://plants.ces.ncsu.edu/)
-   [MSU Extension: Agriculture](https://www.canr.msu.edu/agriculture/index)
-   [MSU Extension: Lawn &amp; Garden](https://www.canr.msu.edu/lawn_garden/index)
-   [Humble Abode Nursery](https://humbleabodenursery.com/)


## Plant Identification {#plant-identification}

-   [How to Identify Poison Ivy](https://extension.umd.edu/resource/how-identify-poison-ivy)
-   [Identifing Poison Ivy Quiz](http://www.birdandmoon.com/poisonivy/)


## Building {#building}

-   [How to Build a Cattle Panel Greenhouse](https://talesfromthemutiny.com/2018/02/how-to-build-a-cattle-panel-grow-tunnel/)
-   [Free Greenhouse Plans](https://www.thespruce.com/free-greenhouse-plans-1357126)

+++
title = "Tech"
author = ["Dorian Wood"]
lastmod = 2022-09-13T12:34:46-04:00
draft = false
weight = 2003
+++

## Mesh Networks: {#mesh-networks}

-   Communities who are working on mesh networking
    -   [Toronto Mesh](https://tomesh.net/)
    -   [Detroit Community Technology Project](https://detroitcommunitytech.org/)
    -   [People's Open](https://peoplesopen.net/): A project of Sudo Mesh
-   Resources for building your own mesh network
    -   [Mesh networking with OpenWRT and BATMAN](https://cgomesu.com/blog/Mesh-networking-openwrt-batman/)
    -   [Documentation of building a mesh network at DWeb 2019](https://dweb-camp-2019.github.io/meshnet/)
    -   [Sudo Mesh](https://sudoroom.org/wiki/Mesh): Mesh community based in Sudo Room in Oakland, California


## Self Hosting {#self-hosting}

-   [Limesurvey](https://www.limesurvey.org/): self hosted survey software, aimed at researchers
-   [Searsia](http://searsia.org/start.html): A federating search engine
-   [Rend-o-matic](https://github.com/Rend-o-matic/Rend-o-matic): For online music collaboration
-   [Etherpad](https://github.com/ether/etherpad-lite): Real time document collaboration
-   [Bookstack](https://www.bookstackapp.com/): Platform for writing/organizing information
-   [Sofie](https://github.com/nrkno/Sofie-TV-automation/): Open source tv broadcasting system used by Norwegian Public broadcasting
-   [SearXNG](https://github.com/searxng/searxng): Fork of SearX, a privacy respecting meta search engine
-   [Docker Mail Server](https://github.com/docker-mailserver/docker-mailserver): Containerized mail server
-   [Mailcow](https://mailcow.email): Containerized mail server
-   [Sabre](https://sabre.io/): CardDAV, CalDAV, WebDAV server


## VPS Options {#vps-options}

-   [AlphaVPS](https://alphavps.com/index.html): Great cheap storage options
-   [Contabo](https://contabo.com/en/vps/): Cheapest Option I've seen, $6 for 8gb ram.
-   [Hetzner](https://www.hetzner.com/cloud): Uses certified hyrdro and wind power for it's datacenters. Based in Germany


## The Fediverse {#the-fediverse}

-   Platforms
    -   [Epicyon](https://epicyon.net/): Microblogging, lightweight, no javascript
    -   [Pixelfed](https://pixelfed.org/): Photo sharing
    -   [Friendica](https://friendi.ca/): Social network, posts, events, photos
    -   [Hubzilla](https://hubzilla.org//page/hubzilla/hubzilla-project): Federated social network based around distributed
    -   [Gancio](https://gancio.org/federation): ActivityPub Events platform
    -   [Mobilizon](https://joinmobilizon.org/en/): by Framasoft, event platform
    -   [Karrot](https://karrot.world): Fedi platform focused on sharing food, designed to enable a more open and democratic way of organizing
-   Resources
    -   [Trunk](https://communitywiki.org/trunk): A community run list of people. Great place to start making friends and connections on the fedi.
    -   [Fediverse.space](https://fediverse.space/): A way to visualize servers on the activitypub network.
-   Tools
    -   <https://github.com/dariusk/rss-to-activitypub>


## Software Licenses {#software-licenses}

-   [Anticapitalist License](https://anticapitalist.software/)
-   [Non Violent Public License](https://thufie.lain.haus/NPL.html)
-   [Hippocratic License](https://firstdonoharm.dev/)
-   [Polyform License](https://polyformproject.org/licenses/)
-   [OpenAnarchist License](https://github.com/AnarchistLicense/OpenAnarchistLicense/blob/master/OpenAnarchistLicense.md)


## Education Tools {#education-tools}

-   [Standford Online High School: How to design and run online courses](https://ohs.stanford.edu/how)
-   [Recurse Code Camp](https://www.recurse.com/about)
-   <https://www.eff.org/deeplinks/2020/12/surveillance-self-defense-and-security-education-year-review-2020>
-   [The Internet Archive Research Assistant - Daily search Internet Archive for new items matching your keywords](https://github.com/savetz/tiara)
-   [Internet-in-a-box: Developing offline wifi learning hotspots](http://internet-in-a-box.org/)
-   [Write yourself a git: Learning git version control by building one from first principles](https://wyag.thb.lt/)
-   [Peer to Peer University: Local community based learning groups](https://www.p2pu.org/en/)
-   [Tech Learning Collective: Technology education for radical organizers and revolutionary communities.](https://techlearningcollective.com/foundations/)


## Web Dev {#web-dev}

-   Static site generators
    -   [Hugo](https://gohugo.io/): Golang (This is what I personally use)
    -   [SSG](https://www.romanzolotarev.com/ssg.html): Bash
    -   [Middleman](https://middlemanapp.com/): Ruby
    -   [Metalsmith](https://metalsmith.io/): NPM
-   Useful Libraries
    -   [Entity diagrams via mermaid.js](https://mermaid-js.github.io/mermaid/#/entityRelationshipDiagram)
    -   [SimpleCSS](https://github.com/kevquirk/simple.css)
    -   [Beautiful Soup](https://beautiful-soup-4.readthedocs.io/en/latest/): A python library for navigating html and xml data structures.
-   Answers I needed at one time
    -   <https://developer.mozilla.org/en-US/>
    -   <https://github.com/hail2u/html-best-practices#start-with-doctype>
    -   <https://anaulin.org/blog/hugo-raw-html-shortcode/>
    -   <https://discourse.gohugo.io/t/need-to-create-a-podcast-friendly-rss-feed/1727>


## Shell {#shell}

-   Useful Software:
    -   [Tut](https://github.com/RasmusLindroth/tut): TUI for Mastodon
    -   [Bashaspec](https://github.com/d10n/bashaspec): Testing for shell scripts
    -   [SO](https://github.com/samtay/so): Browse StackOverflow from terminal
    -   [Present](https://github.com/vinayak-mehta/present): Present from the terminal
    -   [CliFM](https://github.com/leo-arch/clifm): Cli file manager, no ncurses
    -   [Httpie](https://httpie.org/): Because sometimes curl is painful
    -   [Curld](https://github.com/Lohn/curld): Curl for various distributed protocols
    -   [lsd](https://github.com/Peltoche/lsd): A rewrite of GNU ls
    -   [fd](https://github.com/sharkdp/fd): a user friendly 'find' written in rust. Does not aim to be rewrite
-   Coding in the shell
    -   [Advanced Bash Scripting Guide](http://tldp.org/LDP/abs/html/abs-guide.html)
    -   [Charm.sh](https://charm.sh/): Tools for building glamorous command line software
    -   [Command Line Interface Guidelines](https://clig.dev/)
    -   [Gum](https://github.com/charmbracelet/gum): Utility for using Charm nicities in shell scripts


## Hardware {#hardware}

1.  <https://thedorkweb.substack.com/p/the-100-year-computer>
2.  <https://librecellular.org/>


## Web Tools {#web-tools}

1.  [Pixelcraft](https://pixelcraft.web.app/): Web app to make pixel art
2.  [Searchmysite](https://searchmysite.net/): an anti SEO search engine. Main site is an index of user submitted personal sites. Also self hostable.
3.  [Openring](https://git.sr.ht/~sircmpwn/openring): tool to generate webring snippets from RSS feeds. Useful in static site generators
4.  [Feed Me Up Scotty](https://feed-me-up-scotty.vincenttunru.com/): turns arbitrary sites into RSS feeds via CSS selectors
5.  [Lowweb](https://lowweb.tech/): Extensions for Firefox and Chrome designed to make all pages more resource light. For the planet.
6.  [Scroll](https://scroll.pub/): Static Site Generator using NodeJS
7.  [12ft.io](https://12ft.io/): Tool to remove paywalls from news sites
8.  [SponsorBlock](https://sponsor.ajay.app/): Extension to skip sponsor segments in youtube videos


## Text Only News {#text-only-news}

-   [LegibleNews](https://legiblenews.com/)
-   [Wikipedia: Current Events](https://en.wikipedia.org/wiki/Portal:Current_events)
-   [HackerNews](https://news.ycombinator.com/)
-   [CNN](https://lite.cnn.com/en)
-   [NPR](https://text.npr.org/)
-   [Christian Science Monitor](https://www.csmonitor.com/layout/set/text/textedition)


## Software {#software}


### Browsers {#browsers}

<https://github.com/dothq>


### Creative {#creative}

<https://www.darktable.org/>


### Learn to Code {#learn-to-code}

-   [uxn tutorial](https://compudanzas.net/uxn_tutorial.html): Introduction to the assembly programming language used in the uxn machine emulation made by 100r


## Twine {#twine}

-   [Twine 2 Homepage](http://twinery.org/)

[- Twine Cookbook](https://twinery.org/cookbook/index.html)

-   [Tweego Docs](https://www.motoslave.net/tweego/docs/)

[- Harlowe Story Format Docs](https://twine2.neocities.org/)


## Speech to Text {#speech-to-text}

-   [DeepSpeech from Mozilla](https://github.com/mozilla/DeepSpeech)
-   [Kaldi](https://kaldi-asr.org/)
-   [PocketSphinx](https://github.com/cmusphinx/pocketsphinx)


## Video Editors {#video-editors}

-   [MPV Video Splice](https://github.com/pvpscript/mpv-video-splice): Trim and splice videos using mpv and ffmpeg
-   [Lossless cut](https://github.com/mifi/lossless-cut): FFMPEG gui for lossless editing
-   [Editly](https://github.com/mifi/editly): Declarative video editing in the cli


## Server Admin {#server-admin}

-   <https://wiki.archlinux.org/index.php/System_maintenance>
-   <https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Removing_unused_packages_(orphans)>
-   <https://www.cyberciti.biz/tips/linux-unix-bsd-openssh-server-best-practices.html>
-   Matrix
    -   <https://github.com/ZerataX/matrix-registration>
    -   <https://www.matrix.org/blog/2015/04/23/monitoring-synapse-metrics-with-prometheus/>
    -   <https://hub.docker.com/r/awesometechnologies/synapse-admin>
-   Jitsi
    -   <https://community.jitsi.org/t/same-jitsi-meet-instance-with-multiple-domain-names/17391>


## Emacs Resources: {#emacs-resources}

<https://github.com/skeeto/elfeed>
<https://github.com/skeeto/skewer-mode>
<https://github.com/vedang/pdf-tools/#readme>


## Uxn {#uxn}

[Awesom  e Uxn](https://github.com/hundredrabbits/awesome-uxn): list of Uxn resources

+++
title = "Communities"
author = ["Dorian Wood"]
lastmod = 2022-08-23T17:41:10-04:00
draft = false
weight = 2004
+++

## Local: {#local}


### Tech Groups {#tech-groups}

-   [West Michigan Linux Users Group](http://wmlug.org/)
-   [Michigan!/usr/group](https://www.mug.org/)
-   [Semibug](http://semibug.org/): Southeast Michigan BSD User Group
-   [Grand Rapids Linux Users Group](http://wp.grlug.org/)
-   [Letterboxing North America](http://letterboxing.org/)
-   [Atlastquest letterboxing](https://www.atlasquest.com/)


### Tech companies {#tech-companies}

-   [Mallowfields](https://mallowfields.com/)


## Global {#global}

-   [Switter.io](https://switter.io/about): Sex worker friendly instance
-   [Native Land](https://native-land.ca/): Find the tribes that lay claim to the North American land you live on.
-   [Iffybooks](https://iffybooks.net/): Anarchist hacking/publishing space based in Philadelphia


## Community Building Resources {#community-building-resources}

<https://en.wikiversity.org/wiki/Digital_self-determination>
<https://e2c.how/>
<http://peerproduction.net/>
<https://nonprofit.ventures/>
<https://sociocracy30.org/>
<https://www.sociocracyforall.org/platform-co-op-governance-deep-democracy-on-scale/>
[Lurk](https://lurk.org/): Federated community builders
[Runyourown.social](https://runyourown.social/): Essay by @darius on creating digital neighborhoods
[The Handbook of Handbooks for Decentralised Organising](https://hackmd.io/@yHk1snI9T9SNpiFu2o17oA/Skh_dXNbE?type=view)
Co-op resources
  <https://wales.coop/>
  <https://platform6.coop/>
  <https://www.uk.coop/resources/simply-legal>


## Places to Contribute: {#places-to-contribute}

1.  <https://github.com/internetarchive/openlibrary/issues?q=is%3Aissue+is%3Aopen+label%3A%22good+first+issue%22>
2.  <https://cyberia.club/>
3.  <https://www.w3.org/International/participate>
4.  <https://thetubes.club/>
    -   <https://pvpn.reclaim.technology/wg-portal_notes>
5.  <https://dn42.eu/Home>
6.  Tildeverse:
    -   <https://tilde.club/wiki/usenet-news.html> - The flashing bit using just css (no java) is really cool.
    -   <https://tilderadio.org/> - new tagline on each reload
    -   <https://cosmic.voyage/>
    -   <https://tildeverse.org/> - main site
    -   <https://rawtext.club/>
    -   <https://yourtilde.com/>
    -   <https://tildegit.org/>
    -   <https://github.com/tildeclub/tilde.club/blob/master/docs/how-to-set-up-a-tilde.md>
7.  Fediverse:
    1.  Mastodon:
        1.  <https://github.com/nolanlawson/resources-for-mastodon-newbies>
        2.  <https://github.com/joyeusenoelle/GuideToMastodon>
    2.  <https://sepiasearch.org/search?search=&sort=-match&page=1>


## Community Guidelines Resources {#community-guidelines-resources}

-   <https://fossgovernance.org/>
-   <https://www.contributor-covenant.org/>
-   <https://www.theopensourceway.org/the_open_source_way-guidebook-2.0.html>


## Co-ops {#co-ops}

<https://hypha.coop/>


## Other link sites {#other-link-sites}

-   [slapheart](https://slapheart.com/)


## Old school site hosts {#old-school-site-hosts}

-   [Ichi.city](https://ichi.city/)

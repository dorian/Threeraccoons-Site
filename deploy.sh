#!/bin/sh

# Add .md extension for any files that lack it
# This is currently necessary because for some reason I cannot understand (May
# be some weird fuck up on my end somewhere), Hugo refuses to use archetypes
# when I use `hugo new --kind` when I name the file with a .md extension.a
for i in content/*/*; do
	[ "${i##*.}" = "md" ] || mv "$i" "$i.md"
done
# Clear the public directory. This is important so that we don't accidentally
# publish old material that's been shifted around.
rm -rf public/*
# Run hugo to compile the website
hugo
# And put it out there
rsync -ruvP public/ dorian@threeraccoons.com:/var/www/threeraccoons.com/
# rsync -ruvP public/ dorian@159.69.191.176:/var/www/threeraccoons.com/
